# -*- coding: utf-8 -*-
__author__ = 'Yihuan Zhang and Qin Lin'
__email__ = "13yhzhang@tongji.edu.cn, q.lin@tudelft.nl"

from scipy.optimize import rosen, differential_evolution
import scipy.io as si
import math
import time
import numpy as np


def costfunction(x):
    ## a(t) = C1 * dv(t - T) + C2 * (dx(t - T) - D(t)), D(t) = alpha + beta * v(t - T) + gama * a(t - T)
    C1 = x[0]
    C2 = x[1]
    alpha = x[2]
    beta = x[3]
    gama = x[4]
    T = x[5]
    F_cost_tol = 0
    lag_t  = round(T*10)
    num = 0
    for i in range(1,len(dv)):
        for j in range(int(lag_t)+1,len(dv[i])):
            D = alpha + beta * vel[i][j-lag_t] + gama * acc[i][j-lag_t]
            acc_y = C1 * dv[i][j-lag_t] + C2 * (dx[i][j-lag_t]-D)
            temp  = acc[i][j]-acc_y
            F_cost_tol = F_cost_tol + temp*temp
            num = num + 1
    # F_cost_tol = math.sqrt(F_cost_tol / num)
    F_cost_tol = F_cost_tol / num
    return F_cost_tol

print '111 start process'
start_time = time.clock()
#differential_evolution()
test_dir = 'testS/train_seq_2.mat'
data = si.loadmat(test_dir)
acc = data['acc'][0]  # a list, each element of the list contains one car following period
dv = data['dv'][0]
dx = data['dx'][0]
vel = data['vel'][0]

bound_helly = [(-0.5,0.5),(-0.5,0.5),(-1000,1000),(-10000,10000),(-10000,10000),(0,0.5)]
bounds = [(0,2), (0, 2), (0, 2), (0, 2), (0, 2)]
result = differential_evolution(costfunction,bound_helly,maxiter=100,seed=2,popsize=15,mutation=(0.5,1),recombination=0.9)
print result
print test_dir

# test_seq_1.mat
# 4289.169218
#      fun: 0.07519947163094033
#      jac: array([  1.75262582e-05,  -4.17124668e-05,  -4.06702449e-05,
#         -8.31223979e-04,   7.34771965e-04,   0.00000000e+00])
#  message: 'Optimization terminated successfully.'
#     nfev: 6910
#      nit: 75
#  success: True
#        x: array([ -6.51077299e-04,   9.76948388e-03,   2.56103565e+01,
#          7.69188902e-01,  -9.99913648e+01,   4.38853487e-02])
#
# Process finished with exit code 0

####################################################
end_time = time.clock()
print 'time', end_time-start_time,'s'





