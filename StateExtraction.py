# -*- coding: utf-8 -*-
__author__ = 'Yihuan Zhang and Qin Lin'
__email__ = "13yhzhang@tongji.edu.cn, q.lin@tudelft.nl"

##
#extract the state pattern of origin sequence
#output is save_pat_seq_x.txt
##

import time
import re

TRANSITIONS_REGEX = "^(?P<sst>\d+) (?P<sym>\w+) \[(?P<left>\d+), (?P<right>\d+)\]->(?P<dst>[-+]?\d+) \#(?P<fqr>\d+) p=(?P<prb>[0-9].?[0-9]+([eE][-+]?[0-9]+)?)$"

def __load(path):
    model = {}
    with open(path, "r") as mh:
        #mh.readline()
        for line in mh:
            m = re.search(TRANSITIONS_REGEX, line.strip())
            if m is not None:
                #print 'found'
                sstate = int(m.group("sst"))
                dstate = int(m.group("dst"))
                symbol = str(m.group("sym"))
                frequence = int(m.group("fqr"))
                leftGuard = int(m.group("left"))
                rightGuard = int(m.group("right"))
                probability = float(m.group("prb"))
                if sstate not in model:
                    model[sstate] = {}
                if symbol not in model[sstate]:
                    model[sstate][symbol] = {}
                model[sstate][symbol][(leftGuard, rightGuard)] = dstate
    return model

def sequenceConvert(m, dataPath):
    """Get the state sequence frames
    input:
        model, rti format input data
    return:
        state sequence, e.g. ['0,3,2,1', '0,4']
        string sequence with time guard, e.g. [[('d', (0, 542)), ('c', (0, 542)), ('j', (0, 542))], [('i', (0, 542))]]
    """
    stateSequence, stringSequence = [], []
    stateSequenceTemp, stringSequenceTemp = [], []
    nofind = 0
    with open(dataPath, "r") as dh:
        dh.readline()
        for line in dh:
            lineWithoutHead = line.strip().split(' ')[1:]
            currentState = 0
            #stateSequenceTemp.append(currentState)#modify
            stateSequenceTemp.append('00')
            for i in xrange(len(lineWithoutHead)/2):
                symbolUnit = str(lineWithoutHead[i*2])
                timeUnit = int(lineWithoutHead[i*2+1])
                guardUnit = queryTimeGuard(m, currentState, symbolUnit, timeUnit)
                currentState = m[currentState][symbolUnit][guardUnit]
                if currentState not in m:
                    nofind += 1
                    break
                else:
                    temp = []
                    temp.append(symbolUnit)
                    temp.append(guardUnit)
                    stringSequenceTemp.append(tuple(temp))
                    temp = []
                    #stateSequenceTemp.append(currentState)#modify
                    if currentState < 10:
                        stateSequenceTemp.append('0'+str(currentState))
                    else:
                        stateSequenceTemp.append(str(currentState))
            stateSequence.append(','.join(stateSequenceTemp))
            stringSequence.append(stringSequenceTemp)
            stateSequenceTemp = []
            stringSequenceTemp = []
    print nofind/4099.0, 'missing' # some training data are not fired by the model. need reflection!
    # print stateSequence[0:2]
    # print stringSequence[0:2]
    return stateSequence, stringSequence

def queryTimeGuard(m, currentState, symbolUnit, timeUnit):
    """query: the time inside which time guard
    input: model, current state, symbol, time unit. e.g. input 50 output guard [0:542]
    return:
        time guard tuple, e.g. (0,542)
    """
    possibleGuard = set()
    for item in m[currentState][symbolUnit].keys():
        possibleGuard.add((item[0],item[1]))
    for item in list(possibleGuard):
        if int(timeUnit) <= int(item[1]) and  int(timeUnit) >= int(item[0]):
            return item
        else:
            continue

def writestate(src,dst,stateframe):
    ori = []
    with open(src,'r') as sr:
        sr.readline()
        for line in sr:
            ori.append(line)
    dst_head = open(dst, 'w')
    dst_head.write(str(len(stateframe)))
    dst_head.write('\n')
    for i in xrange(len(stateframe)):
        dst_head.write(str(stateframe[i]) + '\n')
        dst_head.write(str(ori[i]) + '\n')
    dst_head.flush()

def seqwriter(pat_index,seq_index,seq_sym,seq_start,seq_end,dst):
    dst_head = open(str(dst)+'.txt', 'a')
    dst_head.write(str(pat_index)+','+str(seq_index)+', ['+str(seq_start)+','+str(seq_end)+'], '+str(seq_sym))
    dst_head.write('\n')
    dst_head.close()


print '111 start'
start_time = time.clock()

####################################################
rti = "solution.txt"
data = "cfrti.txt" #rti input data
patternPath = "stringPattern.txt" #output string patterns
statePath = "statePattern.txt" #output frequent common state patterns
oriPath = 'ori_seq.txt'
m = __load(rti)
stateFrame, stringFrame = sequenceConvert(m, data)
print 'startFrame ready'

####################################################

#load files:
ori_dir = 'ori_seq.txt'# original data
pat_dir = 'testS.txt'#'clusteredStateSequence.txt'# new state pattern sequence


cfrti_data = []
with open(data, "r") as daa:
    daa.readline()
    for line in daa:
        lineWithoutHead = line.strip().split(' ')[1:]
        cfrti_data.append(lineWithoutHead)
seq = []
with open(ori_dir, "r") as rt:
    rt.readline()
    for line in rt:
        seq.append(line.strip())

cluster_st_pat = []
p1 = re.compile(r'\;')
p2 = re.compile(r'\,')
count = 0
with open(pat_dir, "r") as mh:
    for line in mh:
        if line != '\n':
            count += 1
            patt = []
            temp = p1.split(line.strip()) #state patterns ['9,24,19']
            #print temp
            for item in temp:
                temp1 = p2.split(str(item))
                temp11 = ''
                for ii in temp1:
                    #print ii,type(ii)
                    if int(ii) < 10:
                        temp11 = str(temp11) + '0' + str(ii) + ','
                    else:
                        temp11 = str(temp11) + str(ii) + ','
                patt.append(temp11[:-1])
            cluster_st_pat.append(patt)
countt = 0
for temp11 in cluster_st_pat:
    countt += 1
    print countt
    count1 = 0
    for pat_item in temp11:#['00,07,13', '08,07,13', '00,07,17', '08,07,17', '00,07,13,20']
        count = 0
        count1 += 1
        for itemst in stateFrame:#itemst 00,07,13,20,01,06,02,01
            if pat_item in itemst:
                #print 'match'
                count += 1
                start = itemst.find(pat_item)/3
                #print 'start',start
                index = stateFrame.index(itemst)
                #print 'index',index
                length = pat_item.count(',')+1
                #print 'length',length
                #print 'cf',len(cfrti_data[index])
                if start+length < len(cfrti_data[index])/2:
                    #print 'inside'
                    pat_seq = cfrti_data[index][start*2:start*2+length*2]
                    #print pat_seq
                    pat = ''
                    for iii in xrange(length - 1):
                        pat = str(pat)+ '(' + str(pat_seq[iii*2]) + ')' + '{' + str(pat_seq[iii*2+3]) + '}'
                    #print pat
                    m = re.finditer(pat,seq[index])
                    try:
                        while True:
                            # print 'found'
                            itt = m.next()
                            #print itt.group()
                            seqwriter(count1,index, itt.group(), itt.start(), itt.end(), 'save_pat_seq_' + str(countt))
                    except StopIteration:
                        pass
                else:
                    #print 'end of seq'
                    pat_seq = cfrti_data[index][start*2:]
                    pat = ''
                    for iii in xrange(length-2):
                        pat = str(pat)+ '(' + str(pat_seq[iii*2]) + ')' + '{' + str(pat_seq[iii*2+3]) + '}'
                    pat = str(pat) + '(' + str(pat_seq[iii*2+2]) + ')' + '{1,500}'
                    m = re.finditer(pat, seq[index])
                    try:
                        while True:
                            # print 'found'
                            itt = m.next()
                            # print itt.group()
                            seqwriter(count1, index, itt.group(), itt.start(), itt.end(), 'save_pat_seq_' + str(countt))
                    except StopIteration:
                        pass
                #print 'next'
        #print count

####################################################
end_time = time.clock()
print 'time', end_time-start_time,'s'


