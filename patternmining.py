# -*- coding: utf-8 -*-
__author__ = 'Yihuan Zhang and Qin Lin'
__email__ = "13yhzhang@tongji.edu.cn, q.lin@tudelft.nl"

import re
from jellyfish import jaro_distance
import scipy.cluster.hierarchy
import numpy as np
import matplotlib.pyplot as plt
from scipy.cluster import hierarchy
from scipy.cluster.hierarchy import fcluster

TRANSITIONS_REGEX = "^(?P<sst>\d+) (?P<sym>\w+) \[(?P<left>\d+), (?P<right>\d+)\]->(?P<dst>[-+]?\d+) \#(?P<fqr>\d+) p=(?P<prb>[0-9].?[0-9]+([eE][-+]?[0-9]+)?)$"


def __load(path):
    model = {}
    with open(path, "r") as mh:
        #mh.readline()
        for line in mh:
            m = re.search(TRANSITIONS_REGEX, line.strip())
            if m is not None:
                #print 'found'
                sstate = int(m.group("sst"))
                dstate = int(m.group("dst"))
                symbol = str(m.group("sym"))
                frequence = int(m.group("fqr"))
                leftGuard = int(m.group("left"))
                rightGuard = int(m.group("right"))
                probability = float(m.group("prb"))
                if sstate not in model:
                    model[sstate] = {}
                if symbol not in model[sstate]:
                    model[sstate][symbol] = {}
                model[sstate][symbol][(leftGuard, rightGuard)] = dstate
    return model
def sequenceConvert(m, dataPath, stateFramePath, stringFramePath):
    """Get the state sequence frames
    input:
        model, rti format input data
    return:
        state sequence, e.g. ['0,3,2,1', '0,4']
        string sequence with time guard, e.g. [[('d', (0, 542)), ('c', (0, 542)), ('j', (0, 542))], [('i', (0, 542))]]
    """
    stateSequence, stringSequence = [], []
    stateSequenceTemp, stringSequenceTemp = [], []
    nofind = 0
    length = []
    #line_num=0
    with open(dataPath, "r") as dh:
        dh.readline()
        #line_num+=1
        for line in dh:
            #line_num+=1
            #if line_num > 1:
            #    break
            #print line
            lineWithoutHead = line.strip().split(' ')[1:]
            currentState = 0
            #stateSequenceTemp.append(currentState)#modify
            stateSequenceTemp.append('0')
            for i in xrange(len(lineWithoutHead)/2):
                symbolUnit = str(lineWithoutHead[i*2])
                #print symbolUnit, 's'
                #print currentState, 'cs'
                if currentState not in m or symbolUnit not in m[currentState].keys():
                    nofind += 1
                    #print '?'
                    break
                if currentState == None or currentState == -1:
                    nofind += 1
                    #print '??'
                    break
                timeUnit = int(lineWithoutHead[i*2+1])
                if queryTimeGuard(m, currentState, symbolUnit, timeUnit)==None:
                    nofind += 1
                    break
                guardUnit = queryTimeGuard(m, currentState, symbolUnit, timeUnit)
                #print guardUnit, 'guard'
                currentState = m[currentState][symbolUnit][guardUnit]
                if currentState not in m:
                    nofind += 1
                    break
                else:
                    temp = []
                    temp.append(symbolUnit)
                    temp.append(guardUnit)
                    stringSequenceTemp.append(tuple(temp))
                    temp = []
                    #stateSequenceTemp.append(currentState)#modify
                    stateSequenceTemp.append(str(currentState))
            stateSequence.append(','.join(stateSequenceTemp))
            length.append(len(stateSequenceTemp))
            stringSequence.append(stringSequenceTemp)
            stateSequenceTemp = []
            stringSequenceTemp = []
    print min(length),max(length), 'min-max length'
    with open(stateFramePath, "w") as h1:
        for item in stateSequence:
            h1.write(str(item))
            h1.write('\n')
    with open(stringFramePath, "w") as h2:
        for item in stringSequence:
            h2.write(str(item))
            h2.write('\n')
    #print nofind/4099.0, 'missing' # some training data are not fired by the model. need reflection!
    return stateSequence, stringSequence
def queryTimeGuard(m, currentState, symbolUnit, timeUnit):
    """query: the time inside which time guard 
    input: model, current state, symbol, time unit. e.g. input 50 output guard [0:542]
    return:
        time guard tuple, e.g. (0,542)
    """
    possibleGuard = set()
    for item in m[currentState][symbolUnit].keys():
        possibleGuard.add((item[0],item[1]))
    for item in list(possibleGuard):
        if int(timeUnit) <= int(item[1]) and  int(timeUnit) >= int(item[0]):
            return item
        else:
            continue
def frequentStateSequence(trainStateFrame, trainStringFrame):
    """ using N-gram to get all state pattern
    input: state frames
    return: dictionary of state patterns counts: dict[length of state patterns][state patterns] = number of occurence
    top-k frequence sequence: e.g. [('2', '12', '2'), ('12', '2', '12')]
    all possible state pattern: e.g. [('2', '12', '2'), ('12', '2', '12')]
    """
    #statePatternCounter = {}
    stateFrameSplit = []
    for i in xrange(len(trainStateFrame)):
        stateFrameSplit.append(trainStateFrame[i].split(','))
    #print 'ppp', stateFrameSplit[0]
    statePatternCounterWithoutLen = {}
    stateStringLink = {} #mapping state sequence to symbolic sequence
    allStatePatternSet = set() 
    support = 3
    topK = 30
    for i in xrange(support,10):#minimum and maximum (36) of sliding window length 
        #statePatternCounter[i] = {}
        for j in xrange(len(stateFrameSplit)):
            for k in xrange(len(stateFrameSplit[j])-i+1):
                if tuple(stateFrameSplit[j][k:k+i]) not in stateStringLink:
                    stateStringLink[tuple(stateFrameSplit[j][k:k+i])] = []
                if trainStringFrame[j][k:k+i-1] not in stateStringLink[tuple(stateFrameSplit[j][k:k+i])]:
                    stateStringLink[tuple(stateFrameSplit[j][k:k+i])].append(trainStringFrame[j][k:k+i-1])
                #if tuple(stateFrame[j][k:k+i]) in statePatternCounter[i]:
                #     statePatternCounter[i][tuple(stateFrame[j][k:k+i])] = 0
                # statePatternCounter[i][tuple(stateFrame[j][k:k+i])] += 1 #dictionary, key[length][substate], value(count)
                #if tuple(stateFrame[j][k:k+i]) not in statePatternCounterWithoutLen:
                #    statePatternCounterWithoutLen[tuple(stateFrame[j][k:k+i])] = 0
                allStatePatternSet.add(','.join(stateFrameSplit[j][k:k+i])) #dictionary, key[substate], value(count)
    # dictStatePatternCounterWithoutLen = sorted(statePatternCounterWithoutLen.iteritems(), key=lambda d:d[1], reverse=True)
    # topKStateSequence = []
    # for i in xrange(topK):
    #     topKStateSequence.append(dictStatePatternCounterWithoutLen[i][0])
    # allStatePattern = []
    #print len(dictStatePatternCounterWithoutLen)
    #for i in xrange(len(dictStatePatternCounterWithoutLen)):
    #    allStatePattern.append(dictStatePatternCounterWithoutLen[i][0])
    #return statePatternCounter, topKStateSequence, allStatePattern
    return list(allStatePatternSet), stateStringLink
def commonSubStringFinder(trainStateFrame, patternState):
    """ find common substring in state frames
    input: state frames, all possible state patterns, e.g. [('2', '12', '2'), ('12', '2', '12')]
    output: sorted list of common substring of states, e.g. [(('1', '6', '2'), 1033), (('1', '6', '2', '1'), 947)]
    """
    dictCommonSubString = {}
    #print stateFrame[0], "state"
    #print patternState[0], "pa"
    for item in patternState:
        counter = 0
        for jtem in (trainStateFrame):
            if (item) in (jtem):
                counter += 1
        if counter > 100: #threshold to extract the common substring
            dictCommonSubString[item] = counter
    sortedCommonSubString = sorted(dictCommonSubString.iteritems(), key=lambda d:d[1], reverse=True)
    return sortedCommonSubString
def uniChar(stateSymbol):
    uniCharList = ['' for i in xrange(len(stateSymbol))]
    for i in xrange(len(stateSymbol)):
        for j in xrange(len(stateSymbol[i].strip().split(','))):
            uniCharList[i]+=chr(int(stateSymbol[i].strip().split(',')[j])+65)
    #print uniCharList[0], 'uni'
    return uniCharList
def stringDistance(tupleArray, originStrings, warpStrings):
    ''' compute string similarity
    '''
    #for i in xrange(len)
    #print i, j
    #disMatrix = np.zeros((1, len(tupleArray[0])))
    disMatrix = []
    disDict = {}
    for i in xrange(len(tupleArray[0])):
        left = tupleArray[0][i]
        right = tupleArray[1][i]
        #print unicode(inputClustering[left])
        #print unicode(inputClustering[right])
        dis = 1 - jaro_distance(unicode(warpStrings[left]), unicode(warpStrings[right]))
        disDict[tuple(set((originStrings[left], originStrings[right])))] = dis
        disMatrix.append(dis)
    return disMatrix, disDict
def wordsHierarchy(original, warp):
    '''hierarchy clustering of state sequences
    input: state sequences before clustering, e.g. ['162', '1621', '621']
    output: clustered sequence
    '''
    _ = np.triu_indices(len(original), 1)
    upperTriangle, disTable = stringDistance(_,original,warp)
    #print upperTriangle, 'uppp'
    z = scipy.cluster.hierarchy.linkage(upperTriangle, method='average')
    plt.figure()
    #plt.title('Hierarchical Clustering Dendrogram')
    #plt.xlabel('sample index')
    plt.ylabel('distance', fontsize=15)
    hierarchy.dendrogram(z, no_labels=True)
    max_d = 0.903
    plt.plot([0,10000], [max_d, max_d], ls='dashed', linewidth=1.5, c='k')
    plt.show()
    max_cluster = 6
    clusterNumber = fcluster(z, max_d, criterion='distance') #based on maximum gap distance in hierarchy; list of cluster number assigned, e.g. [1 1 1 1 1 1 2 1 1 1]
    #clusterNumber = fcluster(z, max_cluster, criterion='distance') #forced number of clusters
    alphabetCluster = sorted(list(set(clusterNumber))) #[1,2]
    clusterStrings = [[] for x in xrange(len(alphabetCluster))]
    for i in xrange(len(clusterNumber)):
        clusterStrings[clusterNumber[i]-1].append(original[i]) #[['162', '1621', '621', '1221', '1611', '2122', '1161', '216', '21221'], ['491']]
    #print clusterStrings, 'ttt'
    return clusterStrings
def clusterAssign(newSequence, clusterString, disDict):
    '''compute which cluster the new sequence belongs to
    input: new sequence needed to be assigned cluster number, e.g. '1,6,2'; already clustered sequences, e.g. [['1,6,2', '1,6,2,1', '12,2,1'], ['4,9,1']]
    dictionary for looking up the distance between two sequences
    '''
    disPerCluster = []
    disSum = 0
    beforeAssign = [newSequence]
    #print beforeAssign, 'before'
    for i in xrange(len(clusterString)):
        for item in clusterString[i]:
            if tuple(set((newSequence, item))) in disDict:
                disSum += disDict[tuple(set((newSequence, item)))]
            else:
                #print beforeAssign
                #print item
                left = uniChar(beforeAssign)[0]
                right = uniChar([item])[0]
                disSum += (1 - jaro_distance(unicode(left), unicode(right)))
        disPerCluster.append(1.0*disSum/len(clusterString[i]))#distance between new sequence and each cluster
        disSum = 0
    #print disPerCluster
    closestClusterNumber = disPerCluster.index(min(disPerCluster))
    return closestClusterNumber
    # '''Elbow plotting'''
    # last = z[0:, 2]
    # last_rev = last[::-1]
    # idxs = np.arange(0, len(last))
    # x_label_pos = idxs[::-1]
    # _xticks = [i*50 for i in xrange(0,6)]
    # plt.xticks(_xticks, _xticks[::-1])
    # plt.tick_params(labelsize=15)
    # plt.xlabel('Iteration ID', fontsize=15)
    # plt.ylabel('Distance', fontsize=15)
    # plt.plot(idxs, last_rev, ls='dashed', linewidth=2, c='blue')
    # plt.show()
def subListFind(targetList, patternList, clusterID, indexList):
    for i in xrange(len(targetList)-len(patternList)+1):
        if targetList[i:i+len(patternList)] == patternList:
            indexList.append(tuple((list(xrange(i,i+len(patternList))), clusterID)))
    return indexList
def emptyAssign(unitFrame, clusterString, disDict):
    start, end  = 0, 0
    while start < len(unitFrame):
        if unitFrame[start] == -1:
            for i in xrange(len(unitFrame[start:])):
                if unitFrame[start+i] == -1:
                    end = start+i
                    if end == len(unitFrame)-1:
                        filterSeq = unitFrame[start:]
                        filterStr = ",".join(str(x) for x in filterSeq)
                        ID = clusterAssign(filterStr, clusterString, disDict)
                        unitFrame[start:] = [ID for k in xrange(end+1-start)]
                        start = end+1
                else:
                    filterSeq = unitFrame[start:end+1]
                    filterStr = ",".join(str(x) for x in filterSeq)
                    ID = clusterAssign(filterStr, clusterString, disDict)
                    unitFrame[start:end+1] = [ID for k in xrange(end+1-start)]
                    start = end+1
                    break
        else: start += 1
        if start >= len(unitFrame): break
    return unitFrame
def segmentation(Frame, clusterString, disDict):
    '''Assign cluster number to each item in the state frame
    input: 1. list of state frames e.g. ['1,6,2', '1,6,2,1']
    even if the input is a single frame, frame 0 for instance, you need to call:
    segmentation(stateFrame[0:1], clusteredStateSequence, disTable)
    2. aready clustered stateSequences
    3. distance Tabel for looking up
    output print every assigned state frame
    '''
    emptyNum = 0
    totalNum = 0
    assignFrame = []
    #with open(assigned_cluster_sequence, 'w') as acs:
    for i in xrange(len(Frame)):
        indexList = []
        for j in xrange(len(clusterString)):
            for jtem in clusterString[j]:
                indexList = subListFind(Frame[i].split(','), jtem.split(','), j, indexList)
        indexListSort = sorted(indexList, key=lambda indexItem : len(indexItem[0]))
        #print indexListSort, 'index'
        unitFrame = [-1 for k in xrange(len(Frame[i].split(',')))]
        for l in xrange(len(indexListSort)):
            for ltem in indexListSort[l][0]:
                unitFrame[ltem] = indexListSort[l][1]
        '''
        compute the proportion of infrequent data
        '''
        for item in unitFrame:
            if item == -1:
                emptyNum += 1
        totalNum += len(unitFrame)
        assignFrame.append(emptyAssign(unitFrame, clusterString, disDict))
    print emptyNum, totalNum, 'proportion'
    return assignFrame
if __name__ == "__main__":
    rti = "solution_train9.txt" 
    data = "rti_full9.txt" #rti input data
    statePath = "frequent_state_sequence9.txt" #output frequent common state patterns
    clusterPath = 'train_cluster_state_sequence9.txt' #cluster string
    assignedPath = 'assign_sequence9.txt' #complete frames assigned cluster id
    stateFramePath = 'stateFrame.txt'
    stringFramePath = 'stringFrame.txt'
    m = __load(rti)
    #print m[0]
    trainStart = 0
    trainEnd = 3279
    TestStart = 3279
    stateFrame, stringFrame = sequenceConvert(m, data, stateFramePath, stringFramePath)
    print len(stateFrame), 'kzhu'
    trainStateFrame = stateFrame[trainStart:trainEnd]
    trainStringFrame = stringFrame[trainStart:trainEnd]
    patternState, link = frequentStateSequence(trainStateFrame, trainStringFrame)#only train
    frequentCommonSubState = commonSubStringFinder(trainStateFrame, patternState)#only train
    print len(frequentCommonSubState), 'aaa'
    words = []
    with open(statePath, 'w') as sh:
        for item in frequentCommonSubState:
            words.append((item[0]))
            sh.write((item[0]))
            sh.write('\n')
    clusteredStateSequence = wordsHierarchy(words, uniChar(words))
    with open(clusterPath, 'w') as cph:
        for item in clusteredStateSequence:
            #words.append((item[0]))
            cph.write(';'.join(item))
            cph.write('\n\n\n')
    _ = np.triu_indices(len(words), 1)
    upperTriangle, disTable = stringDistance(_, words, uniChar(words))
    #print clusterAssign('0,4', clusteredStateSequence, disTable), 'nnn'
    assignClusterFrame = segmentation(stateFrame, clusteredStateSequence, disTable)
    sentence =''
    with open(assignedPath, 'w') as aph:
        for item in assignClusterFrame:
            for jtem in item:
                sentence+=(str(jtem)+',')
            #words.append((item[0]))
            aph.write(sentence[0:-1])
            sentence = ''
            aph.write('\n')