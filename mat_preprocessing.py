# -*- coding: utf-8 -*-
__author__ = 'Yihuan Zhang and Qin Lin'
__email__ = "13yhzhang@tongji.edu.cn, q.lin@tudelft.nl"

##
#randomly chose 80% of sequence data as training samples and 20% as testing samples
##

import scipy.io as si
import time
import random
import numpy as np

def data_random(src,i):
    data = si.loadmat(src)
    accList = data['acc'][0]
    dvList = data['dv'][0]
    dxList = data['dx'][0]
    velList = data['vel'][0]
    numS = len(accList)
    rand_index = np.arange(numS)
    random.shuffle(rand_index)
    #print rand_index
    train_index = rand_index[0:round(0.8 * numS)]
    test_index = rand_index[round(0.8 * numS) + 1:]

    acc = [accList[ii] for ii in train_index]
    dv  = [dvList[ii]  for ii in train_index]
    dx  = [dxList[ii]  for ii in train_index]
    vel = [velList[ii] for ii in train_index]

    si.savemat('testS/train_seq_' + str(i) + '.mat', {'acc': acc, 'dv': dv, 'dx': dx, 'vel': vel})

    acc = [accList[ii] for ii in test_index]
    dv  = [dvList[ii]  for ii in test_index]
    dx  = [dxList[ii]  for ii in test_index]
    vel = [velList[ii] for ii in test_index]

    si.savemat('testS/test_seq_' + str(i) + '.mat', {'acc': acc, 'dv': dv, 'dx': dx, 'vel': vel})



print '111 start processing'
start_time = time.clock()

index = 6 # CFtest file number
# src_dir = 'testS/CFtest_seq_' + str(1) + '.mat'
for i in xrange(index):
    src_dir = 'testS/CFtest_seq_' + str(i+1) + '.mat'
    data_random(src_dir,i+1)
####################################################
end_time = time.clock()
print 'time', end_time-start_time,'s'





