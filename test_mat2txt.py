# -*- coding: utf-8 -*-
__author__ = 'Yihuan Zhang and Qin Lin'
__email__ = "13yhzhang@tongji.edu.cn, q.lin@tudelft.nl"
import matplotlib.pyplot as plt
import scipy.io as si
import numpy as np
from sklearn.cluster import KMeans
from scipy.cluster.vq import vq
from scipy.spatial import distance
aOffset = 97 #ascci convertion from int to char
def kmeans_labelling(center_model, pre_labelling):
	labels,_ = vq(pre_labelling, center_model)
	return chr(aOffset+labels[0])
def kmeans_cluster(pre_discrete, num_cluster, init = 'random', random_state=10):
	clf = KMeans(n_clusters = num_cluster, init = 'random', random_state=10)
	clf.fit(pre_discrete)
	centroids = clf.cluster_centers_
	kmeans_dict = {}
	kmeans_dict_values = []
	for i in xrange(len(centroids)):
		kmeans_dict[i] = centroids[i]
	alphabet_size = len(kmeans_dict)
	alphabet = kmeans_dict.keys()
	print kmeans_dict
	return alphabet_size, centroids
#convert normal string to rti type string, e.g. aaabbbcdd->(a,0),(b,3),(c,3),(d,1)
def rticonvert(normalStr):
	rtiStr, uniTime, uniEvent= [],[],[]
	for i in xrange(len(normalStr)):
		if i == 0:
			uniTime.append(0)
			uniEvent.append(normalStr[0])
		else:
			if normalStr[i]!=normalStr[i-1]:
				uniTime.append(i)
				uniEvent.append(normalStr[i])
	for i in xrange(len(uniEvent)):
		if i==0:
			rtiStr.append((uniEvent[0],0))
		else:
			rtiStr.append((uniEvent[i],uniTime[i]-uniTime[i-1]))
	return rtiStr
# write the rti sequence into files
def rtiwriter(rtiSequence, numSequence, alphabet_size, dst):
	dst_head = open(dst, 'w')
	dst_head.write(str(numSequence)+' '+str(alphabet_size))
	dst_head.write('\n')
	for i in xrange(numSequence):
		dst_head.write(str(len(rtiSequence[i]))+' ')
		for j in xrange(len(rtiSequence[i])):
			dst_head.write(str(rtiSequence[i][j][0])+' '+str(rtiSequence[i][j][1])+' ')
		dst_head.write('\n')
	dst_head.flush()
src = 'train_data_ngsim.mat'# original data
dstTrain = 'data/rti_train9.txt'# output file-rti train
dstFull = 'data/rti_full9.txt'# output file-rti full
data = si.loadmat(src)
accList = data['acc'][0]#a list, each element of the list contains one car following period
dvList = data['dv'][0]
dxList = data['dx'][0]
velList = data['vel'][0]
numSequence = len(accList)# number of car following periods, 4099 in this case
numInstance = 0
for i in xrange(numSequence):
	numInstance += len(accList[i])
instance = np.zeros((numInstance,3))# the matrix used for clustering, each instance contains 3 features: dv, dx, vel
k = 0
timeTemp = []
period = []#a list containing 4099 elements. Each element is one period of following. Each period it is multivariate time serie.
trainingStart = 0
trainingEnd = 3279
for i in xrange(numSequence):
	for j in xrange(len(accList[i])):
		#instance[k+j][0] = float(accList[i][j])
		instance[k+j][0] = float(dvList[i][j])
		instance[k+j][1] = float(dxList[i][j])
		instance[k+j][2] = float(velList[i][j])
		timeTemp.append([ float(dvList[i][j]), float(dxList[i][j]), float(velList[i][j])])#float(accList[i][j]),
	period.append(timeTemp)
	timeTemp = []
	k = k+len(accList[i])
num_cluster = 9
alphabet_size, center = kmeans_cluster(instance[trainingStart:trainingEnd], num_cluster) #symbol clustering only on training data
sequenceTemp = []
sequence = []
innerClusterVar = [0 for i in xrange(num_cluster)]
innerClusterVarNum = [0 for i in xrange(num_cluster)]
sumInnerVar = 0.0
length_per_frame = []
data_point_num = 0
for i in xrange(len(period)):
	for j in xrange(len(period[i])):
		symbol = kmeans_labelling(center, np.reshape(period[i][j],(1,3)))
		sequenceTemp.append(symbol)
		innerClusterVar[ord(symbol)-aOffset] += distance.euclidean(center[ord(symbol)-aOffset], period[i][j])
		#innerClusterVarNum[ord(symbol)-aOffset] += 1
		data_point_num += 1
	length_per_frame.append(len(period[i]))
	sequence.append(sequenceTemp)
	sequenceTemp = []
print np.min(length_per_frame), "minl"
print np.max(length_per_frame), "maxl"
print data_point_num, "total"
for i in xrange(num_cluster):
	#sumInnerVar += (innerClusterVar[i]*1.0/innerClusterVarNum[i])
	sumInnerVar += innerClusterVar[i]
print sumInnerVar, 'sumInnerVar'
print innerClusterVar, innerClusterVarNum
t_dir = 'trainSymbolSeq9.txt'
f = open(t_dir, 'w')
for item in sequence:
   for item1 in item:
       f.write(str(item1))
   f.write('\n')
f.close()
# print 'finish'
# print center, period[0][0]
rtiTrainSequence = []
rtiFullSequence = []
for item in sequence[trainingStart:trainingEnd]:
    rtiTrainSequence.append(rticonvert(item))
for item in sequence:
    rtiFullSequence.append(rticonvert(item))
rtiwriter(rtiTrainSequence, len(rtiTrainSequence), alphabet_size, dstTrain)
rtiwriter(rtiFullSequence, len(rtiFullSequence), alphabet_size, dstFull)