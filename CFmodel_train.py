# -*- coding: utf-8 -*-
__author__ = 'Yihuan Zhang and Qin Lin'
__email__ = "13yhzhang@tongji.edu.cn, q.lin@tudelft.nl"


##
# train the car following model via pattern sequence
# output CFtest.mat
##

import matplotlib.pyplot as plt
import numpy as np
import scipy.io as si
import re

################

################

#load files:
ori_dir = 'ori_seq.txt'# original data
pat_dir = 'patternSeq.txt'#pattern sequence
src_dir = 'train_data_ngsim.mat'
head_dir = 'pat_seq_head.txt'
data = si.loadmat(src_dir)
print type(data)
accList = data['acc'][0]#a list, each element of the list contains one car following period
print len(accList[0])
print len(accList[1])
#print len('dddddddddddddddccccccccccccccccccddddddccccccccccccccccccccc')
dvList = data['dv'][0]
dxList = data['dx'][0]
velList = data['vel'][0]
numSequence = len(accList)# number of car following periods, 4099 in this case
TRANSITIONS_REGEX = "^(?P<indexx_s>\d+),(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"
with open(head_dir,'r') as he:
    count = 0
    for line1 in he:
        count += 1
        print line1
        #print line[:-1]
        with open(str(line1[:-1])+'.txt','r') as ftemp:
            tempp = []
            for line in ftemp:
                m = re.match(TRANSITIONS_REGEX, line)
                if m is not None:
                    #print 'found'
                    index_temp = int(m.group("indexx"))
                    start_temp = int(m.group("startt"))
                    end_temp   = int(m.group("endd"))
                    tempp.append([index_temp, start_temp, end_temp])
            print len(tempp)
            acc_temp = []
            dv_temp  = []
            dx_temp  = []
            vel_temp = []
            for i in tempp:
                acc_temp.append(accList[i[0]][i[1]:i[2]])
                dv_temp.append(dvList[i[0]][i[1]:i[2]])
                dx_temp.append(dxList[i[0]][i[1]:i[2]])
                vel_temp.append(velList[i[0]][i[1]:i[2]])
        si.savemat('CFtest_seq_' + str(count) + '.mat', {'acc': acc_temp, 'dv': dv_temp, 'dx': dx_temp, 'vel': vel_temp})



#si.savemat('CFtest_seq'+str()+'.mat', {'xi': xi,'yi': yi,'ui': ui,'vi': vi})
print 'end of process'





