# -*- coding: utf-8 -*-
__author__ = 'Yihuan Zhang and Qin Lin'
__email__ = "13yhzhang@tongji.edu.cn, q.lin@tudelft.nl"

import re

TRANSITIONS_REGEX = "^(?P<sst>\d+) (?P<sym>\w+) \[(?P<left>\d+), (?P<right>\d+)\]->(?P<dst>[-+]?\d+) \#(?P<fqr>\d+) p=(?P<prb>[0-9].?[0-9]+([eE][-+]?[0-9]+)?)$"


def __load(path):
    model = {}
    with open(path, "r") as mh:
        #mh.readline()
        for line in mh:
            m = re.search(TRANSITIONS_REGEX, line.strip())
            if m is not None:
                #print 'found'
                sstate = int(m.group("sst"))
                dstate = int(m.group("dst"))
                symbol = str(m.group("sym"))
                frequence = int(m.group("fqr"))
                leftGuard = int(m.group("left"))
                rightGuard = int(m.group("right"))
                probability = round(float(m.group("prb")),2)
                if frequence < 0: # minimum occ!!!
                    continue
                #print sstate, dstate, symbol, frequence, leftGuard, rightGuard, probability
                if sstate not in model:
                    model[sstate] = {}
                if dstate not in model[sstate]:
                    model[sstate][dstate] = {}
                model[sstate][dstate][(leftGuard, rightGuard, symbol)] = (probability,frequence)
    return model


def convert(rti, rti_dot):
    model = __load(rti)
    with open(rti_dot, "w") as ah:
        ah.write("digraph a {")
        ah.write('\n')
        for sstate in model:
            for dstate in model[sstate]:
                for item in model[sstate][dstate]:
                    ah.write("\nS" + str(sstate) + "->S" + str(dstate) + " [ label=\"" + "[" + str(item[0])
                     + ", " +str(item[1]) + "] " + str(item[2]) + ", " + str(model[sstate][dstate][item][0]) + ", #"
                      + str(model[sstate][dstate][item][1]) + "\"];")
                    print str(model[sstate][dstate][item][0])
                    print str(model[sstate][dstate][item][1])
                    #ah.write('\n')
        ah.write('\n}')
if __name__ == "__main__":
    rti = "solution.txt"
    dot = "rti_full.dot"
    convert(rti, dot)