# -*- coding: utf-8 -*-
__author__ = 'Yihuan Zhang and Qin Lin'
__email__ = "13yhzhang@tongji.edu.cn, q.lin@tudelft.nl"
'''
this code conducts clustering on symbolic strings
'''
import re
from jellyfish import jaro_distance
import scipy.cluster.hierarchy
import numpy as np
import matplotlib.pyplot as plt
from scipy.cluster import hierarchy
from scipy.cluster.hierarchy import fcluster

TRANSITIONS_REGEX = "^(?P<sst>\d+) (?P<sym>\w+) \[(?P<left>\d+), (?P<right>\d+)\]->(?P<dst>[-+]?\d+) \#(?P<fqr>\d+) p=(?P<prb>[0-9].?[0-9]+([eE][-+]?[0-9]+)?)$"


def NGram(symbol_frame):
    """ using N-gram to get all symbolic pattern
    input: symbol frames
    return: dictionary of state patterns counts: dict[length of patterns][patterns] = number of occurence
    top-k frequence sequence: e.g. [('2', '12', '2'), ('12', '2', '12')]
    all possible state pattern: e.g. [('2', '12', '2'), ('12', '2', '12')]
    """
    #statePatternCounter = {}
    frameSplit = []
    for i in xrange(len(symbol_frame)):
        frameSplit.append(symbol_frame[i].split(','))
    #print 'ppp', stateFrameSplit[0]
    patternCounterWithoutLen = {}
    stringLink = {} #mapping state sequence to symbolic sequence
    allPatternSet = set() 
    support = 2 #minimum length under consideration
    for i in xrange(support,5):#minimum and maximum of sliding window length 
        #statePatternCounter[i] = {}
        for j in xrange(len(frameSplit)):
            for k in xrange(len(frameSplit[j])-i+1):
                allPatternSet.add(','.join(frameSplit[j][k:k+i])) #dictionary, key[substate], value(count)
    return list(allPatternSet)
def CommonSubStringFinder(train_symbol_frame, pattern_symbol):
    """ find common substring in state frames
    input: symbolic frames, all possible symbolic subsequence, e.g. [('2', '12', '2'), ('12', '2', '12')]
    output: list of common substring of states
    """
    comon_sub_string = []
    for item in pattern_symbol:
        counter = 0
        for jtem in (train_symbol_frame):
            if (item) in (jtem):
                counter += 1
        if counter > len(train_symbol_frame)*0.025*10/8: #threshold to extract the common substring, len(train_symbol_frame)*0.025*10/8 or 100
            comon_sub_string.append(item)
    return comon_sub_string
def UniChar(stateSymbol):
    uniCharList = ['' for i in xrange(len(stateSymbol))]
    for i in xrange(len(stateSymbol)):
        for j in xrange(len(stateSymbol[i].strip().split(','))):
            uniCharList[i]+= str(stateSymbol[i].strip().split(',')[j])
    return uniCharList
def StringDistance(tupleArray, originStrings, warpStrings):
    ''' compute string similarity
    '''
    disMatrix = []
    disDict = {}
    for i in xrange(len(tupleArray[0])):
        left = tupleArray[0][i]
        right = tupleArray[1][i]
        dis = 1 - jaro_distance(unicode(warpStrings[left]), unicode(warpStrings[right]))
        disDict[tuple(set((originStrings[left], originStrings[right])))] = dis
        disMatrix.append(dis)
    return disMatrix, disDict
def WordsHierarchy(original, warp):
    '''hierarchy clustering of symbolic sequences
    input: symbolic sequences before clustering
    output: clustered sequence
    '''
    _ = np.triu_indices(len(original), 1)
    upper_triangle, dis_table = StringDistance(_,original,warp)
    z = scipy.cluster.hierarchy.linkage(upper_triangle, method='average')
    max_d = 0.58
    plt.figure()
    plt.ylabel('distance', fontsize=15)
    hierarchy.dendrogram(z, no_labels=True)
    plt.plot([0,10000], [max_d, max_d], ls='dashed', linewidth=1.5, c='k')
    plt.show()
    #max_cluster = 4
    cluster_ID = fcluster(z, max_d, criterion='distance') #based on maximum gap distance in hierarchy; list of cluster ID assigned, e.g. [1 1 1 1 1 1 2 1 1 1]
    #cluster_ID = fcluster(z, max_cluster, criterion='distance') #forced number of clusters
    alphabet_cluster = sorted(list(set(cluster_ID))) #[1,2]
    cluster_strings = [[] for x in xrange(len(alphabet_cluster))]
    for i in xrange(len(cluster_ID)):
        cluster_strings[cluster_ID[i]-1].append(original[i]) #[['162', '1621', '621', '1221', '1611', '2122', '1161', '216', '21221'], ['491']]
    with open(cluster_path, 'w') as tsh:
        for item in cluster_strings:
            tsh.write(';'.join(item))
            tsh.write('\n\n\n')
    return cluster_strings
def clusterAssign(newSequence, clusterString, disDict):
    '''compute which cluster the new sequence belongs to
    input: new sequence needed to be assigned cluster number, e.g. '1,6,2'; already clustered sequences, e.g. [['1,6,2', '1,6,2,1', '12,2,1'], ['4,9,1']]
    dictionary for looking up the distance between two sequences
    '''
    disPerCluster = []
    disSum = 0
    beforeAssign = [newSequence]
    #print beforeAssign, 'before'
    for i in xrange(len(clusterString)):
        for item in clusterString[i]:
            if tuple(set((newSequence, item))) in disDict:
                disSum += disDict[tuple(set((newSequence, item)))]
            else:
                #print beforeAssign
                #print item
                left = UniChar(beforeAssign)[0]
                right = UniChar([item])[0]
                disSum += (1 - jaro_distance(unicode(left), unicode(right)))
        disPerCluster.append(1.0*disSum/len(clusterString[i]))#distance between new sequence and each cluster
        disSum = 0
    #print disPerCluster
    closestClusterNumber = disPerCluster.index(min(disPerCluster))
    return closestClusterNumber
    # '''Elbow plotting'''
    # last = z[0:, 2]
    # last_rev = last[::-1]
    # idxs = np.arange(0, len(last))
    # x_label_pos = idxs[::-1]
    # _xticks = [i*50 for i in xrange(0,6)]
    # plt.xticks(_xticks, _xticks[::-1])
    # plt.tick_params(labelsize=15)
    # plt.xlabel('Iteration ID', fontsize=15)
    # plt.ylabel('Distance', fontsize=15)
    # plt.plot(idxs, last_rev, ls='dashed', linewidth=2, c='blue')
    # plt.show()
def SubListFind(targetList, patternList, clusterID, indexList):
    for i in xrange(len(targetList)-len(patternList)+1):
        if targetList[i:i+len(patternList)] == patternList:
            indexList.append(tuple((list(xrange(i,i+len(patternList))), clusterID)))
    return indexList
def EmptyAssign(unit_frame, clusterString, disDict):
    start, end  = 0, 0
    while start < len(unit_frame):
        if unit_frame[start] == -1:
            for i in xrange(len(unit_frame[start:])):
                if unit_frame[start+i] == -1:
                    end = start+i
                    if end == len(unit_frame)-1:
                        filterSeq = unit_frame[start:]
                        filterStr = ",".join(str(x) for x in filterSeq)
                        ID = clusterAssign(filterStr, clusterString, disDict)
                        unit_frame[start:] = [ID for k in xrange(end+1-start)]
                        start = end+1
                else:
                    filterSeq = unit_frame[start:end+1]
                    filterStr = ",".join(str(x) for x in filterSeq)
                    ID = clusterAssign(filterStr, clusterString, disDict)
                    unit_frame[start:end+1] = [ID for k in xrange(end+1-start)]
                    start = end+1
                    break
        else: start += 1
        if start >= len(unit_frame): break
    return unit_frame
def Segmentation(frame, cluster_string, dis_dict):
    '''Assign cluster ID to each item in the symbolic frame, using already clustered strings
    input: 1. list of symbolic frames
    even if the input is a single frame, frame 0 for instance, you need to call:
    segmentation(stateFrame[0:1], clusteredStateSequence, dis_table)
    2. aready clustered symbolic strings
    3. distance tabel for looking up
    output print every assigned state frame
    '''
    return_frame = []
    empty_num = 0 #not yet assigned cluster ID
    total_num = 0
    for i in xrange(len(frame)):
        index_list = []
        for j in xrange(len(cluster_string)):
            for jtem in cluster_string[j]:
                index_list = SubListFind(frame[i].split(','), jtem.split(','), j, index_list)
        index_list_sort = sorted(index_list, key=lambda index_item : len(index_item[0]))
        unit_frame = [-1 for k in xrange(len(frame[i].split(',')))]
        for l in xrange(len(index_list_sort)):
            for ltem in index_list_sort[l][0]:
                unit_frame[ltem] = index_list_sort[l][1]
        '''
        compute the proportion of infrequent data
        '''
        for item in unit_frame:
            if item == -1:
                empty_num += 1
        total_num += len(unit_frame)
        return_frame.append(EmptyAssign(unit_frame, cluster_string, dis_dict))
    print empty_num, total_num, 'proportion'
    return return_frame

def Redu(symbol_frame):
    '''
    time reductance of symbolic strings, aaabbb->ab
    '''
    reduced = []
    length = []
    #print symbol_frame[30]
    #print symbol_frame[31]
    #print symbol_frame[33]
    for i in xrange(len(symbol_frame)):
        temp = str(symbol_frame[i][0])
        splitF = symbol_frame[i].split(',')
        #length.append(len(splitF))
        #print splitF
    #     temp = str(splitF[0])
        # if len(splitF)<2:
        #     print 'fff', splitF
        #     continue
        for j in xrange(1,len(splitF)):
            #print slitF[i]
            if splitF[j] != splitF[j-1]:
                temp += str(splitF[j])
            else:
                continue
        length.append(len(temp))
        reduced.append(','.join(temp))
    print min(length), max(length)
    return reduced
def CompleteAssign(cluster_list, unique_symbol, repeat_symbol):
    '''assign cluster ID to roginal symbolic strings (same sampling interval with time series)
    unique_symbol: timed string removing time
    repaet: orginal strings
    '''
    complete = []
    for i in xrange(len(repeat_symbol)):
        unit_frame = [-1 for k in xrange(len(repeat_symbol[i].split(',')))]
        change = set()
        if len(cluster_list[i]) == 1:
            for x in xrange(len(unit_frame)):
                unit_frame[x]=cluster_list[i][0]
            complete.append(unit_frame)
            continue
        change.add(0)
        for j in xrange(1, len(repeat_symbol[i].split(','))-1):
            if repeat_symbol[i].split(',')[j] != repeat_symbol[i].split(',')[j-1]:
                change.add(j)
            #print j
        change_list = list(change)
        for k in xrange(len(change_list)):
            if k != len(change_list)-1:
                unit_frame[change_list[k]:change_list[k+1]] = [cluster_list[i][k] for y in xrange(change_list[k+1]-change_list[k])]
            else:
                for z in xrange(change_list[-1],len(repeat_symbol[i].split(','))):
                    unit_frame[z]=cluster_list[i][-1]
        complete.append(unit_frame)
    '''
    write completely assigned symbolic frames into file
    '''
    with open(tar_path, 'w') as sch:
        for item in complete:
            sentence = ''
            for jtem in item:
                sentence+=str(jtem)
            sch.write(','.join(sentence))
            sch.write('\n')
            #sch.flush()
    return complete
if __name__ == "__main__":
    symbolic_path = "data/US101_1/US101_1_R1/train_symbolseq_US101_1.txt" #input: symbolic strings with same frequency as original data
    tar_path = "data/US101_1/US101_1_R1/assign_symbol_US101_1.txt" #output: cluster assigned sequence
    cluster_path = "data/US101_1/US101_1_R1/train_cluster_symbolic_US101_1.txt" #output: cluster of symbolic sequence
    frequent_substring_cluster_path = "data/US101_1/US101_1_R1/symbolic_substring_cluster_US101_1.txt"
    training_start = 0
    training_end = 2940#I80-1: 3279, I80-2:2187, I80-3:2069, US101-1:2940, US101-2:1940, US101-3: 1756
    all_symbol_list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
    with open(symbolic_path, 'r') as symbol_h:
        symbol_frame = []
        length = []
        for line in symbol_h:
            symbol_frame.append(','.join(line.strip()))
            length.append(len(line.strip()))
    redu_symbol_frame = Redu(symbol_frame)#
    ctest = 0
    pattern_symbol = NGram(redu_symbol_frame[training_start:training_end]) # get N-gram of all possible subsequence
    frequent_common = CommonSubStringFinder(redu_symbol_frame[training_start:training_end], pattern_symbol)
    words = []
    for item in frequent_common:
        words.append((item))
    clustered_symbolic_sequence = WordsHierarchy(words, UniChar(words))
    with open(frequent_substring_cluster_path, 'w') as cph:
        for item in clustered_symbolic_sequence:
            #words.append((item[0]))
            cph.write(';'.join(item))
            cph.write('\n\n\n')
    print len(clustered_symbolic_sequence)
#################based on clustered state substrings, we want to put states into group (no overlapping states), added for TITS R1##########
    cluster_symbol_counter = dict()
    all_symbol_seen_set = set()
    #print len(clusteredstate_sequence)
    for i in xrange(len(clustered_symbolic_sequence)):
        for j in xrange(len(clustered_symbolic_sequence[i])):
            string_temp = clustered_symbolic_sequence[i][j].split(',')
            for k in xrange(len(string_temp)):
                all_symbol_seen_set.add(str(string_temp[k]))
    all_symbol_seen_list = list(all_symbol_seen_set)#states in common substrings
    print all_symbol_seen_list
    for i in xrange(len(all_symbol_seen_list)):
        for j in xrange(len(clustered_symbolic_sequence)):
            symbol = str(all_symbol_seen_list[i])
            category = j
            if symbol not in cluster_symbol_counter:
                cluster_symbol_counter[symbol] = {}
            if category not in cluster_symbol_counter[symbol]:
                cluster_symbol_counter[symbol][category] = {}
            cluster_symbol_counter[symbol][category] = 0
    print cluster_symbol_counter

    for i in xrange(len(clustered_symbolic_sequence)):
        for j in xrange(len(clustered_symbolic_sequence[i])):
            string_temp = clustered_symbolic_sequence[i][j].split(',')
            for k in xrange(len(string_temp)):
                cluster_symbol_counter[str(string_temp[k])][i] += 1
    print cluster_symbol_counter.keys()
    symbol_ID = dict()
    for item in cluster_symbol_counter.keys():
        max_id = 0
        for j in xrange(len(clustered_symbolic_sequence)):
            if cluster_symbol_counter[item][j]>cluster_symbol_counter[item][max_id]:
                max_id = j
        symbol_ID[item] = max_id
        print item, max_id
    ID_symbol = dict()
    for item in symbol_ID.keys():
        symbol = item
        ID = symbol_ID[item]
        if ID not in ID_symbol:
            ID_symbol[ID] = set()
        ID_symbol[ID].add(symbol)
    print ID_symbol
    print symbol_ID
    #########manuallly correct##########
    #ID_symbol = {0: set(['d']), 1: set(['a', 'j']), 2: set(['c', 'e']), 3: set(['b', 'f'])}#I80-1
    #ID_symbol = {0: set(['a', 'h']), 1: set(['i', 'j']), 2: set(['c', 'b', 'f']), 3: set(['d', 'g'])}#I80-1
    #ID_symbol = {0: set(['c']), 1: set(['a', 'h', 'e', 'g']), 2: set(['b']), 3: set(['j']), 4: set(['d'])}#I80-2
    #ID_symbol = {0: set(['e']), 1: set(['b', 'd']), 2: set(['j', 'g']), 3: set(['a', 'c', 'f']), 4: set(['h'])}#US101-1, 0.45
    ID_symbol = {0: set(['c', 'e', 'f']), 1: set(['b', 'd']), 2: set(['j', 'g']), 3: set(['h']), 4: set(['a'])}#US101-1, 0.58
    #ID_symbol = {0: set(['h', 'd']), 1: set(['i', 'j']), 2: set(['e']), 3: set(['f']), 4: set(['c']), 5: set(['a', 'g'])}#I80-3, mad_d 0.39
    #ID_symbol = {0: set(['j']), 1: set(['h', 'e']), 2: set(['a', 'c', 'd', 'g']), 3: set(['f'])}#US101-2, 0.4
    #ID_symbol = {0: set(['a', 'g']), 1: set(['d']), 2: set(['h']), 3: set(['f']), 4: set(['b']), 5: set(['c']), 6: set(['j']), 7: set(['i'])}#US101-3.0.3
    #ID_state = {0: set(['15', '5', '28', '7', '23']), 1: set(['9', '3', '4']), 2: set(['25', '12', '27', '22', '30']), 3: set(['11', '26', '20', '14', '17']), 4: set(['13', '21', '16']), 5: set(['1', '0', '2']), 6: set(['10', '6']), 7: set(['19', '8', '18', '29'])}##us101-3
    symbol_ID = dict()
    for item in ID_symbol.keys():
        for jtem in ID_symbol[item]:
            symbol_ID[jtem] = item
    print ID_symbol
    print symbol_ID

    missing_symbol = list()#states in the model but not in the frequent substrings clustering
    for item in all_symbol_list:
        if str(item) not in all_symbol_seen_list:
            missing_symbol.append(item)
    print missing_symbol
    for item in missing_symbol:
        symbol_ID[str(item)] = len(ID_symbol.keys())#add one more cluster to missing (infrequent) state

    assign_symbol_frame = list()
    for i in xrange(len(symbol_frame)):
        symbol_frame_each = symbol_frame[i].split(',')
        symbol_ID_each = list()
        for j in xrange(len(symbol_frame_each)):
            symbol_ID_each.append(str(symbol_ID[symbol_frame_each[j]]))
        assign_symbol_frame.append(symbol_ID_each)
        symbol_ID_each = list()
    ##write assigned state sequences into file
    with open(tar_path, 'w') as aph:
        for item in assign_symbol_frame:
            #for jtem in item:
            sentence = ','.join(item)
            aph.write(sentence)
            #sentence = ''
            aph.write('\n')
    
    #_ = np.triu_indices(len(words), 1)
    #upper_triangle, dis_table = StringDistance(_, words, UniChar(words))
    #cluster_list = Segmentation(redu_symbol_frame, clustered_symbolic_sequence, dis_table)
    #print clustered_symbolic_sequence
    #final_assign_sequence = CompleteAssign(cluster_list, redu_symbol_frame, symbol_frame)