1. string_generator_TITS_R1.py:
convert NGSIM .mat file into symbolic strings readable by RTI algorithm
2. train a timed automaton in RTI, get the solution.txt
3. pattern_mining_state_sequence_R1.py
get the state sequences, do the state clustering, assign cluster ID to each state
4. All_in_One.py
aggregate original numeric NGSIM data according to the last step's results
5. Training_all_in_one.m
use differential generic algorithm to train and get parameters of the car-following model