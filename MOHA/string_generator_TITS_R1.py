# -*- coding: utf-8 -*-
__author__ = 'Yihuan Zhang and Qin Lin'
__email__ = "13yhzhang@tongji.edu.cn, q.lin@tudelft.nl"
'''
this code makes the conversion from time series data into symbolic strings/timed strings, IEEE TITS revision round 1
training and testing set
'''
import matplotlib.pyplot as plt
import scipy.io as si
import numpy as np
from sklearn.cluster import KMeans
from scipy.cluster.vq import vq
from scipy.spatial import distance
from sklearn import linear_model
aOffset = 97 #ascci convertion from int to char
def KmeansLabelling(center_model, pre_labelling):
	labels,_ = vq(pre_labelling, center_model)
	return chr(aOffset+labels[0])
def KmeansCluster(pre_discrete, num_cluster, init = 'random', random_state = 10):
	clf = KMeans(n_clusters = num_cluster, init = 'random', random_state = 10)
	clf.fit(pre_discrete)
	centroids = clf.cluster_centers_
	kmeans_dict = {}
	kmeans_dict_values = []
	for i in xrange(len(centroids)):
		kmeans_dict[i] = centroids[i]
	alphabet_size = len(kmeans_dict)
	alphabet = kmeans_dict.keys()
	#print kmeans_dict
	return alphabet_size, centroids
def LMethod(WSS_list, max_number_cluster):
    '''
    see the reference: Learning States and Rules for Time Series Anomaly Detection
    '''
    MSE_list = []
    for i in xrange(1, max_number_cluster-3):#num 3-max_number_cluster-2
        L_x = np.reshape(list(xrange(2, i+3)), (len(list(xrange(2, i+3))), 1))
        L_y = np.reshape(WSS_list[0:i+1], (len(WSS_list[0:i+1]), 1))
        R_x = np.reshape(list(xrange(i+3, max_number_cluster+1)),
                         (len(list(xrange(i+3, max_number_cluster+1))),1 ))
        R_y = np.reshape(WSS_list[i+1:], (len(WSS_list[i+1:]),1))
        #print L_x, L_y
        #print R_x, R_y
        regr = linear_model.LinearRegression()
        regr.fit(L_x, L_y)
        L_mse = np.mean((regr.predict(L_x) - L_y) ** 2)
        #print L_mse
        regr = linear_model.LinearRegression()
        regr.fit(R_x, R_y)
        R_mse = np.mean((regr.predict(R_x) - R_y) ** 2)
        print (i+1)*L_mse/(max_number_cluster-1)+(max_number_cluster-i-2)*R_mse/(max_number_cluster-1), 'mse'
        MSE_list.append(L_mse+R_mse)
    return MSE_list.index(min(MSE_list))+3
def select_optimal(scaled_instance_train, max_number_cluster, origin_period, dimension):
    sum_inner_var_list = []
    for candidate in xrange(2, max_number_cluster + 1):  # possible number of clusters, max=20
        print 'now'+str(candidate)
        alphabet_size, center = KmeansCluster(scaled_instance_train, candidate)  # train
        inner_cluster_var = [0 for i in xrange(candidate)]
        sum_inner_var = 0.0
        for i in xrange(len(origin_period)):
            for j in xrange(len(origin_period[i])):
                symbol = KmeansLabelling(center,
                    np.reshape(origin_period[i][j], (1, dimension)))
                inner_cluster_var[ord(symbol) - aOffset] += distance.euclidean(center[ord(symbol) - aOffset],
                            np.reshape(origin_period[i][j], (1, dimension)))
        for i in xrange(candidate):
            sum_inner_var += inner_cluster_var[i]
        sum_inner_var_list.append(sum_inner_var)
        sum_inner_var = 0
    optimal_num_cluster = LMethod(sum_inner_var_list, max_number_cluster)
    #print sum_inner_var_list
    #print optimal_num_cluster, 'optimal_num_cluster'
    print optimal_num_cluster, 'optimal_num_cluster'
    return optimal_num_cluster
#convert normal string to rti type string, e.g. aaabbbcdd->(a,0),(b,3),(c,3),(d,1)
def RTIConvert(normal_str):
	rti_str, uni_time, uni_event= [],[],[]
	for i in xrange(len(normal_str)):
		if i == 0:
			uni_time.append(0)
			uni_event.append(normal_str[0])
		else:
			if normal_str[i]!=normal_str[i-1]:
				uni_time.append(i)
				uni_event.append(normal_str[i])
	for i in xrange(len(uni_event)):
		if i==0:
			rti_str.append((uni_event[0],0))
		else:
			rti_str.append((uni_event[i],uni_time[i]-uni_time[i-1]))
	return rti_str
# write the rti sequence into files
def RTIWriter(rti_sequence, num_sequence, alphabet_size, dst):
	dst_head = open(dst, 'w')
	dst_head.write(str(num_sequence)+' '+str(alphabet_size))
	dst_head.write('\n')
	for i in xrange(num_sequence):
		dst_head.write(str(len(rti_sequence[i]))+' ')
		for j in xrange(len(rti_sequence[i])):
			dst_head.write(str(rti_sequence[i][j][0])+' '+str(rti_sequence[i][j][1])+' ')
		dst_head.write('\n')
	dst_head.flush()
src = 'data/NGSIM/train_data_ngsim.mat'# original data
dst_rti_train = 'data/I80_1/rti_train_I80_1.txt'# output file-rti train
dst_rti_full = 'data/I80_1/rti_full_I80_1.txt'# output file-rti full
dst_symbolic = 'data/I80_1/train_symbolseq_I80_1.txt' # output symbolic
data = si.loadmat(src)
acc_list = data['acc'][0]#a list, each element of the list contains one car following period, accelaration
print data['acc'][0][2].size, 'test'
dv_list = data['dv'][0]#ralative speed
dx_list = data['dx'][0]#relative distance
vel_list = data['vel'][0]
num_cluster = 10
training_frame_start = 0
training_frame_end = int(4099*1.0) #train, I80_combined: 6800 I80-1 (train_data_ngsim): 3279, I80-2:2187, I80-3:2069, US101-1:2940, US101-2:1940, US101-3: 1756
MAX_NUM_CLUSTER = 15
DIMENSION = 3
num_sequence = len(acc_list)# number of car following periods/frames
#print num_sequence, 'len'
num_instance = 0#number of data points (3-dimension)
num_instance_train = 0
for i in xrange(num_sequence):
	num_instance += len(acc_list[i])
for i in xrange(training_frame_end):
	num_instance_train += len(acc_list[i])
instance = np.zeros((num_instance, 3))# the matrix used for clustering, each instance contains 3 features: dv, dx, vel
k = 0
time_temp = []
period = []#a list containing 4099 elements. Each element is one period of following. Each period it is multivariate time serie.
for i in xrange(num_sequence):
	for j in xrange(len(acc_list[i])):
		instance[k+j][0] = float(dv_list[i][j])
		instance[k+j][1] = float(dx_list[i][j])
		instance[k+j][2] = float(vel_list[i][j])
		time_temp.append([ float(dv_list[i][j]), float(dx_list[i][j]), float(vel_list[i][j])])
	period.append(time_temp)
	time_temp = []
	k = k+len(acc_list[i])
print period[0], 'period'

# optimal_num_cluster = select_optimal(instance[0:num_instance_train], 
#                 MAX_NUM_CLUSTER, period, DIMENSION)
# print optimal_num_cluster
#alphabet_size, center = KmeansCluster(instance[training_start:], num_cluster) #symbol clustering only on full data
alphabet_size, center = KmeansCluster(instance[0:num_instance_train], num_cluster) #train
center =   ([[  7.87678858e-01, 5.78714836e+01,   1.36950600e+01],
           [  3.02970280e+00,   3.61310525e+01,   1.05424942e+01],
           [ -2.88274327e+00,   1.56395870e+01,   7.74508749e+00],
           [  4.82479035e+00,   1.55531956e+01,   5.94254846e+00],
           [ -3.12855987e+00,   2.04180145e+02,   1.94124447e+01],
           [ -9.82077737e-01,   9.60900744e+01,   1.72569637e+01],
           [ -9.67519506e+00,   3.97474969e+01,   1.29985277e+01],
           [  2.52215728e+00,   2.40075123e+01,   8.38240630e+00],
           [ -7.02308911e+00,   2.44784548e+01,   1.01049181e+01],
           [  1.20296756e-01,   1.01355039e+01,   4.12085824e+00]])
print center, "center"
sequence_temp = []
sequence = []
inner_cluster_var = [0 for i in xrange(num_cluster)]
inner_cluster_var_num = [0 for i in xrange(num_cluster)]
sum_inner_var = 0.0
length_per_frame = []
data_point_num = 0
for i in xrange(len(period)):
	for j in xrange(len(period[i])):
		symbol = KmeansLabelling(center, np.reshape(period[i][j],(1,3)))
		sequence_temp.append(symbol)
		inner_cluster_var[ord(symbol)-aOffset] += distance.euclidean(center[ord(symbol)-aOffset], period[i][j])
		data_point_num += 1
	length_per_frame.append(len(period[i]))
	sequence.append(sequence_temp)
	sequence_temp = []
#print np.min(length_per_frame), "min length of per frame"
#print np.max(length_per_frame), "max length of per frame"
#print data_point_num, "total data points"
'''
write symbolic sequences into file (full, including train and test) 
'''
f = open(dst_symbolic, 'w')
for item in sequence:
   for item1 in item:
       f.write(str(item1))
   f.write('\n')
f.close()

RTI_train_sequence = []
RTI_full_sequence = []
for item in sequence[training_frame_start:training_frame_end]: #train
#for item in sequence[training_start:]:full
    RTI_train_sequence.append(RTIConvert(item))
for item in sequence:
    RTI_full_sequence.append(RTIConvert(item))
RTIWriter(RTI_train_sequence, len(RTI_train_sequence), alphabet_size, dst_rti_train)
RTIWriter(RTI_full_sequence, len(RTI_full_sequence), alphabet_size, dst_rti_full)