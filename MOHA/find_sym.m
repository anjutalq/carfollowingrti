function cur_sym = find_sym(dv_sim,dx_sim,vf_sim,sym_mat);
%% symbol codebook 10 symbols abcdefghij dv dx v
% sym_mat = {[0.0804 17.3146 11.6458],[0.1224 10.9766 3.5801],...
%            [0.4202 18.5242 8.1155],[0.3367 22.5688 9.5268],...
%            [0.2302 9.5087 10.3659],[-0.0256 20.5214 11.9276],...
%            [0.5321 12.6049 7.0281],[0.8776 28.4871 12.9641],...
%            [0.2665 14.2914 9.8069]};    
num = length(sym_mat);
dis = [];
for i = 1:num
    cen = sym_mat{i};
    dis(i) = sqrt((cen(1)-dv_sim)^2+(cen(2)-dx_sim)^2+(cen(3)-vf_sim)^2);
end
[mindis cur_sym] = min(dis);
end