# -*- coding: utf-8 -*-
__author__ = 'Yihuan Zhang and Qin Lin'
__email__ = "13yhzhang@tongji.edu.cn, q.lin@tudelft.nl"

import re
from jellyfish import jaro_distance
import scipy.cluster.hierarchy
import numpy as np
import matplotlib.pyplot as plt
from scipy.cluster import hierarchy
from scipy.cluster.hierarchy import fcluster

TRANSITIONS_REGEX = "^(?P<sst>\d+) (?P<sym>\w+) \[(?P<left>\d+), (?P<right>\d+)\]->(?P<dst>[-+]?\d+) \#(?P<fqr>\d+) p=(?P<prb>[0-9].?[0-9]+([eE][-+]?[0-9]+)?)$"


def __load(path):
    model = {}
    with open(path, "r") as mh:
        #mh.readline()
        for line in mh:
            m = re.search(TRANSITIONS_REGEX, line.strip())
            if m is not None:
                #print 'found'
                sstate = int(m.group("sst"))
                dstate = int(m.group("dst"))
                symbol = str(m.group("sym"))
                frequence = int(m.group("fqr"))
                leftGuard = int(m.group("left"))
                rightGuard = int(m.group("right"))
                probability = float(m.group("prb"))
                if sstate not in model:
                    model[sstate] = {}
                if symbol not in model[sstate]:
                    model[sstate][symbol] = {}
                model[sstate][symbol][(leftGuard, rightGuard)] = dstate
    return model
def SequenceConvert(m, rti_input_path, state_frame_path, string_frame_path):
    """Get the state sequence frames
    input:
        model, rti format input data
    return:
        state sequence, e.g. ['0,3,2,1', '0,4']
        string sequence with time guard, e.g. [[('d', (0, 542)), ('c', (0, 542)), ('j', (0, 542))], [('i', (0, 542))]]
    """
    state_sequence, string_sequence = [], []
    state_sequence_temp, string_sequence_temp = [], []
    nofind = 0
    length = []
    with open(rti_input_path, "r") as dh:
        dh.readline()
        #line_num+=1
        for line in dh:
            #line_num+=1
            #if line_num > 1:
            #    break
            #print line
            line_without_head = line.strip().split(' ')[1:]
            current_state = 0
            #state_sequence_temp.append(current_state)#modify
            state_sequence_temp.append('0')
            for i in xrange(len(line_without_head)/2):
                symbol_unit = str(line_without_head[i*2])
                #print symbol_unit, 's'
                #print current_state, 'cs'
                if current_state not in m or symbol_unit not in m[current_state].keys():
                    nofind += 1
                    #print '?'
                    break
                if current_state == None or current_state == -1:
                    nofind += 1
                    #print '??'
                    break
                time_unit = int(line_without_head[i*2+1])
                if QueryTimeGuard(m, current_state, symbol_unit, time_unit)==None:
                    nofind += 1
                    break
                guard_unit = QueryTimeGuard(m, current_state, symbol_unit, time_unit)
                #print guard_unit, 'guard'
                current_state = m[current_state][symbol_unit][guard_unit]
                if current_state not in m:
                    nofind += 1
                    break
                else:
                    temp = []
                    temp.append(symbol_unit)
                    temp.append(guard_unit)
                    string_sequence_temp.append(tuple(temp))
                    temp = []
                    #state_sequence_temp.append(current_state)#modify
                    state_sequence_temp.append(str(current_state))
            state_sequence.append(','.join(state_sequence_temp))
            length.append(len(state_sequence_temp))
            string_sequence.append(string_sequence_temp)
            state_sequence_temp = []
            string_sequence_temp = []
    print min(length),max(length), 'min-max length'
    with open(state_frame_path, "w") as h1:
        for item in state_sequence:
            h1.write(str(item))
            h1.write('\n')
    with open(string_frame_path, "w") as h2:
        for item in string_sequence:
            h2.write(str(item))
            h2.write('\n')
    #print nofind/4099.0, 'missing' # some training data are not fired by the model. need reflection!
    return state_sequence, string_sequence
def QueryTimeGuard(m, current_state, symbol_unit, time_unit):
    """query: the time inside which time guard 
    input: model, current state, symbol, time unit. e.g. input 50 output guard [0:542]
    return:
        time guard tuple, e.g. (0,542)
    """
    possibleGuard = set()
    for item in m[current_state][symbol_unit].keys():
        possibleGuard.add((item[0],item[1]))
    for item in list(possibleGuard):
        if int(time_unit) <= int(item[1]) and  int(time_unit) >= int(item[0]):
            return item
        else:
            continue
def NGram(train_state_frame, train_string_frame):
    """ using N-gram to get all state pattern
    input: traing state frames
    return: dictionary of state patterns counts: dict[length of state patterns][state patterns] = number of occurence
    top-k frequence sequence: e.g. [('2', '12', '2'), ('12', '2', '12')]
    all possible state pattern: e.g. [('2', '12', '2'), ('12', '2', '12')]
    """
    #statePatternCounter = {}
    state_frame_split = []
    for i in xrange(len(train_state_frame)):
        state_frame_split.append(train_state_frame[i].split(','))
    state_string_link = {} #mapping state sequence to symbolic sequence
    all_state_pattern_set = set() 
    support = 3
    for i in xrange(support,10):#minimum and maximum (36) of sliding window length 
        #statePatternCounter[i] = {}
        for j in xrange(len(state_frame_split)):
            for k in xrange(len(state_frame_split[j])-i+1):
                if tuple(state_frame_split[j][k:k+i]) not in state_string_link:
                    state_string_link[tuple(state_frame_split[j][k:k+i])] = []
                if train_string_frame[j][k:k+i-1] not in state_string_link[tuple(state_frame_split[j][k:k+i])]:
                    state_string_link[tuple(state_frame_split[j][k:k+i])].append(train_string_frame[j][k:k+i-1])
                all_state_pattern_set.add(','.join(state_frame_split[j][k:k+i])) #dictionary, key[substate], value(count)
    return list(all_state_pattern_set), state_string_link
def CommonSubStringFinder(train_state_frame, pattern_state):
    """ find common substring in state frames
    input: state frames, all possible state patterns, e.g. [('2', '12', '2'), ('12', '2', '12')]
    output: sorted list of common substring of states, e.g. [(('1', '6', '2'), 1033), (('1', '6', '2', '1'), 947)]
    """
    dict_common_sub_string = {}
    for item in pattern_state:
        counter = 0
        for jtem in (train_state_frame):
            if (item) in (jtem):
                counter += 1
        if counter > len(train_state_frame)*0.025*10/8: #threshold to extract the common substring; len(train_state_frame)*0.025*10/8 or 100
            dict_common_sub_string[item] = counter
    sorted_common_sub_string = sorted(dict_common_sub_string.iteritems(), key=lambda d:d[1], reverse=True)
    return sorted_common_sub_string
def UniChar(stateSymbol):
    uni_char_list = ['' for i in xrange(len(stateSymbol))]
    for i in xrange(len(stateSymbol)):
        for j in xrange(len(stateSymbol[i].strip().split(','))):
            uni_char_list[i]+=chr(int(stateSymbol[i].strip().split(',')[j])+65)
    return uni_char_list
def StringDistance(tuple_array, origin_strings, warp_strings):
    ''' compute string similarity
    '''
    dis_matrix = []
    dis_dict = {}
    for i in xrange(len(tuple_array[0])):
        left = tuple_array[0][i]
        right = tuple_array[1][i]
        #print unicode(inputClustering[left])
        #print unicode(inputClustering[right])
        dis = 1 - jaro_distance(unicode(warp_strings[left]), unicode(warp_strings[right]))
        dis_dict[tuple(set((origin_strings[left], origin_strings[right])))] = dis
        dis_matrix.append(dis)
    return dis_matrix, dis_dict
def WordsHierarchy(original, warp):
    '''hierarchy clustering of state sequences
    input: state sequences before clustering, e.g. ['162', '1621', '621']
    output: clustered sequence
    '''
    _ = np.triu_indices(len(original), 1)
    upper_triangle, dis_table = StringDistance(_,original,warp)
    z = scipy.cluster.hierarchy.linkage(upper_triangle, method='average')
    plt.figure()
    #plt.title('Hierarchical Clustering Dendrogram')
    #plt.xlabel('sample index')
    plt.ylabel('distance', fontsize=15)
    hierarchy.dendrogram(z, no_labels=True)
    max_d = 0.963
    plt.plot([0,10000], [max_d, max_d], ls='dashed', linewidth=1.5, c='k')
    plt.show()
    max_cluster = 6
    cluster_ID = fcluster(z, max_d, criterion='distance') #based on maximum gap distance in hierarchy; list of cluster number assigned, e.g. [1 1 1 1 1 1 2 1 1 1]
    #cluster_ID = fcluster(z, max_cluster, criterion='distance') #forced number of clusters
    alphabet_cluster = sorted(list(set(cluster_ID))) #[1,2]
    cluster_strings = [[] for x in xrange(len(alphabet_cluster))]
    for i in xrange(len(cluster_ID)):
        cluster_strings[cluster_ID[i]-1].append(original[i]) #[['162', '1621', '621', '1221', '1611', '2122', '1161', '216', '21221'], ['491']]
    #print cluster_strings, 'ttt'
    return cluster_strings
def ClusterAssign(new_sequence, cluster_string, dis_dict):
    '''compute which cluster the new sequence belongs to
    input: new sequence needed to be assigned cluster number, e.g. '1,6,2'; already clustered sequences, e.g. [['1,6,2', '1,6,2,1', '12,2,1'], ['4,9,1']]
    dictionary for looking up the distance between two sequences
    '''
    dis_per_cluster = []
    dis_sum = 0
    before_assign = [new_sequence]
    #print before_assign, 'before'
    for i in xrange(len(cluster_string)):
        for item in cluster_string[i]:
            if tuple(set((new_sequence, item))) in dis_dict:
                dis_sum += dis_dict[tuple(set((new_sequence, item)))]
            else:
                #print before_assign
                #print item
                left = UniChar(before_assign)[0]
                right = UniChar([item])[0]
                dis_sum += (1 - jaro_distance(unicode(left), unicode(right)))
        dis_per_cluster.append(1.0*dis_sum/len(cluster_string[i]))#distance between new sequence and each cluster
        dis_sum = 0
    closest_cluster_ID = dis_per_cluster.index(min(dis_per_cluster))
    return closest_cluster_ID
    # '''Elbow plotting'''
    # last = z[0:, 2]
    # last_rev = last[::-1]
    # idxs = np.arange(0, len(last))
    # x_label_pos = idxs[::-1]
    # _xticks = [i*50 for i in xrange(0,6)]
    # plt.xticks(_xticks, _xticks[::-1])
    # plt.tick_params(labelsize=15)
    # plt.xlabel('Iteration ID', fontsize=15)
    # plt.ylabel('Distance', fontsize=15)
    # plt.plot(idxs, last_rev, ls='dashed', linewidth=2, c='blue')
    # plt.show()
def sub_list_find(target_list, pattern_list, cluster_ID, index_list):
    for i in xrange(len(target_list)-len(pattern_list)+1):
        if target_list[i:i+len(pattern_list)] == pattern_list:
            index_list.append(tuple((list(xrange(i,i+len(pattern_list))), cluster_ID)))
    return index_list
def EmptyAssign(unit_frame, cluster_string, dis_dict):
    start, end  = 0, 0
    while start < len(unit_frame):
        if unit_frame[start] == -1:
            for i in xrange(len(unit_frame[start:])):
                if unit_frame[start+i] == -1:
                    end = start+i
                    if end == len(unit_frame)-1:
                        filter_seq = unit_frame[start:]
                        filter_str = ",".join(str(x) for x in filter_seq)
                        ID = ClusterAssign(filter_str, cluster_string, dis_dict)
                        unit_frame[start:] = [ID for k in xrange(end+1-start)]
                        start = end+1
                else:
                    filter_seq = unit_frame[start:end+1]
                    filter_str = ",".join(str(x) for x in filter_seq)
                    ID = ClusterAssign(filter_str, cluster_string, dis_dict)
                    unit_frame[start:end+1] = [ID for k in xrange(end+1-start)]
                    start = end+1
                    break
        else: start += 1
        if start >= len(unit_frame): break
    return unit_frame
def Segmentation(frame, cluster_string, dis_dict):
    '''Assign cluster number to each item in the state frame
    input: 1. list of state frames e.g. ['1,6,2', '1,6,2,1']
    even if the input is a single frame, frame 0 for instance, you need to call:
    segmentation(stateFrame[0:1], clusteredstate_sequence, disTable)
    2. aready clustered state_sequences
    3. distance Tabel for looking up
    output print every assigned state frame
    '''
    empty_num = 0
    total_num = 0
    assign_frame = []
    #with open(assigned_cluster_sequence, 'w') as acs:
    for i in xrange(len(frame)):
        index_list = []
        for j in xrange(len(cluster_string)):
            for jtem in cluster_string[j]:
                index_list = sub_list_find(frame[i].split(','), jtem.split(','), j, index_list)
        index_list_sort = sorted(index_list, key=lambda index_item : len(index_item[0]))
        unit_frame = [-1 for k in xrange(len(frame[i].split(',')))]
        for l in xrange(len(index_list_sort)):
            for ltem in index_list_sort[l][0]:
                unit_frame[ltem] = index_list_sort[l][1]
        '''
        compute the proportion of infrequent data
        '''
        for item in unit_frame:
            if item == -1:
                empty_num += 1
        total_num += len(unit_frame)
        assign_frame.append(EmptyAssign(unit_frame, cluster_string, dis_dict))
    print empty_num, total_num, 'proportion'
    return assign_frame
if __name__ == "__main__":
    rti_solution_path = "data/I80_1/solution.txt" #input: rti solution
    rti_input_path = "data/I80_1/rti_full_I80_1_1.0.txt" #input: full rti data
    state_path = "data/I80_1/frequent_state_sequence_I80_1_1.0.txt" #output: frequent common state patterns
    cluster_path = 'data/I80_1/train_cluster_state_I80_1_1.0.txt' #output: cluster state string
    assigned_path = 'data/I80_1/assign_state_I80_1_1.0.txt' #output: complete frames assigned cluster id
    state_frame_path = 'data/I80_1/state_frame_I80_1.0.txt'#output: full state frame
    string_frame_path = 'data/I80_1/string_frame_I80_1.0.txt'#output: full string frame
    m = __load(rti_solution_path)
    all_states_list = m.keys()
    print all_states_list
    train_start = 0
    train_end = 4099
    test_start = 4099
    state_frame, string_frame = SequenceConvert(m, rti_input_path, state_frame_path, string_frame_path)
    train_state_frame = state_frame[train_start:train_end]#
    train_string_frame = string_frame[train_start:train_end]#
    pattern_state, link = NGram(train_state_frame, train_string_frame)#only train
    frequent_common_sub_state = CommonSubStringFinder(train_state_frame, pattern_state)#only train
    words = []
    #cluster_states_seen = []
    #cluster_state_counter = dict()
    with open(state_path, 'w') as sh:
        for item in frequent_common_sub_state:
            words.append((item[0]))
            sh.write((item[0]))
            sh.write('\n')
    clusteredstate_sequence = WordsHierarchy(words, UniChar(words))
    with open(cluster_path, 'w') as cph:
        for item in clusteredstate_sequence:
            #words.append((item[0]))
            cph.write(';'.join(item))
            cph.write('\n\n\n')
    print len(clusteredstate_sequence)
#################based on clustered state substrings, we want to put states into group (no overlapping states), added for TITS R1##########
    cluster_state_counter = dict()
    all_states_seen_set = set()
    #print len(clusteredstate_sequence)

    for i in xrange(len(clusteredstate_sequence)):
        for j in xrange(len(clusteredstate_sequence[i])):
            string_temp = clusteredstate_sequence[i][j].split(',')
            for k in xrange(len(string_temp)):
                all_states_seen_set.add(str(string_temp[k]))
    all_states_seen_list = list(all_states_seen_set)#states in common substrings
    print all_states_seen_list

    for i in xrange(len(all_states_seen_list)):
        for j in xrange(len(clusteredstate_sequence)):
            state = str(all_states_seen_list[i])
            category = j
            if state not in cluster_state_counter:
                cluster_state_counter[state] = {}
            if category not in cluster_state_counter[state]:
                cluster_state_counter[state][category] = {}
            cluster_state_counter[state][category] = 0
    print cluster_state_counter
    #print cluster_state_counter.keys()
    #print cluster_state_counter['0'][0]

                # if sstate not in model:
                #     model[sstate] = {}
                # if symbol not in model[sstate]:
                #     model[sstate][symbol] = {}

    for i in xrange(len(clusteredstate_sequence)):
        for j in xrange(len(clusteredstate_sequence[i])):
            string_temp = clusteredstate_sequence[i][j].split(',')
            for k in xrange(len(string_temp)):
                cluster_state_counter[str(string_temp[k])][i] += 1
    print cluster_state_counter.keys()
    state_ID = dict()
    for item in cluster_state_counter.keys():
        max_id = 0
        for j in xrange(len(clusteredstate_sequence)):
            if cluster_state_counter[item][j]>cluster_state_counter[item][max_id]:
                max_id = j
        state_ID[item] = max_id
        print item, max_id
    ID_state = dict()
    for item in state_ID.keys():
        state = item
        ID = state_ID[item]
        if ID not in ID_state:
            ID_state[ID] = set()
        ID_state[ID].add(state)
    print ID_state
    print state_ID
    
    #########manuallly correct##########
    ID_state = ({0: set(['0', '3', '5', '8', '18', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '-1']),
                1: set(['17', '21']), 2: set(['7', '13', '20']), 3: set(['4', '9', '10', '14']), 4: set(['15', '19']),
                5: set(['1', '2', '6', '11', '12', '16'])})#I80_1 full data
    #ID_state = {0: set(['12', '21', '23']), 1: set(['25', '27', '32']), 2: set(['13', '15', '14', '17']), 3: set(['11', '10', '3', '5']), 4: set(['9', '0', '2', '4']), 5: set(['24', '19', '31', '18', '16']), 6: set(['1', '8', '7', '22', '6']), 7: set(['33', '28', '20'])}#I80_combined, 0.80
    #ID_state = {0: set(['12', '21', '23']), 1: set(['25', '27', '32']), 2: set(['13', '15', '14', '17']), 3: set(['24', '11', '22', '10', '19', '18', '31', '16', '1', '0', '3', '2', '5', '4', '7', '6', '9', '8']), 4: set(['33', '28', '20'])}#I80_combined, 0.93
    #ID_state = {0: set(['12', '21', '23']), 1: set(['25', '27', '32']), 2: set(['24', '22', '1', '0', '3', '2', '5', '4', '7', '6', '9', '8', '11', '10', '13', '15', '14', '17', '16', '19', '18', '31']), 3: set(['33', '28', '20'])}#I80_combined, 0.963
    #ID_state = {0: set(['15', '5', '28', '7', '23']), 1: set(['9', '3', '4']), 2: set(['25', '12', '27', '22', '30']), 3: set(['11', '26', '20', '14', '17']), 4: set(['13', '21', '16']), 5: set(['1', '0', '2']), 6: set(['10', '6']), 7: set(['19', '8', '18', '29'])}##us101-3
    state_ID = dict()
    for item in ID_state.keys():
        for jtem in ID_state[item]:
            state_ID[jtem] = item
    print ID_state, 'ID-State'
    print state_ID, state_ID.keys(), 'State-ID'


    # optional, if define all possible states-ID manually already, not necessary to to
    # missing_states = list()#states in the model but not in the frequent substrings clustering
    # for item in all_states_list:
    #     if str(item) not in all_states_seen_list:
    #         missing_states.append(item)
    # print missing_states, 'missing'
    # for item in missing_states:
    #     state_ID[str(item)] = len(ID_state.keys())#add one more cluster to missing (infrequent) state


    #print state_ID
    #num_cluster_
    # _ = np.triu_indices(len(words), 1)
    # upper_triangle, dis_table = StringDistance(_, words, UniChar(words))
    # assign_cluster_frame = Segmentation(state_frame, clusteredstate_sequence, dis_table)
    # sentence =''
    # '''
    assign_cluster_frame = list()
    for i in xrange(len(state_frame)):
        state_frame_each = state_frame[i].split(',')
        state_ID_each = list()
        for j in xrange(len(state_frame_each)):
            state_ID_each.append(str(state_ID[state_frame_each[j]]))
        assign_cluster_frame.append(state_ID_each)
        state_ID_each = list()
    ##write assigned state sequences into file
    with open(assigned_path, 'w') as aph:
        for item in assign_cluster_frame:
            #for jtem in item:
            sentence = ','.join(item)
            aph.write(sentence)
            #sentence = ''
            aph.write('\n')