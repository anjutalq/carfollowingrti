close all
clear all
clc
global datasets
%% Helly
% minb_helly = [0.01,0.0001,10,0.1,0.1,0.0];
% maxb_helly = [ 0.6,   0.2,30,  2,  1,0.5];
% id_helly   = 6;%num of para modified Qin

minb_helly = [0.01,0.0001,10,0.1];
maxb_helly = [ 0.6,0.2,   30,  2];
id_helly   = 4;%num of para

%% IDM
minb_IDM = [0.001,0.001,1,0.001,1,0.001];%-10*ones(1,I_D);
maxb_IDM = [6,6,80,6,4,6];
id_IDM   = 6;

%% GHR
minb_GHR = [-100,-10,-10,0];%-10*ones(1,I_D);
maxb_GHR = [100,10,10,2];
id_GHR   = 4;
%% st
minb_st = [-100,-100,-100,0];%-10*ones(1,I_D);
maxb_st = [100,100,100,2];
id_st   = 4;
%%
% data for gross fitting and testing
% load('data_by_state.mat');
% temp = floor(length(dv)*0.8);
% dv = dv(1:temp);dx = dx(1:temp);vel = vel(1:temp);acc = acc(1:temp);
% save train_data_gross.mat dv dx vel acc
% load('data_by_state.mat');
% dv = dv(temp+1:end);dx = dx(temp+1:end);vel = vel(temp+1:end);acc = acc(temp+1:end);
% save test_data_gross.mat dv dx vel acc
% load('data_by_state.mat');
% dv = dv(1:end);dx = dx(1:end);vel = vel(1:end);acc = acc(1:end);
% save data_gross.mat dv dx vel acc

%% train for gross fitting
% tic
% trainfile = 'test_data_gross.mat';
% trainfile = 'data/NGSIM/train_data_ngsim.mat';
% data = load(trainfile);
% datasets = data;
% % [a,b]  = DE_train_ngsim(data,id_helly,minb_helly,maxb_helly,'objfun_helly');
% % para_helly_all = [a,b.FVr_oa];
% % save('/Users/Qlin/PycharmProjects/MOHA/parameters/helly_gross/para_helly_all.mat', 'para_helly_all')
% [a,b]  = DE_train_ngsim(data,id_IDM,minb_IDM,maxb_IDM,'objfun_IDM');
% para_idm_all = [a,b.FVr_oa];
% save('/Users/Qlin/PycharmProjects/MOHA/parameters/idm_gross/para_idm_all.mat', 'para_idm_all')
% toc
%% train for testS
%tic
for i = 1:6
    trainfile = ['train_cluster/CFtest_seq_',num2str(i),'.mat'];
    data = load(trainfile);
    datasets = data;
    [a,b]  = DE_train_ngsim(data,id_helly,minb_helly,maxb_helly,'objfun_helly');
    para{i} = [a,b.FVr_oa];
end
save('/Users/Qlin/PycharmProjects/MOHA/parameters/helly_MOHA/para_helly_MOHA.mat', 'para_helly_MOHA')
% toc
% for i = 1:6
%     trainfile = ['train_testS/CFtest_seq_',num2str(i),'.mat'];
%     data = load(trainfile);
%     datasets = data;
%     [a,b]  = DE_train_ngsim(data,id_IDM,minb_IDM,maxb_IDM,'objfun_IDM');
%     para{i} = [a,b.FVr_oa];
% end
% save para_idm_testS para
%% train for symbolic
% for i = 1:4
%     trainfile = ['train_sym/CFtest_seq_',num2str(i),'.mat'];
%     data = load(trainfile);
%     datasets = data;
%     [a,b]  = DE_train_ngsim(data,id_helly,minb_helly,maxb_helly,'objfun_helly');
%     para{i} = [a,b.FVr_oa];
% end
% save para_helly_sym para
% for i = 1:4
%     trainfile = ['train_sym/CFtest_seq_',num2str(i),'.mat'];
%     data = load(trainfile);
%     datasets = data;
%     [a,b]  = DE_train_ngsim(data,id_IDM,minb_IDM,maxb_IDM,'objfun_IDM');
%     para{i} = [a,b.FVr_oa];
% end
% save para_idm_sym para
%% train for higgs
% for i = 1:4
%     trainfile = ['train_higgs/CFtest_seq_',num2str(i),'.mat'];
%     data = load(trainfile);
%     datasets = data;
%     [a,b]  = DE_train_ngsim(data,id_helly,minb_helly,maxb_helly,'objfun_helly');
%     para{i} = [a,b.FVr_oa];
% end
% save para_helly_higgs para
% for i = 1:4
%     trainfile = ['train_higgs/CFtest_seq_',num2str(i),'.mat'];
%     data = load(trainfile);
%     datasets = data;
%     [a,b]  = DE_train_ngsim(data,id_IDM,minb_IDM,maxb_IDM,'objfun_IDM');
%     para{i} = [a,b.FVr_oa];
% end
% save para_idm_higgs para

%%



