function next_state = find_nextstate(cur_state,time_temp,cur_sym,solution)
% solution 1->a 2->b
% cur sym ts te next
% 0 5 0 569 9
    if cur_state == -1
        cur_state = 0;
    end%undo: Qin
    index = find(solution(:,1)==cur_state & solution(:,2)==cur_sym);
    len = length(index);
    next_state = 100;
    for i = 1:len
        if time_temp > solution(index(i),3) & time_temp < solution(index(i),4)
            next_state = solution(index(i),5);
            break;
        end
    end
    if next_state == 100
        next_state = cur_state;
    end
end