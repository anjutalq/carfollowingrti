function ferror = f_error_cal(sim,acc)
    temp3 = sim;
    temp5 = find(sim==Inf);
    temp3(temp5)=[];acc(temp5)=[];
    temp1 = mean(abs(acc));
    ferror = sqrt(mean(temp3)/temp1);
end