%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function:         S_MSE= objfun(FVr_temp, S_struct)
% Author:           Rainer Storn
% Description:      Implements（执行） the cost function to be minimized.
% Parameters:       FVr_temp     (I)    Paramter vector
%                   S_Struct     (I)    Contains a variety of parameters(包含多种参数).
%                                       For details see Rundeopt.m
% Return value:     S_MSE.I_nc   (O)    Number of constraints（约束）
%                   S_MSE.FVr_ca (O)    Constraint values. 0 means the constraints
%                                       are met. Values > 0 measure the distance
%                                       to a particular constraint.
%                   S_MSE.I_no   (O)    Number of objectives（目标）.
%                   S_MSE.FVr_oa (O)    Objective function values（目标函数值）.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S_MSE= objfun(FVr_temp, S_struct)
%FVr_temp(1) ---Caf FVr_temp(2)---Car
%---Check the passband(通带通率) first------------------------------------
%% dv dx vel acc
global datasets;
dx = datasets.dx;
dv = datasets.dv;
vel = datasets.vel;
acc = datasets.acc;
tt = datasets.tt;
% load para_data1.mat; 
%% a(t)=c1*dv(t-T)+c2*dx(t-T)+C3*v(t-T)
c1   = FVr_temp(1);
c2   = FVr_temp(2);
c3  = FVr_temp(3);
T  = FVr_temp(4);

F_cost_tol = 0;
temp1      = 0;
% T        = 0.5;
lag_t    = round(T*10);
num      = 0;
acc_save = [];
acc_act  = [];
% num      = length(z)-floor(tau*10);
for i = 1:length(dv)
    for j = lag_t+1:length(dv{i})
%         D(j)  = alpha+beta*vel{i}(j-lag_t)+gama*acc{i}(j-lag_t);
%         acc_y = C1*dv{i}(j-lag_t)+C2*(dx{i}(j-lag_t)-D(j));
%         acc_y = c1*dv{i}(j-lag_t)+c2*dx{i}(j-lag_t)+c3*vel{i}(j-lag_t);
        acc_y = c1*dv{i}(j-lag_t)+c2*(dx{i}(j)-vel{i}(j)*T-c3);
        if isnan(acc_y) || isnan(acc{i}(j)) 
            aaa=1;
        end
        F_cost_tol = F_cost_tol + (acc{i}(j)-acc_y).^2;
        num   = num+1;
%         acc_save = [acc_save acc_y];
%         acc_act  = [acc_act acc{i}(j)];
    end
end
% for i = double(floor(tau*10)):length(z)
%     wl    = i - floor(tau*10) + 1;
%     wl = double(wl);
%     y1 = FVr_temp(1)*(d1(i)^FVr_temp(2))*d2(wl)/(d3(wl)^FVr_temp(3));
%     F_cost_tol = F_cost_tol+abs(z(i) - y1);
% end
F_cost_tol = sqrt(F_cost_tol/num);
%%
%---Now check the stopband(阻带)--------------------------------------


%   
%---End: tolerance scheme---------------------------------------
%----strategy to put everything into a cost function------------
S_MSE.I_nc      = 0;%no constraints
S_MSE.FVr_ca    = 0;%no constraint array
S_MSE.I_no      = 1;%number of objectives (costs)
S_MSE.FVr_oa(1) = F_cost_tol;