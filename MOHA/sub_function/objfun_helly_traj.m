%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function:         S_MSE= objfun(FVr_temp, S_struct)
% Author:           Rainer Storn
% Description:      Implements（执行） the cost function to be minimized.
% Parameters:       FVr_temp     (I)    Paramter vector
%                   S_Struct     (I)    Contains a variety of parameters(包含多种参数).
%                                       For details see Rundeopt.m
% Return value:     S_MSE.I_nc   (O)    Number of constraints（约束）
%                   S_MSE.FVr_ca (O)    Constraint values. 0 means the constraints
%                                       are met. Values > 0 measure the distance
%                                       to a particular constraint.
%                   S_MSE.I_no   (O)    Number of objectives（目标）.
%                   S_MSE.FVr_oa (O)    Objective function values（目标函数值）.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S_MSE= objfun(FVr_temp, S_struct, obj, dataset)
%% dv dx vel acc
global datasets;
dx = datasets.dx;
dv = datasets.dv;
vel = datasets.vel;
acc = datasets.acc;
%% a(t)=C1*dv(t-T)+C2*(dx(t-T)-D(t)),D(t)=alpha+beta*v(t-T)+gama*a(t-T)
% C1 = FVr_temp(1);
% C2 = FVr_temp(2);
% alpha = FVr_temp(3);
% beta  = FVr_temp(4);
% gama  = FVr_temp(5);
% T     = FVr_temp(6);
F_cost_tol = 0;
% T        = 0.5;
lag_t    = round(FVr_temp(6)*10);
num      = 0;
para = FVr_temp;
%%
for i = 1:length(dv)
    if isempty(dv{i})
        continue;
    else
        dv_temp = dv{i};
        dx_temp = dx{i};
        vel_temp = vel{i};
        acc_temp = acc{i};
        len_temp = length(dv_temp);
        xf = 0; xl = dx_temp(1); vl = vel_temp(1)+dv_temp(1);
        xf_sim = 0; dx_sim = dx_temp(1); vf_sim = vel_temp(1);dv_sim = dv_temp(1);acc_sim = acc_temp(1);
        err_temp = 0;
        for i = 2:len_temp
            xl(i) = xf(i-1) + dx_temp(i) + vel_temp(i) * 0.1;
            xf(i) = xf(i-1) + vel_temp(i-1) * 0.1;
            vl(i) = vel_temp(i-1) + dv_temp(i-1);
            xf_sim(i) = xf_sim(i-1) + vf_sim(i-1) * 0.1;
            dv_sim(i) = vl(i-1) - vf_sim(i-1);
            dx_sim(i) = xl(i) - xf_sim(i);
            if i < lag_t
                acc_sim(i) =  0;
                vf_sim(i) = vf_sim(i-1) + acc_sim(i) * 0.1;
                err_temp(i) = (dx_sim(i)-dx_temp(i))^2/abs(dx_temp(i));
            else
                acc_sim(i) =  helly_out(dx_sim(i),dv_sim(i),vf_sim(i-1),acc_sim(i-1),para);
                if abs(acc_sim(i)) > 2
                    acc_sim(i) = min(-2,max(2,acc_sim(i)));
                end
                vf_sim(i) = vf_sim(i-1) + acc_sim(i) * 0.1;
                err_temp(i) = (dx_sim(i)-dx_temp(i))^2/abs(dx_temp(i));
            end
        end
        F_cost_tol = F_cost_tol + f_error_cal(err_temp,dx_temp);
    end
end

F_cost_tol = F_cost_tol/length(dv);

%---Now check the stopband(阻带)--------------------------------------


%   
%---End: tolerance scheme---------------------------------------
%----strategy to put everything into a cost function------------
S_MSE.I_nc      = 0;%no constraints
S_MSE.FVr_ca    = 0;%no constraint array
S_MSE.I_no      = 1;%number of objectives (costs)
S_MSE.FVr_oa(1) = F_cost_tol;