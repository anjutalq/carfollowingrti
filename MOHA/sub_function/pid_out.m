function acc_bounded = pid_out(vel, dx, err_current,err_his,method)
    %% a(t)=C1*dv(t-T)+C2*(dx(t-T)-D(t)),D(t)=alpha+beta*v(t-T)+gama*a(t-T)
    kp = 0.8;
    kd = 0.03;
    b_on = -2;
    s_safe_BD = 0;
    s_safe_HIT = 0;
    t_headway = 2;
    v_max = 33.3;
    a_min = -4;
    a_max = 6;
    acc_y = kp*err_current+kd*(err_current-err_his);
    if strcmp(method,'BD')
        if isnan(acc_y) | isinf(acc_y)
            acc = 0;
        elseif vel <= -2*b_on*(dx-s_safe_BD)/v_max
            acc = acc_y;
        else
            acc = a_min;
        end
    elseif strcmp(method,'HIT')
        if isnan(acc_y)
            acc = 0;
        elseif dx>=vel*t_headway+s_safe_HIT
            acc = acc_y;
        else
            acc = a_min;
        end
    else
        if isnan(acc_y) | isinf(acc_y)
            acc = 0;
        else
            acc = acc_y;
        end
    end
    acc_bounded = acc;
%     if acc>=a_max
%         acc_bounded = a_max;
%     end
%     if acc<=a_min
%         acc_bounded = a_min;
%     end
%     if acc_bounded+vel<=0
%         acc_bounded = -vel;
%     end