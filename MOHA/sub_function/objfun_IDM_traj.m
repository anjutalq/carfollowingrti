%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function:         S_MSE= objfun(FVr_temp, S_struct)
% Author:           Rainer Storn
% Description:      Implements（执行） the cost function to be minimized.
% Parameters:       FVr_temp     (I)    Paramter vector
%                   S_Struct     (I)    Contains a variety of parameters(包含多种参数).
%                                       For details see Rundeopt.m
% Return value:     S_MSE.I_nc   (O)    Number of constraints（约束）
%                   S_MSE.FVr_ca (O)    Constraint values. 0 means the constraints
%                                       are met. Values > 0 measure the distance
%                                       to a particular constraint.
%                   S_MSE.I_no   (O)    Number of objectives（目标）.
%                   S_MSE.FVr_oa (O)    Objective function values（目标函数值）.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S_MSE= objfun(FVr_temp, S_struct)
%FVr_temp(1) ---Caf FVr_temp(2)---Car
%---Check the passband(通带通率) first------------------------------------
%% dv dx vel acc
global datasets;
dx = datasets.dx;
dv = datasets.dv;
vel = datasets.vel;
acc = datasets.acc;
% load para_data1.mat; 
%% a(t)=a*(1-(vel(t)/v0)^delta-(S(t)/dx(t))^2)  S(t) = S0+T*vel(t)+(vel(t)*dv(t)/2/sqrt(a*b))
a   = FVr_temp(1);
b   = FVr_temp(2);
v0  = FVr_temp(3);
S0  = FVr_temp(4);
delta = FVr_temp(5);
T   = FVr_temp(6);
F_cost_tol = 0;
temp1      = 0;
num      = 0;
%%
% for i = 1:length(dv)
%     for j = 1:length(dv{i})
% %         D(j)  = alpha+beta*vel{i}(j-lag_t)+gama*acc{i}(j-lag_t);
% %         acc_y = C1*dv{i}(j-lag_t)+C2*(dx{i}(j-lag_t)-D(j));
% %         acc_y = c*(vel{i}(j)^m)*(dv{i}(j-lag_t))/(dx{i}(j-lag_t)^l);
%         S(j)  = S0+T*vel{i}(j)+(vel{i}(j)*dv{i}(j)/(2*sqrt(a*b)));
%         acc_y = a*(1-(vel{i}(j)/v0)^delta-(S(j)/dx{i}(j))^2);
%         if isnan(acc_y) || isnan(acc{i}(j)) 
%             aaa=1;
%         else
%             F_cost_tol = F_cost_tol + (acc{i}(j)-acc_y).^2;
%             num   = num+1;
%         end
% %         acc_save = [acc_save acc_y];
% %         acc_act  = [acc_act acc{i}(j)];
%     end
% end
%%
for j = 1:length(dv)
    S(j)  = S0+T*vel(j)+(vel(j)*dv(j)/(2*sqrt(a*b)));
    acc_y = a*(1-(vel(j)/v0)^delta-(S(j)/dx(j))^2);
    if isnan(acc_y) || isnan(acc(j))
        aaa=1;
    else
        F_cost_tol = F_cost_tol + (acc(j)-acc_y).^2;
        num   = num+1;
    end
end
F_cost_tol = sqrt(F_cost_tol/num);
%%
%---Now check the stopband(阻带)--------------------------------------


%   
%---End: tolerance scheme---------------------------------------
%----strategy to put everything into a cost function------------
S_MSE.I_nc      = 0;%no constraints
S_MSE.FVr_ca    = 0;%no constraint array
S_MSE.I_no      = 1;%number of objectives (costs)
S_MSE.FVr_oa(1) = F_cost_tol;