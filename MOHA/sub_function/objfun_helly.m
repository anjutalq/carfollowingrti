%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function:         S_MSE= objfun(FVr_temp, S_struct)
% Author:           Rainer Storn
% Description:      Implements???????? the cost function to be minimized.
% Parameters:       FVr_temp     (I)    Paramter vector
%                   S_Struct     (I)    Contains a variety of parameters(????????????).
%                                       For details see Rundeopt.m
% Return value:     S_MSE.I_nc   (O)    Number of constraints????????
%                   S_MSE.FVr_ca (O)    Constraint values. 0 means the constraints
%                                       are met. Values > 0 measure the distance
%                                       to a particular constraint.
%                   S_MSE.I_no   (O)    Number of objectives????????.
%                   S_MSE.FVr_oa (O)    Objective function values??????????????.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S_MSE= objfun(FVr_temp, S_struct, obj, dataset)
%FVr_temp(1) ---Caf FVr_temp(2)---Car
%---Check the passband(????????) first------------------------------------
%% dv dx vel acc
global datasets;
dx = datasets.dx;
dv = datasets.dv;
vel = datasets.vel;
acc = datasets.acc;
% load para_data1.mat; 
%% a(t)=C1*dv(t-T)+C2*(dx(t-T)-D(t)),D(t)=alpha+beta*v(t-T)+gama*a(t-T)
C1 = FVr_temp(1);
C2 = FVr_temp(2);
alpha = FVr_temp(3);
beta  = FVr_temp(4);
%gama  = FVr_temp(5);
%T     = FVr_temp(6);%modified Qin
F_cost_tol = 0;
% T        = 0.5;
%lag_t    = round(T*10);%modified Qin
lag_t    = 0;
num      = 0;
%%
for i = 1:length(dv)
    %for j = lag_t+1:length(dv{i})
    for j = 1+lag_t:length(dv{i})%modified Qin
        %temp  = alpha + beta * double(vel{i}(j-lag_t)) + gama *
        %double(acc{i}(j-lag_t));modified Qin
        temp  = alpha + beta * double(vel{i}(j-lag_t));
        acc_y = C1 * double(dv{i}(j-lag_t)) + C2 * (double(dx{i}(j-lag_t)) - temp);
        if isnan(acc_y) || isnan(acc{i}(j)) 
            aaa = 1
        else
            F_cost_tol = F_cost_tol + (double(acc{i}(j)) - acc_y).^2;
            num   = num + 1;
            if F_cost_tol == 255
                aaaa = 1
            end
        end
    end
end
F_cost_tol = sqrt(double(F_cost_tol)/num);


%---Now check the stopband(????)--------------------------------------


%   
%---End: tolerance scheme---------------------------------------
%----strategy to put everything into a cost function------------
S_MSE.I_nc      = 0;%no constraints
S_MSE.FVr_ca    = 0;%no constraint array
S_MSE.I_no      = 1;%number of objectives (costs)
S_MSE.FVr_oa(1) = F_cost_tol;