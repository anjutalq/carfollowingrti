%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function:         PlotIt(FVr_temp,iter,S_struct)
% Author:           Rainer Storn
% Description:      PlotIt can be used for special purpose plots
%                   used in deopt.m.
% Parameters:       FVr_temp     (I)    Paramter vector
%                   iter         (I)    counter for optimization iterations
%                   S_Struct     (I)    Contains a variety of parameters.
%                                       For details see Rundeopt.m
% Return value:     -
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function PlotIt(FVr_temp,iter,S_struct,obj)
global count;
global storedata;
%%
global datasets;
dx = datasets.dx;
dv = datasets.dv;
vel = datasets.vel;
acc = datasets.acc;


%% GHR a(t)=c*v(t)^m*dv(t-T)/dx(t-T)^l
% c = FVr_temp(1);
% m = FVr_temp(2);
% l = FVr_temp(3);
% T     = FVr_temp(4);
% 
% F_cost_tol = 0;
% % T        = 0.5;
% lag_t    = round(T*10);
% num      = 0;
% acc_save = [];
% acc_act  = [];
% % num      = length(z)-floor(tau*10);
% for i = 1:length(dv)
%     for j = lag_t+1:length(dv{i})-5
% %         D(j)  = alpha+beta*vel{i}(j-lag_t)+gama*acc{i}(j-lag_t);
% %         acc_y = C1*dv{i}(j-lag_t)+C2*(dx{i}(j-lag_t)-D(j));
%         acc_y = c*(vel{i}(j)^m)*(dv{i}(j-lag_t))/(dx{i}(j-lag_t)^l);
%         if isnan(acc_y) || isnan(acc{i}(j)) 
%             aaa=1;
%         end
%         F_cost_tol = F_cost_tol + (acc{i}(j)-acc_y).^2;
%         num   = num+1;
%         acc_save = [acc_save acc_y];
%         acc_act  = [acc_act acc{i}(j)];
%     end
% end
% 
% F_cost_tol = sqrt(F_cost_tol/num);
% storedata_t = [c,m,l,T]';
%% st a(t)=c1*dv(t-T)+c2*dx(t-T)+C3*v(t-T)
% c1   = FVr_temp(1);
% c2   = FVr_temp(2);
% c3  = FVr_temp(3);
% T  = FVr_temp(4);
% 
% F_cost_tol = 0;
% temp1      = 0;
% % T        = 0.5;
% lag_t    = round(T*10);
% num      = 0;
% acc_save = [];
% acc_act  = [];
% % num      = length(z)-floor(tau*10);
% for i = 1:length(dv)
%     for j = lag_t+1:length(dv{i})
%         acc_y = c1*dv{i}(j-lag_t)+c2*(dx{i}(j)-vel{i}(j)*T-c3);
%         if isnan(acc_y) || isnan(acc{i}(j)) 
%             aaa=1;
%         end
%         F_cost_tol = F_cost_tol + (acc{i}(j)-acc_y).^2;
%         num   = num+1;
%         acc_save = [acc_save acc_y];
%         acc_act  = [acc_act acc{i}(j)];
%     end
% end
% F_cost_tol = sqrt(F_cost_tol/num);
% storedata_t = [c1,c2,c3,T]';
%% IDM a(t)=a*(1-(vel(t)/v0)^4-(S(t)/dx(t))^2)  S(t) = S0+T*vel(t)+(vel(t)*dv(t)/2/sqrt(a*b))
% a   = FVr_temp(1);
% b   = FVr_temp(2);
% v0  = FVr_temp(3);
% S0  = FVr_temp(4);
% delta   = FVr_temp(5);
% T   = FVr_temp(6);
% F_cost_tol = 0;
% temp1      = 0;
% % T        = 0.5;
% lag_t    = round(T*10);
% num      = 0;
% acc_save = [];
% acc_act  = [];
% % num      = length(z)-floor(tau*10);
% for i = 1:length(dv)
%     for j = 1:length(dv{i})-2
% %         D(j)  = alpha+beta*vel{i}(j-lag_t)+gama*acc{i}(j-lag_t);
% %         acc_y = C1*dv{i}(j-lag_t)+C2*(dx{i}(j-lag_t)-D(j));
% %         acc_y = c*(vel{i}(j)^m)*(dv{i}(j-lag_t))/(dx{i}(j-lag_t)^l);
%         S(j)  = S0+T*vel{i}(j)+(vel{i}(j)*dv{i}(j)/(2*sqrt(a*b)));
%         acc_y = a*(1-(vel{i}(j)/v0)^delta-(S(j)/dx{i}(j))^2);
%         if isnan(acc_y) || isnan(acc{i}(j)) 
%             aaa=1;
%         end
%         %F_cost_tol = F_cost_tol + (acc{i}(j)-acc_y).^2;
%         F_cost_tol = F_cost_tol + abs(acc{i}(j)-acc_y);
%         temp1      = temp1 + acc{i}(j)^2;        
%         num   = num+1;
%         acc_save = [acc_save acc_y];
%         acc_act  = [acc_act acc{i}(j)];
%     end
% end
% F_cost_tol = sqrt(F_cost_tol/num);
% storedata_t = [a,b,v0,S0,T]';
%% Helly a(t)=C1*dv(t-T)+C2*(dx(t-T)-D(t)),D(t)=alpha+beta*v(t-T)+gama*a(t-T)
C1 = FVr_temp(1);
C2 = FVr_temp(2);
alpha = FVr_temp(3);
beta  = FVr_temp(4);
gama  = FVr_temp(5);
T     = FVr_temp(6);
F_cost_tol = 0;
% T        = 0.5;
lag_t    = round(T*10);
num      = 0;
acc_save = [];
acc_act  = [];
% num      = length(z)-floor(tau*10);
for i = 1:length(dv)%1:length(dv)
    for j = lag_t+1:length(dv{i})
        D(j)  = alpha+beta*vel{i}(j-lag_t)+gama*acc{i}(j-lag_t);
        acc_y = C1*dv{i}(j-lag_t)+C2*(dx{i}(j-lag_t)-D(j));
        if isnan(acc_y) || isnan(acc{i}(j)) 
            aaa=1;
        end
        F_cost_tol = F_cost_tol + (acc{i}(j)-acc_y).^2;
        num   = num+1;
        acc_save = [acc_save acc_y];
        acc_act  = [acc_act acc{i}(j)];
    end
end
F_cost_tol = sqrt(F_cost_tol/num);
storedata_t = [C1,C2,alpha,beta,gama,T]';
%%
  obj
  figure(1)
  %----plot the polynomial.----------
  plot(acc_save,'b');
  hold on;
  plot(acc_act,'r');

  %----plot the tolerance scheme.----
%   plot(S_struct.FVr_x,S_struct.FVr_lim_up,'r');
%   plot(S_struct.FVr_xplot,S_struct.FVr_lim_lo_plot,'r');
  %axis([-1.3 1.3 -2 S_struct.FVr_bound(length(FVr_temp))]);
%   axis([-1.3 1.3 -2 10]);
  hold off;
  grid on;
%   zoom on;
  title(['Helly model calibration',' RMSE: ',num2str(F_cost_tol)]);
  xlabel('data number');
  ylabel('acc (m/s^2)');
  legend('calibrated','raw data');
  count = count+1;
  storedata = [storedata,storedata_t];
%   saveas(gcf,['C:\Users\ugv\Box Sync\VeCaN\Report\20160420\video1\','sss',num2str(count),'.png']);
  drawnow;
  return
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
