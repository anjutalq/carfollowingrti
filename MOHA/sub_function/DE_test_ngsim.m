function [obs_data, cal_data, rmse]  = DE_test_ngsim(data , para , model)
    %% dv dx vel acc
    dx = data.dx;
    dv = data.dv;
    vel = data.vel;
    acc = data.acc;
    switch model
        case 'helly'
%             model
            %% a(t)=C1*dv(t-T)+C2*(dx(t-T)-D(t)),D(t)=alpha+beta*v(t-T)+gama*a(t-T)
            C1 = para(1);
            C2 = para(2);
            alpha = para(3);
            beta  = para(4);
            gama  = para(5);
            T     = para(6);
            F_cost_tol = 0;
            lag_t    = round(T*10);
            num      = 0;
            cal_data = [];
            obs_data = [];
            for i = 1:length(dv)
                obs_data_temp = [];
                cal_data_temp = [];
                for j = lag_t+1:length(dv{i})
                    D(j)  = alpha+beta*vel{i}(j-lag_t)+gama*acc{i}(j-lag_t);
                    acc_y = C1*dv{i}(j-lag_t)+C2*(dx{i}(j-lag_t)-D(j));
                    if isnan(acc_y) || isnan(acc{i}(j))
                        aaa=1;
                    end
                    F_cost_tol = F_cost_tol + (acc{i}(j)-acc_y).^2;
                    num   = num+1;
                    cal_data_temp = [cal_data_temp acc_y];
                    obs_data_temp  = [obs_data_temp acc{i}(j)];
                end
                cal_data{i} = cal_data_temp;
                obs_data{i} = obs_data_temp;
            end
            rmse = sqrt(double(F_cost_tol)/num);
        case 'ghr'
%             model
            %% a(t)=c*v(t)^m*dv(t-T)/dx(t-T)^l
            c = para(1);
            m = para(2);
            l = para(3);
            T = para(4);
            F_cost_tol = 0;
            lag_t    = round(T*10);
            num      = 0;
            obs_data = [];
            cal_data = [];
            for i = 1:length(dv)
                for j = lag_t+1:length(dv{i})-5
                    acc_y = c*(vel{i}(j)^m)*(dv{i}(j-lag_t))/(dx{i}(j-lag_t)^l);
                    if isnan(acc_y) || isnan(acc{i}(j))
                        aaa=1;
                    end
                    F_cost_tol = F_cost_tol + (acc{i}(j)-acc_y).^2;
                    num   = num + 1;
                    cal_data = [cal_data acc_y];
                    obs_data  = [obs_data acc{i}(j)];
                end
            end
            rmse = sqrt(F_cost_tol/num);
        case 'idm'
%             model
%% a(t)=a*(1-(vel(t)/v0)^delta-(S(t)/dx(t))^2)  S(t) = S0+T*vel(t)+(vel(t)*dv(t)/2/sqrt(a*b))
            a = para(1);
            b = para(2);
            v0 = para(3);
            S0  = para(4);
            delta  = para(5);
            T     = para(6);
            F_cost_tol = 0;
            lag_t    = round(T*10);
            num      = 0;
            cal_data = [];
            obs_data = [];
            for i = 1:length(dv)
                obs_data_temp = [];
                cal_data_temp = [];
                for j = lag_t+1:length(dv{i})
                    S(j)  = S0+T*vel{i}(j-lag_t)+(vel{i}(j-lag_t)*dv{i}(j-lag_t)/(2*sqrt(a*b)));
                    acc_y = a*(1-(vel{i}(j-lag_t)/v0)^delta-(S(j-lag_t)/dx{i}(j-lag_t))^2);
                    if isnan(acc_y) || isnan(acc{i}(j))
                        aaa=1;
                    end
                    F_cost_tol = F_cost_tol + (acc{i}(j)-acc_y).^2;
                    num   = num+1;
                    cal_data_temp = [cal_data_temp acc_y];
                    obs_data_temp  = [obs_data_temp acc{i}(j)];
                end
                cal_data{i} = cal_data_temp;
                obs_data{i} = obs_data_temp;
            end
            rmse = sqrt(double(F_cost_tol)/num);
            
    end

    
end