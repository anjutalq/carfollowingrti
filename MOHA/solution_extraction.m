function solution = solution_extraction(sol_dir)
    a = [];
    b = [];
    i = 1;
    f = fopen(sol_dir);
    %f = fopen('/Users/Qlin/PycharmProjects/MOHA/ori/solution.txt')
    % 0 e [0, 569]->9 #125 p=0.0711845
    pat = '(?<cur>\d+) (?<sym>\w) \[(?<t_s>\d+), (?<t_e>\d+)]->(?<next>\d+) #';
    while ~feof(f)
        a{i} = fgetl(f);
        temp = regexp(a{i},pat,'names');
        if ~isempty(temp)
            b = [b;str2num(temp.cur),abs(temp.sym)-96,str2num(temp.t_s),str2num(temp.t_e),str2num(temp.next)];
        else
            pat1 = '(?<cur>\d+) (?<sym>\w) \[(?<t_s>\d+), (?<t_e>\d+)]->';
            temp = regexp(a{i},pat1,'names');
            b = [b;str2num(temp.cur),abs(temp.sym)-96,str2num(temp.t_s),str2num(temp.t_e),-1];
        end
        i = i + 1;
    end
    solution = b;
end