function cur_cluster = find_cluster(cur_state,cl_mat)
%% cluster codebook 6+1 clusters
% cl_mat = {[0 2 3 8 13],...
%           [17 21],...
%           [7 13 20],...
%           [4 9 10 14],...
%           [12 15 19],...
%           [1 2 6 11 12],...
%           [5 16 18 22 23 24 25 26 27 28 29 30 31 32 33 34 -1]};
if ismember(cur_state,cl_mat{1})
    cur_cluster = 1;
elseif ismember(cur_state,cl_mat{2})
    cur_cluster = 2;
elseif ismember(cur_state,cl_mat{3})
    cur_cluster = 3;
elseif ismember(cur_state,cl_mat{4})
    cur_cluster = 4;
elseif ismember(cur_state,cl_mat{5})
    cur_cluster = 5;
elseif ismember(cur_state,cl_mat{6})
    cur_cluster = 6;
elseif ismember(cur_state,cl_mat{7})
    cur_cluster = 7;
else
    cur_cluster = 7;
end


end