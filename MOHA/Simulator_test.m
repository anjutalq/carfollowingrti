% close all
clear all
clc
tic
%% simulation testing of trajectory
%% read the solution file
sol_dir = 'ori/solution.txt';
%f = fopen(sol_dir);
solution = solution_extraction(sol_dir);
auto_brake_method = 'NONE';
%auto_brake_method = 'HIT';
%auto_brake_method = 'BD';
%% cluster codebook 6+1 clusters
% cl_mat = {[0 2 3 8 13],...
%           [17 21],...
%           [7 13 20],...
%           [4 9 10 14],...
%           [12 15 19],...
%           [1 2 6 11 12],...
%           [5 16 18 22 23 24 25 26 27 28 29 30 31 32 33 34 -1]};
cl_mat = {[0, 3, 5, 8, 18, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, -1],...
          [17 21],...
          [7 13 20],...
          [4 9 10 14],...
          [15 19],...
          [1, 2, 6, 11, 12, 16]};
%% symbol codebook 10 symbols abcdefghij dv dx v
sym_mat = {[  7.87678858e-01   5.78714836e+01   1.36950600e+01],...
           [  3.02970280e+00   3.61310525e+01   1.05424942e+01],...
           [ -2.88274327e+00   1.56395870e+01   7.74508749e+00],...
           [  4.82479035e+00   1.55531956e+01   5.94254846e+00],...
           [ -3.12855987e+00   2.04180145e+02   1.94124447e+01],...
           [ -9.82077737e-01   9.60900744e+01   1.72569637e+01],...
           [ -9.67519506e+00   3.97474969e+01   1.29985277e+01],...
           [  2.52215728e+00   2.40075123e+01   8.38240630e+00],...
           [ -7.02308911e+00   2.44784548e+01   1.01049181e+01],...
           [  1.20296756e-01   1.01355039e+01   4.12085824e+00]};
%% load the testing dataset
% load('test_data_gross.mat');
load('train_data_ngsim.mat');
load('pattern_seq_over_5.mat');
fid = fopen('ori/ori_seq.txt');
ori_seq = textscan(fid,'%s');
fclose(fid);
% ori_seq = textread('ori/ori_seq.txt','%s');
load('parameters/helly_gross/para_helly_all.mat');%parameters/helly_gross/para_helly_all.mat
load('parameters/helly_MOHA/para_helly_MOHA.mat');%parameters/helly_MOHA/para_helly_MOHA.mat
para_helly_moha = para;
para_helly_gross = para_helly_all;
len = length(seq_index);
%len = length(dx);
%% pid parameters
kp = 0.8; kd = 0.03;d_safe = 20;
%% frame
dx_err = [];dx_err1 = [];dx_err2 = [];
dv_err = [];dv_err1 = [];dv_err2 = [];
da_err = [];da_err1 = [];da_err2 = [];
dxf_err = [];dxf_err1 = [];dxf_err2 = [];
for k = 1:len%1:len % 14 30
    index = seq_index(k);
    pat_temp = seq_pattern{k}(seq_start(k):end);
    dv_temp = dv{index}(seq_start(k):seq_end(k));
    dx_temp = dx{index}(seq_start(k):seq_end(k));
    vel_temp = vel{index}(seq_start(k):seq_end(k));
    acc_temp = acc{index}(seq_start(k):seq_end(k));
    
%     pat_temp = seq_pattern{k}(seq_start(k):end);
%     dv_temp = dv{index}(seq_start(k):seq_end(k));
%     dx_temp = dx{index}(seq_start(k):seq_end(k));
%     vel_temp = vel{index}(seq_start(k):seq_end(k));
%     acc_temp = acc{index}(seq_start(k):seq_end(k));
    
    %sym_temp = abs(ori_seq{index}(1:end))-96;
    len_temp = length(dv_temp);
    % initial states
    xf = 0.01; xl = dx_temp(1); vl = vel_temp(1)+dv_temp(1);
    xf_sim = 0; dx_sim = dx_temp(1); vf_sim = vel_temp(1);dv_sim = dv_temp(1);acc_sim = acc_temp(1);
    xf_sim1 = 0; dx_sim1 = dx_temp(1); vf_sim1 = vel_temp(1);dv_sim1 = dv_temp(1);acc_sim1 = acc_temp(1);
    xf_sim2 = 0; dx_sim2 = dx_temp(1); vf_sim2 = vel_temp(1);dv_sim2 = dv_temp(1);acc_sim2 = acc_temp(1);
    d_des    = d_safe + vf_sim2;
    err_pid  = dx_sim2-d_des;
    cur_state = str2num(pat_temp(1)); time_temp = 1;
    cur_sym = find_sym(dv_sim,dx_sim,vf_sim,sym_mat);
    %cur_cluster = 1;% modify Qin
    cur_cluster = 1;
    ori_cluster = str2num(pat_temp(1));
    ori_state = str2num(pat_temp(1));
    counter_example = [];
    for i = 2:len_temp       
        xl(i) = xf(i-1) + dx_temp(i) + vel_temp(i) * 0.1;
        xf(i) = xf(i-1) + vel_temp(i-1) * 0.1;
        vl(i) = vel_temp(i-1) + dv_temp(i-1);
        xf_sim(i) = xf_sim(i-1) + vf_sim(i-1) * 0.1;
        dv_sim(i) = vl(i-1) - vf_sim(i-1);
        dx_sim(i) = xl(i) - xf_sim(i);
        cur_sym(i) = find_sym(dv_sim(i-1),dx_sim(i-1),vf_sim(i-1),sym_mat); 
        if cur_sym(i) == cur_sym(i-1)
            cur_state(i) = cur_state(i-1);
            time_temp = time_temp + 1;
        else
            cur_state(i) = find_nextstate(cur_state(i-1),time_temp,cur_sym(i),solution);
            time_temp = 0;
        end
        %cur_state(i)
        cur_cluster(i) = find_cluster(cur_state(i),cl_mat);
%         if cur_cluster(i) == 7
%             cur_cluster(i) = cur_cluster(i-1);
%         end
        para_temp = para_helly_moha{cur_cluster(i)};
%         para_temp = para_helly_testS{1};
%         acc_sim(i) =  helly_out(dx_sim(i-1),dv_sim(i-1),vf_sim(i-1),acc_sim(i-1),para_temp);
        acc_sim(i) =  helly_out_nodelay(dx_sim(i),dv_sim(i),vf_sim(i-1),para_temp, auto_brake_method);
        vf_sim(i) = max(0,vf_sim(i-1) + acc_sim(i) * 0.1);
%         dv_sim(i) = vl(i) - vf_sim(i);      
        xf_sim1(i) = xf_sim1(i-1) + vf_sim1(i-1) * 0.1;
        dv_sim1(i) = vl(i-1) - vf_sim1(i-1);
        dx_sim1(i) = xl(i) - xf_sim1(i);
        acc_sim1(i) =  helly_out_nodelay(dx_sim1(i),dv_sim1(i),vf_sim1(i-1),para_helly_gross, auto_brake_method);
        vf_sim1(i) = max(0,vf_sim1(i-1) + acc_sim1(i) * 0.1);
        
        ori_cluster(i) = str2num(pat_temp(i));
        ori_state(i) = str2num(pat_temp(i));
        
        %% pid controller
        xf_sim2(i)  = xf_sim2(i-1) + vf_sim2(i-1) * 0.1;
        dv_sim2(i)  = vl(i-1) - vf_sim2(i-1);
        dx_sim2(i)  = xl(i) - xf_sim2(i);
        d_des(i)    = d_safe + vf_sim2(i-1);
        err_pid(i)  = dx_sim2(i)-d_des(i);
        acc_sim2(i) =  pid_out(vf_sim2(i-1), dx_sim2(i), err_pid(i), err_pid(i-1), auto_brake_method);
        vf_sim2(i)  = max(0,vf_sim2(i-1) + acc_sim2(i) * 0.1);
    end
    if dx_sim<=0
        counter_example = [counter_example, k];
    end
	%% calculate the error
    % RMSE std F_error
%     if ~isempty(find(dx_sim<0.1)) | ~isempty(find(dx_sim1<0.1)) | ~isempty(find(dx_sim2<0.1))
%         continue;
%     else
    %relative error
%     dx_err_x  = [dx_err_x,  sqrt(mean((((xf_sim -xf)./xf).^2)))];
%     dx_err1_x = [dx_err1_x, sqrt(mean((((xf_sim1-xf)./xf).^2)))];
%     dx_err2_x = [dx_err2_x, sqrt(mean((((xf_sim2-xf)./xf).^2)))];
%     
%     dx_err_v  = [dx_err_v,  sqrt(mean((((vf_sim -vel_temp')./vel_temp').^2)))];
%     dx_err1_v = [dx_err1_v, sqrt(mean((((vf_sim1-vel_temp')./vel_temp').^2)))];
%     dx_err2_v = [dx_err2_v, sqrt(mean((((vf_sim2-vel_temp')./vel_temp').^2)))];

%     % absolute error
%     dx_err  = [dx_err,  sqrt(mean((xf_sim -xf).^2)/(mean(xf)^2))];
%     dx_err1 = [dx_err1, sqrt(mean((xf_sim1-xf).^2)/(mean(xf)^2))];
%     dx_err2 = [dx_err2, sqrt(mean((xf_sim2-xf).^2)/(mean(xf)^2))];
    
    
    da_err  = [da_err,  sqrt(mean((acc_sim -acc_temp').^2)/(mean(acc_temp)^2))];
    da_err1 = [da_err1, sqrt(mean((acc_sim1-acc_temp').^2)/(mean(acc_temp)^2))];
    da_err2 = [da_err2, sqrt(mean((acc_sim2-acc_temp').^2)/(mean(acc_temp)^2))];
    
    dx_err  = [dx_err,  sqrt(mean((xf_sim -xf).^2)/len_temp)];
    dx_err1 = [dx_err1, sqrt(mean((xf_sim1-xf).^2)/len_temp)];
    dx_err2 = [dx_err2, sqrt(mean((xf_sim2-xf).^2)/len_temp)];
    
    dv_err  = [dv_err,  sqrt(mean((vf_sim -vel_temp').^2)/len_temp)];
    dv_err1 = [dv_err1, sqrt(mean((vf_sim1-vel_temp').^2)/len_temp)];
    dv_err2 = [dv_err2, sqrt(mean((vf_sim2-vel_temp').^2)/len_temp)];
dx_err_all = mean(dx_err);%helly_multi
dx_err_all_1 = mean(dx_err1);%%helly_single
dx_err_all_2 = mean(dx_err2);%PID

dv_err_all = mean(dv_err);%helly_multi
dv_err_all_1 = mean(dv_err1);%%helly_single
dv_err_all_2 = mean(dv_err2);%PID

da_err_all = mean(da_err);%helly_multi
da_err_all_1 = mean(da_err1);%%helly_single
da_err_all_2 = mean(da_err2);%PID

%     % mixed error measure
%     dxf_err  = [dxf_err,  sqrt(mean(((xf_sim -xf).^2)./abs(xf))/mean(abs(xf)))];    
%     dxf_err1 = [dxf_err1, sqrt(mean(((xf_sim1-xf).^2)./abs(xf))/mean(abs(xf)))];  
%     dxf_err2 = [dxf_err2, sqrt(mean(((xf_sim2-xf).^2)./abs(xf))/mean(abs(xf)))];
%     end
    %% plot section
%     dx_err = mean(sqrt(((xf-xf_sim).^2)./abs(xf)));
%     dxf_err = sqrt(mean(((xf-xf_sim).^2)./abs(xf))/mean(xf));
%     dx_err1 = mean(sqrt(((xf-xf_sim1).^2)./abs(xf)));
%     dxf_err1 = sqrt(mean(((xf-xf_sim1).^2)./abs(xf))/mean(xf));
%     dx_err2 = mean(sqrt(((xf-xf_sim2).^2)./abs(xf)));
%     dxf_err2 = sqrt(mean(((xf-xf_sim2).^2)./abs(xf))/mean(xf));
    %% plot for paper
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     ttt = 0.1*(1:length(xf));
%     figure(11);hold off;plot(ttt,xf,':','linewidth',3);hold on;grid on;plot(ttt,xf_sim,'linewidth',3);plot(ttt,xf_sim1,'-.','linewidth',3);plot(ttt,xf_sim2,'--','linewidth',3);%plot(xl,'linewidth',1);
%     xlabel('??????????');ylabel('??????????');legend('????','??????','????????','PID');
%     set(gca,'FontSize',18);
% %     set(gca,'FontName','Times New Roman');
%     title('?????????????? ??2');
% %     title(['dx RMSE: ',num2str(dx_err),', ',num2str(dx_err1),', ',num2str(dx_err2)]);
%     figure(22);hold off;plot(ttt,vel_temp,':','linewidth',3);hold on;grid on;plot(ttt,vf_sim,'linewidth',3);plot(ttt,vf_sim1,'-.','linewidth',3);plot(ttt,vf_sim2,'--','linewidth',3);%plot(vl,'linewidth',1);
%     xlabel('??????????');ylabel('??????????????');legend('????','??????','????????','PID');
%     set(gca,'FontSize',18);
%     title('?????????????? ??2');
% %     set(gca,'FontName','Times New Roman');
% %     title(['dx F err: ',num2str(dxf_err),', ',num2str(dxf_err1),', ',num2str(dxf_err2)]);
%     figure(33);hold off;plot(ttt,dx_temp,':','linewidth',3);hold on;grid on;plot(ttt,dx_sim,'linewidth',3);plot(ttt,dx_sim1,'-.','linewidth',3);plot(ttt,dx_sim2,'--','linewidth',3);%plot(d_des);
% %     xlabel('time (s)');ylabel('relative distance (m)');legend('dx','dx proposed','dx gross','pid');%,'acc aim'
%     xlabel('??????????');ylabel('??????????????');legend('????','??????','????????','PID');
%     set(gca,'FontSize',18);
%     title('???????????????????? ??1');
% %     set(gca,'FontName','Times New Roman');
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     ttt = 0.1*(1:length(xf));
%     figure(11);
%     hold off;plot(ttt,xl,':','linewidth',2);hold on;grid on;
%     plot(ttt,xf,'linewidth',2);
%     plot(ttt,xf_sim,'linewidth',2);
%     plot(ttt,xf_sim1,'-.','linewidth',2);
%     plot(ttt,xf_sim2,'--','linewidth',2);
%     xlabel('Time (s)');ylabel('Distance (m)');
%     legend('Human driver-lead','Human driver-follow','Multi-mode','Single-mode','PID','Location','NorthWest');
%     set(gca,'FontSize',15);
%     figure(22);
%     hold off;plot(ttt,vl,':','linewidth',2);hold on;grid on;
%     plot(ttt,vel_temp,'linewidth',2);
%     plot(ttt,vf_sim,'linewidth',2);
%     plot(ttt,vf_sim1,'-.','linewidth',2);
%     plot(ttt,vf_sim2,'--','linewidth',2);
%     xlabel('Time (s)');ylabel('Velocity (m/s)');
%     legend('Human driver-lead','Human driver-follow','Multi-mode','Single-mode','PID','Location','NorthWest');
%     set(gca,'FontSize',15);
%     figure(33);
%     hold off;plot(ttt,dx_temp,':','linewidth',2);hold on;grid on;
%     plot(ttt,dx_sim,'linewidth',2);
%     plot(ttt,dx_sim1,'-.','linewidth',2);
%     plot(ttt,dx_sim2,'--','linewidth',2);
%     %plot(d_des);
%     xlabel('Time (s)');ylabel('Relative distance (m)');
%     legend('Human driver-follow','Multi-mode','Single-mode','PID','Location','NorthWest');
%     set(gca,'FontSize',15);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     ttt = 0.1*(1:length(xf));
%     figure(1);hold off;plot(ttt,xf_sim,'Linewidth',2);hold on;grid on;plot(ttt,xf_sim1,'Linewidth',2);plot(ttt,xf_sim2,'Linewidth',2);plot(ttt,xf,'Linewidth',2);plot(ttt,xl,'Linewidth',2);
%     xlabel('time (s)');ylabel('distance (m)');legend('xf proposed','xf gross','pid','xf','xl','Location','Northwest');
%     set(gca,'FontName','Times New Roman','FontSize',18);
%     figure(2);hold off;plot(ttt,vf_sim,'Linewidth',2);hold on;grid on;plot(ttt,vf_sim1,'Linewidth',2);plot(ttt,vf_sim2,'Linewidth',2);plot(ttt,vel_temp,'Linewidth',2);
%     xlabel('time (s)');ylabel('velocity (m/s)');legend('vf proposed','vf gross','pid','vf','Location','Northwest');%plot(vl);
%     set(gca,'FontName','Times New Roman','FontSize',18);
%     figure(3);hold off;plot(ttt,dx_temp,'Linewidth',2);hold on;grid on;plot(ttt,dx_sim,'Linewidth',2);plot(ttt,dx_sim1,'Linewidth',2);plot(ttt,dx_sim2,'Linewidth',2);%plot(d_des);
%     xlabel('time (s)');ylabel('relative distance (m)');legend('dx','dx proposed','dx gross','pid','Location','Northwest');
%     set(gca,'FontName','Times New Roman','FontSize',18);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    %     figure(4);hold off;plot(cur_sym);hold on;grid on;plot(sym_temp);
%     xlabel('time (s)');ylabel('symbol');legend('simu','ori');
%     figure(5);hold off;plot(cur_state);hold on;grid on;plot(ori_state);
%     xlabel('time (s)');ylabel('state');legend('simu','ori');
%     figure(6);hold off;plot(cur_cluster);hold on;grid on;plot(ori_cluster);
%     xlabel('time (s)');ylabel('cluster');legend('simu','ori');
    %% observe
%     subplot(2,3,1);hold off;plot(xl);hold on;grid on;plot(xf_sim,'.');plot(xf_sim1);plot(xf_sim2);plot(xf);
%     xlabel('time (s)');ylabel('distance (m)');legend('xl','xf proposed','xf gross','pid','xf');
%     title(['dx RMSE: ',num2str(dx_err),', ',num2str(dx_err1),', ',num2str(dx_err2)]);
%     subplot(2,3,2);hold off;plot(vl);hold on;grid on;plot(vf_sim,'.');plot(vf_sim1);plot(vf_sim2);plot(vel_temp);
%     xlabel('time (s)');ylabel('velocity (m/s)');legend('vl','vf proposed','vf gross','pid','vf');
%     title(['dx F err: ',num2str(dxf_err),', ',num2str(dxf_err1),', ',num2str(dxf_err2)]);
%     subplot(2,3,3);hold off;plot(dx_temp);hold on;grid on;plot(dx_sim,'.');plot(dx_sim1);plot(dx_sim2);%plot(d_des);
%     xlabel('time (s)');ylabel('relative distance (m)');legend('dx','dx proposed','dx gross','pid');%,'acc aim'
%     subplot(2,3,4);hold off;plot(cur_sym);hold on;grid on;plot(sym_temp);
%     xlabel('time (s)');ylabel('symbol');legend('simu','ori');
%     subplot(2,3,5);hold off;plot(cur_state);hold on;grid on;plot(ori_state);
%     xlabel('time (s)');ylabel('state');legend('simu','ori');
%     subplot(2,3,6);hold off;plot(cur_cluster);hold on;grid on;plot(ori_cluster);
%     xlabel('time (s)');ylabel('cluster');legend('simu','ori');
end
% mean_err = [mean(dx_err),mean(dx_err1),mean(dx_err2)]
% std_err = [std(dx_err),std(dx_err1),std(dx_err2)]
% mean_aerr = [mean(dxa_err),mean(dxa_err1),mean(dxa_err2)]
% std_aerr = [std(dxa_err),std(dxa_err1),std(dxa_err2)]
% mean_ferr = [mean(dxf_err),mean(dxf_err1),mean(dxf_err2)]
% std_ferr = [std(dxf_err),std(dxf_err1),std(dxf_err2)]
%%
toc




