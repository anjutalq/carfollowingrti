# -*- coding: utf-8 -*-
__author__ = 'Yihuan Zhang and Qin Lin'
__email__ = "13yhzhang@tongji.edu.cn, q.lin@tudelft.nl"


##
# original .mat file
# output  length according to stateframe
##
# extract the state pattern of origin sequence
# output is save_pat_seq_x.txt
##
# train the car following model via pattern sequence
# output CFtest.mat
##

import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as si
import re

################
# pat_index: 
def seqwriter(pat_index,seq_index,seq_sym,seq_start,seq_end,dst):
    dst_head = open(str(dst)+'.txt', 'a')
    dst_head.write(str(pat_index)+','+str(seq_index)+', ['+str(seq_start)+','+str(seq_end)+'], '+str(seq_sym))
    dst_head.write('\n')
    dst_head.close()

def seqwriter_n(seq_index,seq_sym,seq_start,seq_end,dst):
    dst_head = open(str(dst)+'.txt', 'a')
    dst_head.write(str(seq_index)+', ['+str(seq_start)+','+str(seq_end)+'], '+str(seq_sym))
    dst_head.write('\n')
    dst_head.close()

########
# Function: write original sequence, convert rti strings into symbolic string (one symbol every time step)
# Input:
#   src_dir: 'train_data_ngsim_I80_1.mat' original .mat file
#   cfrti_dir: 'ori/rti_full_I80_1.txt' orignal rti output file
# Output:
#   output_dir: 'ori_seq.txt' orignal symbol sequence 
########
def Ori_seq_write(src_dir,cfrti_dir,output_dir):
    print 'start writing ori_seq.txt'
    data = si.loadmat(src_dir)
    accList = data['acc'][0]  # a list, each element of the list contains one car following period
    numSequence = len(accList)  # number of car following periods, 4099 in this case
    cfrti_data = []
    with open(cfrti_dir, "r") as daa:
        daa.readline()
        for line in daa:
            lineWithoutHead = line.strip().split(' ')[1:]
            cfrti_data.append(lineWithoutHead)

    f = open(output_dir, 'w')
    for i in xrange(numSequence):
        s_len = len(accList[i])
        c_len = len(cfrti_data[i])
        seq = []
        seq_temp = []
        count = 0
        for j in xrange(c_len / 2):
            if j < c_len / 2 - 1:
                symbol = cfrti_data[i][j * 2]
                amount = int(cfrti_data[i][j * 2 + 3])
                count = count + amount
                seq_temp.append(symbol * amount)
            else:
                symbol = cfrti_data[i][j * 2]
                amount = s_len - count
                seq_temp.append(symbol * amount)
        seq.append(''.join(seq_temp))
        f.write(str(seq[0]))
        f.write('\n')
    f.close()
    print 'finish writing ori_seq.txt'

########
# Function: transform src_dir to data_by_state.mat according to the clusters
# Input:
#   src_dir: 'train_data_ngsim_I80_1.mat' original .mat file
#   stateframe_dir --> clusterframe_dir: 'ori/assign_state_I80_1.txt' assigned clusters
#   cfrti_dir: 'ori/rti_full_I80_1.txt' orignal rti output file
# Output:
#   data_by_state.mat, this data will all be fired by the state machine
########
def Ori2stateframe(src_dir,stateframe_dir,cfrti_dir):
    data = si.loadmat(src_dir)
    accList = data['acc'][0]  # a list, each element of the list contains one car following period
    dvList = data['dv'][0]
    dxList = data['dx'][0]
    velList = data['vel'][0]
    numSequence = len(accList)  # number of car following periods, 4099 in this case
    stateFrame = []
    with open(stateframe_dir, 'r') as st:
        for line in st:
            temp = []
            temp.append('00')
            for item in line.strip().split(',')[1:]:
                if int(item) < 10:
                    temp.append('0' + item)
                else:
                    temp.append(item)
            stateFrame.append(','.join(temp))
    print 'startFrame ready'
    cfrti_data = []
    cfrti_head = []
    with open(cfrti_dir, "r") as daa:
        daa.readline()
        for line in daa:
            lineWithoutHead = line.strip().split(' ')
            cfrti_head.append(lineWithoutHead[0])
            cfrti_data.append(lineWithoutHead[1:])
    acc_temp = []
    dv_temp = []
    dx_temp = []
    vel_temp = []
    # print stateFrame[0], len(stateFrame[0]) / 3
    # print cfrti_head

    for i in xrange(numSequence):
        statenum = len(stateFrame[i]) / 3#not including starting state
        #print len(stateFrame[i]), statenum
        if statenum == int(cfrti_head[i]):#strings fired by the TA, then #symbols=#states
            # print statenum,stateFrame[i],cfrti_data[i]
            acc_temp.append(accList[i])
            dv_temp.append(dvList[i])
            dx_temp.append(dxList[i])
            vel_temp.append(velList[i])
            # print 'same'
        else:
            # print statenum,cfrti_head[i]
            numcount = 0
            # print statenum,stateFrame[i],cfrti_data[i]
            for j in xrange(statenum):
                numcount += int(cfrti_data[i][j * 2 + 3])
            acc_temp.append(accList[i][0:numcount])
            dv_temp.append(dvList[i][0:numcount])
            dx_temp.append(dxList[i][0:numcount])
            vel_temp.append(velList[i][0:numcount])
    si.savemat('data_by_state.mat', {'acc': acc_temp, 'dv': dv_temp, 'dx': dx_temp, 'vel': vel_temp})
    print 'data_by_state.mat is writen'

########
# Function: extract clusters in orignal files and save them into seperate new files
# Input:
#   stateframe_dir: 'ori/state_frame_I80_1.txt' original state file
#   cfrti_dir: 'ori/rti_full_I80_1.txt' orignal rti output file
#   pat_dir: 'ori/train_cluster_state_sequence_I80_1.txt' common state sequences "7,10,22", etc.
#   ori_dir: 'ori_seq.txt' orignal symbol sequence
#   dir: 1 -> train, 2 -> test
# Output:
#   'save_pat_seq_' one cluster (state pattern) per .txt file
########
def StateExtraction(stateframe_dir,cfrti_dir,pat_dir,ori_dir,dir):
    stateFrame = []
    with open(stateframe_dir, 'r') as st:
        for line in st:
            temp = []
            temp.append('00')
            for item in line.strip().split(',')[1:]:
                if int(item) < 10:
                    temp.append('0' + item)
                else:
                    temp.append(item)
            stateFrame.append(','.join(temp))
    print 'startFrame ready'
    cfrti_data = []
    with open(cfrti_dir, "r") as daa:
        daa.readline()
        for line in daa:
            lineWithoutHead = line.strip().split(' ')[1:]
            cfrti_data.append(lineWithoutHead)
    cluster_st_pat = []
    p1 = re.compile(r'\;')
    p2 = re.compile(r'\,')
    count = 0
    with open(pat_dir, "r") as mh:
        for line in mh:
            if line != '\n':
                count += 1
                patt = []
                temp = p1.split(line.strip())  # state patterns ['9,24,19']
                # print temp
                for item in temp:
                    temp1 = p2.split(str(item))
                    temp11 = ''
                    for ii in temp1:
                        if int(ii) < 10:
                            temp11 = str(temp11) + '0' + str(ii) + ','
                        else:
                            temp11 = str(temp11) + str(ii) + ','
                    patt.append(temp11[:-1])
                cluster_st_pat.append(patt)
    seq = []
    with open(ori_dir, "r") as rt:
        for line in rt:
            seq.append(line.strip())
    if dir == 1:
        start_temp = 0
        end_temp = int(proportion*len(seq))
    else:
        start_temp = int(proportion*len(seq))
        end_temp = len(seq)
    countt = 0
    for temp11 in cluster_st_pat:
        countt += 1
        print countt
        count1 = 0
        for pat_item in temp11:  # ['00,07,13', '08,07,13', '00,07,17', '08,07,17', '00,07,13,20']
            count = 0
            count1 += 1
            for itemst in stateFrame[start_temp:end_temp]:  # itemst 00,07,13,20,01,06,02,01
                if pat_item in itemst:
                    #print 'match'
                    count += 1
                    start = itemst.find(pat_item) / 3
                    # print 'start',start
                    index = stateFrame.index(itemst)
                    # print 'index',index
                    length = pat_item.count(',') + 1
                    # print 'length',length
                    # print 'cf',len(cfrti_data[index])
                    if start + length < len(cfrti_data[index]) / 2:
                        # print 'inside'
                        pat_seq = cfrti_data[index][start * 2:start * 2 + length * 2]
                        # print pat_seq
                        pat = ''
                        for iii in xrange(length - 1):
                            pat = str(pat) + '(' + str(pat_seq[iii * 2]) + ')' + '{' + str(
                                pat_seq[iii * 2 + 3]) + '}'
                        # print pat
                        m = re.finditer(pat, seq[index])
                        try:
                            while True:
                                # print 'found'
                                itt = m.next()
                                # print itt.group()
                                seqwriter(count1, index, itt.group(), itt.start(), itt.end(),
                                          'save_pat_seq_' + str(countt))
                        except StopIteration:
                            pass
                    else:
                        # print 'end of seq'
                        pat_seq = cfrti_data[index][start * 2:]
                        pat = ''
                        for iii in xrange(length - 2):
                            pat = str(pat) + '(' + str(pat_seq[iii * 2]) + ')' + '{' + str(
                                pat_seq[iii * 2 + 3]) + '}'
                        pat = str(pat) + '(' + str(pat_seq[iii * 2 + 2]) + ')' + '{1,500}'
                        m = re.finditer(pat, seq[index])
                        try:
                            while True:
                                # print 'found'
                                itt = m.next()
                                # print itt.group()
                                seqwriter(count1, index, itt.group(), itt.start(), itt.end(),
                                          'save_pat_seq_' + str(countt))
                        except StopIteration:
                            pass
    print 'end of save_pat_seq writing'

########
# Function: CFmodel_train(src_dir,head_dir,method=1) # method: 
# Input:
#   src_dir: 'train_data_ngsim_I80_1.mat' original .mat file
#   head_dir: 'pat_seq_head.txt' # write yourself, the head of the patterns/clusters
#   method: 1 -> testS, 2 -> sym and higgs
# Output:
#   'CFtest_seq_' one save_pat_seq file per .mat file
########
def CFmodel_train(src_dir,head_dir,method):
    data = si.loadmat(src_dir)
    accList = data['acc'][0]  # a list, each element of the list contains one car following period
    dvList = data['dv'][0]
    dxList = data['dx'][0]
    velList = data['vel'][0]
    if method == 1:
        TRANSITIONS_REGEX = "^(?P<indexxz>\d+),(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"
    else:
        TRANSITIONS_REGEX = "^(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"
    #TRANSITIONS_REGEX = "^(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"  # "^(?P<indexxz>\d+),(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"
    with open(head_dir,'r') as he:
        count = 0
        for line1 in he:
            count += 1
            print line1
            with open(str(line1[:-1])+'.txt','r') as ftemp:
                tempp = []
                for line in ftemp:
                    m = re.match(TRANSITIONS_REGEX, line)
                    if m is not None:
                        index_temp = int(m.group("indexx"))
                        start_temp = int(m.group("startt"))
                        end_temp   = int(m.group("endd"))
                        tempp.append([index_temp, start_temp, end_temp])
                print len(tempp)
                acc_temp = []
                dv_temp  = []
                dx_temp  = []
                vel_temp = []
                for i in tempp:
                    acc_temp.append(accList[i[0]][i[1]:i[2]])
                    dv_temp.append(dvList[i[0]][i[1]:i[2]])
                    dx_temp.append(dxList[i[0]][i[1]:i[2]])
                    vel_temp.append(velList[i[0]][i[1]:i[2]])
            si.savemat('CFtest_seq_' + str(count) + '.mat', {'acc': acc_temp, 'dv': dv_temp, 'dx': dx_temp, 'vel': vel_temp})
########
# Function: split original data, as we know in each state/symboli sequence star/end index of each cluster
# Input:
#   src_dir: original numeric data
#   head_dir: path of each clustered data
#   method
# Output:
#   
########            
def CFmodel_train_cluster(src_dir, head_dir, method):
    data = si.loadmat(src_dir)
    accList = data['acc'][0]  # a list, each element of the list contains one car following period
    dvList = data['dv'][0]
    dxList = data['dx'][0]
    velList = data['vel'][0]
    if method == 1:
        TRANSITIONS_REGEX = "^(?P<indexxz>\d+),(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"
    else:
        TRANSITIONS_REGEX = "^(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"
    #TRANSITIONS_REGEX = "^(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"  # "^(?P<indexxz>\d+),(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"
    with open(head_dir,'r') as he:
        count = 0
        for line1 in he:
            count += 1
            #print line1
            with open(str(line1[:-1])+'.txt','r') as ftemp:
                tempp = []
                for line in ftemp:
                    m = re.match(TRANSITIONS_REGEX, line)
                    if m is not None:
                        index_temp = int(m.group("indexx"))
                        start_temp = int(m.group("startt"))
                        end_temp   = int(m.group("endd"))
                        tempp.append([index_temp, start_temp, end_temp])
                print len(tempp), 'len_temp'
                #print tempp[0]
                acc_temp = []
                dv_temp  = []
                dx_temp  = []
                vel_temp = []
                for i in tempp:
                    acc_temp.append(accList[i[0]][i[1]:i[2]])
                    dv_temp.append(dvList[i[0]][i[1]:i[2]])
                    dx_temp.append(dxList[i[0]][i[1]:i[2]])
                    vel_temp.append(velList[i[0]][i[1]:i[2]])
            si.savemat('train_cluster/CFtest_seq_' + str(count) + '.mat', {'acc': acc_temp, 'dv': dv_temp, 'dx': dx_temp, 'vel': vel_temp})
def CFmodel_train1(src_dir,head_dir,method):
    data = si.loadmat(src_dir)
    accList = data['acc'][0]  # a list, each element of the list contains one car following period
    dvList = data['dv'][0]
    dxList = data['dx'][0]
    velList = data['vel'][0]
    if method == 1:
        TRANSITIONS_REGEX = "^(?P<indexxz>\d+),(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"
    else:
        TRANSITIONS_REGEX = "^(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"
    #TRANSITIONS_REGEX = "^(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"  # "^(?P<indexxz>\d+),(?P<indexx>\d+), \[(?P<startt>\d+),(?P<endd>\d+)\]"
    with open(head_dir,'r') as he:
        count = 0
        for line1 in he:
            count += 1
            print line1
            with open(str(line1[:-1])+'.txt','r') as ftemp:
                tempp = []
                for line in ftemp:
                    m = re.match(TRANSITIONS_REGEX, line)
                    if m is not None:
                        index_temp = int(m.group("indexx"))
                        start_temp = int(m.group("startt"))
                        end_temp   = int(m.group("endd"))
                        tempp.append([index_temp, start_temp, end_temp])
                print len(tempp)
                acc_temp = []
                dv_temp  = []
                dx_temp  = []
                vel_temp = []
                for i in tempp:
                    acc_temp.append(accList[i[0]][i[1]:i[2]])
                    dv_temp.append(dvList[i[0]][i[1]:i[2]])
                    dx_temp.append(dxList[i[0]][i[1]:i[2]])
                    vel_temp.append(velList[i[0]][i[1]:i[2]])
            si.savemat('train_cluster/CFtest_seq_' + str(count) + '.mat', {'acc': acc_temp, 'dv': dv_temp, 'dx': dx_temp, 'vel': vel_temp})

########
# Function: create cluster sequences for calibration experiment
# Input:
#   ori_dir: 'ori_seq.txt' orignal symbol sequence
#   pat_match_dir: 'ori/assign_state_I80_1.txt' assigned clusters
#   cfrti_dir: 'ori/rti_full_I80_1.txt' orignal rti output file
#   dir: 1 -> train, 2 -> test
# Output:
#   pat_ori_dir: 'pattern_ori_4testS.txt' # load data in Matlab for sequence info
########
def Testing_testS_processing(ori_dir,pat_match_dir,cfrti_dir,pat_ori_dir,dir):
    cf_seq = []
    with open(cfrti_dir, 'r') as cf:
        cf.readline()
        for line in cf:
            lineWithoutHead = line.strip().split(' ')[1:]
            cf_seq.append(lineWithoutHead)
    pa_seq = []
    with open(pat_match_dir, 'r') as pa:
        for line in pa:
            tempp = line.strip().split(',')[1:]
            pa_seq.append(tempp)
    seq = []
    with open(ori_dir, "r") as rt:
        for line in rt:
            seq.append(line.strip())
    #####################################################
    if dir == 1:
        start_temp = 0
        end_temp = int(proportion*len(seq))
    else:
        start_temp = int(proportion*len(seq))
        end_temp = len(seq)
    count = 0
    dst_head = open(pat_ori_dir, 'w')
    for item_pa in pa_seq[start_temp:end_temp]:
        index = count + start_temp
        count += 1
        length = len(item_pa)
        if length != 0:
            if length < len(cf_seq[index]) / 2:
                pat_temp = ''
                len_temp = 0
                for ii in xrange(length):
                    pat_temp = str(pat_temp) + str(int(item_pa[ii]) + 1) * int(cf_seq[index][ii * 2 + 3])
                    len_temp += int(cf_seq[index][ii * 2 + 3])
                dst_head.write(str(index) + ', ' + str(1) + ', ' + str(len_temp) + ', ' + str(pat_temp))
                dst_head.write('\n')
            else:
                pat_temp = ''
                len_temp = 0
                if length == 1:
                    len_temp = len(seq[index])
                    pat_temp = str(pat_temp) + str(int(item_pa[0]) + 1) * len_temp
                    dst_head.write(str(index) + ', ' + str(1) + ', ' + str(len(seq[index])) + ', ' + str(pat_temp))
                    dst_head.write('\n')
                else:
                    for ii in xrange(length - 1):
                        pat_temp = str(pat_temp) + str(int(item_pa[ii]) + 1) * int(cf_seq[index][ii * 2 + 3])
                        len_temp += int(cf_seq[index][ii * 2 + 3])
                    pat_temp = str(pat_temp) + str(int(item_pa[-1]) + 1) * (len(seq[index]) - len_temp)
                    dst_head.write(str(index) + ', ' + str(1) + ', ' + str(len(seq[index])) + ', ' + str(pat_temp))
                    dst_head.write('\n')
        else:
            if len(cf_seq[index]) < 4:
                len_temp = len(seq[index])
                pat_temp = str(1) * len_temp
            else:
                len_temp = int(cf_seq[index][3])
                pat_temp = str(1) * len_temp
            dst_head.write(str(index) + ', ' + str(1) + ', ' + str(len_temp) + ', ' + str(pat_temp))
            dst_head.write('\n')
    dst_head.close()

########
# Function: create data for symbolized method
# Input:
#   sym_ori_dir: 'symbolic_I80_1/assign_symbol_I80_1.txt'
#   sym_pat_dir: 'symbolic_I80_1/train_cluster_symbolic_I80_1.txt'
# Output:
#   'save_pat_seq_' symbolized training and testing data
########
def Sym_train_processing(sym_ori_dir,sym_pat_dir):
    seq = []
    with open(sym_ori_dir, "r") as rt:
        for line in rt:
            seq.append(line.strip())
    pat_seq = []
    with open(sym_pat_dir, 'r') as pa:
        for line in pa:
            #print line
            sub_seq = []
            if len(line) > 3:
                temp = line.rstrip().split(';')
                for item in temp:
                    tempp = item.split(',')
                    pat_temp = ''
                    for ii in tempp:
                        pat_temp = pat_temp + '(' + str(ii) + '){1,542}'
                    # print tempp,pat_temp
                    sub_seq.append(pat_temp)
                pat_seq.append(sub_seq)
    count = 0
    for sub_pat in pat_seq:
        count += 1
        print 'current cluster',count
        #save_pat_seq.append('save_pat_seq_' + str(count))
        for sub_seq in seq[0:int(proportion*len(seq))]:
            for item in sub_pat:
                m = re.finditer(item, sub_seq)
                try:
                    while True:
                        # print 'found'
                        itt = m.next()
                        # print sub_pat.index(item)
                        seqwriter_n(seq.index(sub_seq), itt.group(), itt.start(), itt.end(),'save_pat_seq_' + str(count))
                except StopIteration:
                    pass

########
# Function: create data for Higgs method
# Input:
#   higgs_ori_dir: 'higgs_I80_1/higgs_I80_1.txt'
# Output:
#   save_pat_seq_' higgs training and testing data
########
def Higgs_train_procssing(higgs_ori_dir):
    sym_seq = []
    with open(higgs_ori_dir, 'r') as sy:
        for line in sy:
            sym_seq.append(line)
    count = 0
    for seq in sym_seq[0:int(proportion*len(sym_seq))]:
        m = re.finditer('a+', seq)
        try:
            while True:
                itt = m.next()
                seqwriter_n(count, itt.group(), itt.start(), itt.end(), 'save_pat_seq_1')
        except StopIteration:
            pass
        m = re.finditer('b+', seq)
        try:
            while True:
                itt = m.next()
                seqwriter_n(count, itt.group(), itt.start(), itt.end(), 'save_pat_seq_2')
        except StopIteration:
            pass
        m = re.finditer('c+', seq)
        try:
            while True:
                itt = m.next()
                seqwriter_n(count, itt.group(), itt.start(), itt.end(), 'save_pat_seq_3')
        except StopIteration:
            pass
        m = re.finditer('d+', seq)
        try:
            while True:
                itt = m.next()
                seqwriter_n(count, itt.group(), itt.start(), itt.end(), 'save_pat_seq_4')
        except StopIteration:
            pass
        count += 1

########
# Function: divide the original data according to the clusters
# Input:
#   state_src_dir: 'data_by_state.mat' transformed original data according to the clusters
#   cfrti_dir: 'ori/rti_full_I80_1.txt' orignal rti output file
#   clusterframe_dir: 'ori/assign_state_I80_1.txt' assigned clusters
# Output:
#   'train_cluster/save_state_seq_' one cluster (state pattern) per .txt file, each file defines cluster number, id of sequence, symbols, start, end
########
def dvi_by_cluster_new(state_src_dir,cfrti_dir,clusterframe_dir):
    stateFrame = []
    with open(clusterframe_dir, 'r') as st:
        temp1 = []
        for line in st:
            temp = []
            temp.append('00')
            for item in line.strip().split(',')[1:]:
                if int(item) < 10:
                    temp.append('0' + item)
                    temp1.append('0' + item)
                else:
                    temp.append(item)
                    temp1.append(item)
            stateFrame.append(','.join(temp))
    print 'startFrame ready' #cluster frame
    #print stateFrame
    state_uni = np.unique(temp1)#number of clusters
    #print state_uni 
    data = si.loadmat(src_dir)#original (no cut) data
    accList = data['acc'][0]
    with open('dvi_cluster_head.txt', 'w') as ft:
        for i in xrange(len(state_uni)):
            # print str(i+1)
            if i < 9:
                ft.write('train_cluster/save_state_seq_0' + str(i + 1))
            else:
                ft.write('train_cluster/save_state_seq_'  + str(i + 1))
            ft.write('\n')
    cfrti_data = []
    cfrti_head = []
    with open(cfrti_dir, "r") as daa:
        daa.readline()
        for line in daa:
            lineWithoutHead = line.strip().split(' ')
            cfrti_head.append(lineWithoutHead[0])
            cfrti_data.append(lineWithoutHead[1:])
    numSequence = len(stateFrame)
    #print numSequence
    numSequence = int(proportion * numSequence)
    #print numSequence
    for i in xrange(numSequence):
        len_state = len(accList[i])
        # print stateFrame[i][-2:]
        # print cfrti_data[i][-2]
        statenum = len(stateFrame[i]) / 3
        if statenum == int(cfrti_head[i]):
            numcount = 0
            for j in xrange(statenum - 1):
                count1 = stateFrame[i][j * 3 + 3:j * 3 + 5]#cluster number
                symbols = cfrti_data[i][j * 2]
                # print stateFrame[i], statenum
                # print count1, 'count1'
                # print symbols, 'symbols'

                start = numcount
                numcount += int(cfrti_data[i][j * 2 + 3])
                end = numcount
                # print cfrti_data[0]
                # print start, end
                if int(count1) < 10:
                    seqwriter(count1, i, symbols, start, end, 'train_cluster/save_state_seq_0' + str(int(count1) + 1))
                else:
                    seqwriter(count1, i, symbols, start, end, 'train_cluster/save_state_seq_'  + str(int(count1) + 1))
            if int(stateFrame[i][-2:]) < 10:
                seqwriter(stateFrame[i][-2:], i, cfrti_data[i][-2], numcount, len_state,
                      'train_cluster/save_state_seq_0' + str(int(stateFrame[i][-2:])+1))
            else:
                seqwriter(stateFrame[i][-2:], i, cfrti_data[i][-2], numcount, len_state,
                      'train_cluster/save_state_seq_'  + str(int(stateFrame[i][-2:])+1))
        else:
            numcount = 0
            for j in xrange(statenum):
                count1 = stateFrame[i][j * 3 + 3:j * 3 + 5]
                symbols = cfrti_data[i][j * 2]
                start = numcount
                numcount += int(cfrti_data[i][j * 2 + 3])
                end = numcount
                if int(count1) < 10:
                    seqwriter(count1, i, symbols, start, end, 'train_cluster/save_state_seq_0' + str(int(count1) + 1))
                else:
                    seqwriter(count1, i, symbols, start, end, 'train_cluster/save_state_seq_'  + str(int(count1) + 1))

########
# Function: divide the original data according to the states
# Input:
#   src_dir: 'train_data_ngsim_I80_1.mat' original .mat file
#   cfrti_dir: 'ori/rti_full_I80_1.txt' orignal rti output file
#   stateframe_dir --> clusterframe_dir: 'ori/assign_state_I80_1.txt' assigned clusters
# Output:
#   'train_cluster/save_state_seq_' one state per .txt file
########
def dvi_by_state(src_dir, cfrti_dir, stateframe_dir):
    stateFrame = []
    with open(stateframe_dir, 'r') as st:
        temp1 = []
        for line in st:
            temp = []
            temp.append('00')
            for item in line.strip().split(',')[1:]:
                if int(item) < 10:
                    temp.append('0' + item)
                    temp1.append('0' + item)
                else:
                    temp.append(item)
                    temp1.append(item)
            stateFrame.append(','.join(temp))
    print 'startFrame ready'
    state_uni = np.unique(temp1)
    data = si.loadmat(src_dir)
    accList = data['acc'][0]
    with open('dvi_state_head.txt', 'w') as ft:
        for i in xrange(len(state_uni)):
            if i < 9:
                ft.write('train_cluster/save_state_seq_0' + str(i + 1))
            else:
                ft.write('train_cluster/save_state_seq_' + str(i + 1))
            ft.write('\n')
    cfrti_data = []
    cfrti_head = []
    with open(cfrti_dir, "r") as daa:
        daa.readline()
        for line in daa:
            lineWithoutHead = line.strip().split(' ')
            cfrti_head.append(lineWithoutHead[0])
            cfrti_data.append(lineWithoutHead[1:])
    numSequence = len(stateFrame)
    #print numSequence
    numSequence = int(proportion* numSequence)
    #print numSequence
    for i in xrange(numSequence):
        len_state = len(accList[i])
        # print stateFrame[i][-2:]
        # print cfrti_data[i][-2]
        statenum = len(stateFrame[i]) / 3
        if statenum == int(cfrti_head[i]):
            numcount = 0
            for j in xrange(statenum - 1):
                count1 = stateFrame[i][j * 3 + 3:j * 3 + 5]
                symbols = cfrti_data[i][j * 2]
                start = numcount
                numcount += int(cfrti_data[i][j * 2 + 3])
                end = numcount
                seqwriter(count1, i, symbols, start, end, 'train_cluster/save_state_seq_' + str(count1))
            seqwriter(stateFrame[i][-2:], i, cfrti_data[i][-2], numcount, len_state,
                      'train_cluster/save_state_seq_' + str(stateFrame[i][-2:]))
        else:
            numcount = 0
            for j in xrange(statenum):
                count1 = stateFrame[i][j * 3 + 3:j * 3 + 5]
                symbols = cfrti_data[i][j * 2]
                start = numcount
                numcount += int(cfrti_data[i][j * 2 + 3])
                end = numcount
                seqwriter(count1, i, symbols, start, end, 'train_cluster/save_state_seq_' + str(count1))

########
# Function: divide the original data according to the clusters
# Input:
#   src_dir: 'train_data_ngsim_I80_1.mat' original .mat file
#   cfrti_dir: 'ori/rti_full_I80_1.txt' orignal rti output file
#   clusterframe_dir: 'ori/assign_state_I80_1.txt' assigned clusters
#   cluster_dir： 'dvi_cluster_head.txt'
#   output_dir: --> test_dir = 'testby_cluster.txt'
# Output:
#   'train_cluster/save_state_seq_', 'testby_cluster.txt'
########
def dvi_by_cluster(src_dir, cfrti_dir, clusterframe_dir, cluster_dir, output_dir):
    stateFrame = []
    with open(clusterframe_dir, 'r') as st:
        temp1 = []
        for line in st:
            temp = []
            temp.append('00')
            for item in line.strip().split(',')[1:]:
                if int(item) < 10:
                    temp.append('0' + item)
                    temp1.append('0' + item)
                else:
                    temp.append(item)
                    temp1.append(item)
            stateFrame.append(','.join(temp))
    print 'startFrame ready'
    state_uni = np.unique(temp1)
    data = si.loadmat(src_dir)
    accList = data['acc'][0]
    with open(cluster_dir, 'w') as ft:
        for i in xrange(len(state_uni)):
            if i < 10:
                ft.write('train_cluster/save_state_seq_0' + str(i))
            else:
                ft.write('train_cluster/save_state_seq_' + str(i))
            ft.write('\n')
    cfrti_data = []
    cfrti_head = []
    with open(cfrti_dir, "r") as daa:
        daa.readline()
        for line in daa:
            lineWithoutHead = line.strip().split(' ')
            cfrti_head.append(lineWithoutHead[0])
            cfrti_data.append(lineWithoutHead[1:])
    numSequence = len(stateFrame)
    #print numSequence
    # for training
    training_len = int(proportion* numSequence)
    for i in xrange(training_len):
        len_state = len(accList[i])
        # print stateFrame[i][-2:]
        # print cfrti_data[i][-2]
        statenum = len(stateFrame[i]) / 3
        if statenum == int(cfrti_head[i]):
            numcount = 0
            for j in xrange(statenum - 1):
                count1 = stateFrame[i][j * 3 + 3:j * 3 + 5]
                symbols = cfrti_data[i][j * 2]
                start = numcount
                numcount += int(cfrti_data[i][j * 2 + 3])
                end = numcount
                seqwriter(count1, i, symbols, start, end, 'train_cluster/save_state_seq_' + str(count1))
            seqwriter(stateFrame[i][-2:], i, cfrti_data[i][-2], numcount, len_state,
                      'train_cluster/save_state_seq_' + str(stateFrame[i][-2:]))
        else:
            numcount = 0
            for j in xrange(statenum):
                count1 = stateFrame[i][j * 3 + 3:j * 3 + 5]
                symbols = cfrti_data[i][j * 2]
                start = numcount
                numcount += int(cfrti_data[i][j * 2 + 3])
                end = numcount
                seqwriter(count1, i, symbols, start, end, 'train_cluster/save_state_seq_' + str(count1))
    # for testing
    testing_len = numSequence - training_len
    testing_seq = []
    f = open(output_dir, 'w')
    for kk in xrange(testing_len):
        i = kk + training_len
        len_state = len(accList[i])
        statenum = len(stateFrame[i]) / 3       
        # print statenum, int(cfrti_head[i]),len_state,stateFrame[i]
        if statenum == int(cfrti_head[i]):
            # print len_state,stateFrame[i], cfrti_data[i]
            seq_temp = []
            numcount = 0
            for j in xrange(statenum - 1):
                count1 = stateFrame[i][j * 3 + 3:j * 3 + 5]
                symbols = cfrti_data[i][j * 2]
                start = numcount
                numcount += int(cfrti_data[i][j * 2 + 3])
                end = numcount
                seq_temp.append(str(count1) * (end-start))
                # print count1, symbols, start, numcount, end, len(seq_temp)
            seq_temp.append(str(stateFrame[i][-2:]) * (len_state - numcount))
            testing_seq.append(''.join(seq_temp))
        else:
            seq_temp = []
            numcount = 0
            for j in xrange(statenum - 1):
                count1 = stateFrame[i][j * 3 + 3:j * 3 + 5]
                symbols = cfrti_data[i][j * 2]
                start = numcount
                numcount += int(cfrti_data[i][j * 2 + 3])
                end = numcount
                seq_temp.append(str(count1) * (end-start))
            testing_seq.append(''.join(seq_temp))
        # print len(testing_seq[-1])
        f.write(str(testing_seq[-1]))
        f.write('\n')   
    f.close()

########
# Function: generate a txt file according to the cluster
# Input:
#   state_src_dir: 'data_by_state.mat' transformed original data according to the clusters
#   cfrti_dir: 'ori/rti_full_I80_1.txt' orignal rti output file
#   clusterframe_dir: 'ori/assign_state_I80_1.txt' assigned clusters
#   output_dir: --> test_dir = 'testby_cluster.txt'
# Output:
#   'testby_cluster.txt'
########
def ClusterSeq_generate(state_src_dir, cfrti_dir, clusterframe_dir, output_dir):
    stateFrame = []
    with open(clusterframe_dir, 'r') as st:
        temp1 = []
        for line in st:
            temp = []
            temp.append('00')
            for item in line.strip().split(',')[1:]:
                if int(item) < 10:
                    temp.append('0' + item)
                    temp1.append('0' + item)
                else:
                    temp.append(item)
                    temp1.append(item)
            stateFrame.append(','.join(temp))
    print 'startFrame ready'    
    #print stateFrame[0]
    cfrti_data = []
    cfrti_head = []
    with open(cfrti_dir, "r") as daa:
        daa.readline()
        for line in daa:
            lineWithoutHead = line.strip().split(' ')
            cfrti_head.append(lineWithoutHead[0])
            cfrti_data.append(lineWithoutHead[1:])
    numSequence = len(stateFrame)
    #print numSequence   
    #print cfrti_data[0]
    #print cfrti_head[0] 
    data = si.loadmat(state_src_dir)
    accList = data['acc'][0]
    f = open(output_dir, 'w')
    testing_seq = []
    cc = 0
    for i in xrange(numSequence):
        # i = 305
        len_state = len(accList[i])
        statenum = len(stateFrame[i]) / 3       
        # print statenum, int(cfrti_head[i]),len_state,stateFrame[i]
        if statenum == 0:
            # print 'error'
            cc = cc + 1
            testing_seq.append('99')
        else:
            if statenum == int(cfrti_head[i]):
                # print len_state,stateFrame[i], cfrti_data[i]
                seq_temp = []
                numcount = 0
                for j in xrange(statenum - 1):
                    count1 = stateFrame[i][j * 3 + 3:j * 3 + 5]
                    symbols = cfrti_data[i][j * 2]
                    start = numcount
                    numcount += int(cfrti_data[i][j * 2 + 3])
                    end = numcount
                    seq_temp.append(str(count1) * (end-start))
                    # print count1, symbols, start, numcount, end, len(seq_temp)
                seq_temp.append(str(stateFrame[i][-2:]) * (len_state - numcount))
                testing_seq.append(''.join(seq_temp))
            else:
                seq_temp = []
                numcount = 0
                for j in xrange(statenum):
                    count1 = stateFrame[i][j * 3 + 3:j * 3 + 5]
                    symbols = cfrti_data[i][j * 2]
                    start = numcount
                    numcount += int(cfrti_data[i][j * 2 + 3])
                    end = numcount
                    seq_temp.append(str(count1) * (end-start))
                testing_seq.append(''.join(seq_temp))
        # print len(testing_seq[-1])
        f.write(str(testing_seq[-1]))
        f.write('\n')   
    f.close()
    print cc
################
def select_simulation_sequences(test_dir, des_path, min_num_state=1):
    with open(test_dir, "r") as h:
        #daa.readline()
        i = 0
        seq_pattern = []
        seq_index = []
        seq_start = []
        seq_end = []
        for line in h:
            lineWithoutHead = line.strip()
            num_state = len(lineWithoutHead)/2
            #print num_state
            if num_state >= min_num_state:
            	seq_index.append(i+1)#matlab starts from 1
            	seq_start.append(0+1)
            	seq_end.append(num_state)
            	pattern_temp = []
                for j in xrange(num_state):
                    pattern_temp.append(lineWithoutHead[j*2+1])
                seq_pattern.append(pattern_temp)
            i+=1
        #print seq_index
        si.savemat(des_path, {'seq_index': seq_index, 'seq_start': seq_start, 'seq_end': seq_end, 'seq_pattern': seq_pattern},oned_as='column')
            #cfrti_head.append(lineWithoutHead[0])
            #cfrti_data.append(lineWithoutHead[1:])

# load files:
# inputs
src_dir = 'data/NGSIM/train_data_ngsim.mat'
stateframe_dir = 'ori/state_frame_I80_1_1.0.txt'
clusterframe_dir = 'ori/assign_state_I80_1_1.0.txt'
cfrti_dir = 'ori/rti_full_I80_1_1.0.txt' #rti input data
# outputs
ori_dir = 'ori_seq.txt'# original data
cluster_dir = 'dvi_cluster_head.txt'
test_dir = 'testby_cluster.txt'
proportion = 1.0


'''
ori_dir = 'ori_seq.txt'# original data
src_dir = 'train_data_ngsim_I80_1.mat'
stateframe_dir = 'proposed_I80_1/state_frame_I80_1.txt'
cfrti_dir = "proposed_I80_1/rti_full_I80_1.txt" #rti input data
pat_dir = 'proposed_I80_1/train_cluster_state_sequence_I80_1.txt'# state sequence in each cluster
head_dir = 'pat_seq_head.txt' # write yourself
pat_match_dir = "proposed_I80_1/assign_state_sequence_I80_1.txt" # cluster file
pat_ori_dir = 'pattern_ori_4testS.txt' # load data in Matlab for sequence info.
sym_dir = 'symbolic_I80_1/assign_symbol_I80_1.txt'
sym_pat_dir = 'symbolic_I80_1/train_cluster_symbolic_I80_1.txt'
sym_ori_dir = 'symbolic_I80_1/train_symbolseq_I80_1.txt'
higgs_ori_dir = 'higgs_I80_1/higgs_I80_1.txt'
'''

### Proposed method procedure
Ori_seq_write(src_dir,cfrti_dir,ori_dir)
Ori2stateframe(src_dir,clusterframe_dir,cfrti_dir)
# StateExtraction(stateframe_dir,cfrti_dir,pat_dir,ori_dir,dir=1) # dir: 1 -> train, 2 -> test
# CFmodel_train(src_dir,head_dir,method=1) # method: 1 -> testS, 2 -> sym and higgs
# Testing_testS_processing(ori_dir,pat_match_dir,cfrti_dir,pat_ori_dir,dir=2) # dir: 1 -> train, 2 -> test

### Symbolic method procedure
# Sym_train_processing(sym_ori_dir,sym_pat_dir)
# CFmodel_train(src_dir,head_dir,method=2) # method: 1 -> testS, 2 -> sym and higgs

### Higgs method procedure
# Higgs_train_procssing(higgs_ori_dir)
# CFmodel_train(src_dir,head_dir,method=2) # method: 1 -> testS, 2 -> sym and higgs

state_src_dir = 'data_by_state.mat'
### divided by state
# dvi_by_state(state_src_dir,cfrti_dir,clusterframe_dir)
# CFmodel_train1(src_dir,'dvi_state_head.txt',method=1) # method: 1 -> testS, 2 -> sym and higgs

### divided by state
# dvi_by_cluster(src_dir, cfrti_dir, clusterframe_dir, cluster_dir, test_dir)
dvi_by_cluster_new(state_src_dir,cfrti_dir,clusterframe_dir)
CFmodel_train_cluster(src_dir,'dvi_cluster_head.txt',method=1) # method: 1 -> testS, 2 -> sym and higgs
ClusterSeq_generate(state_src_dir, cfrti_dir, clusterframe_dir, test_dir)
select_simulation_sequences(test_dir,'pattern_seq_over_1')
print 'end of process'





