function acc = idm_out(dx,dv,vel,acc,para)
%% a(t)=a*(1-(vel(t)/v0)^delta-(S(t)/dx(t))^2)  S(t) = S0+T*vel(t)+(vel(t)*dv(t)/2/sqrt(a*b))
    a = para(1);
    b = para(2);
    v0 = para(3);
    S0  = para(4);
    delta  = para(5);
    T = 0.1;%para(6);
    S  = S0+T*vel+(vel*dv/(2*sqrt(a*b)));
    acc_y = a*(1-(vel/v0)^delta-(S/dx)^2);
    if isnan(acc_y) || isnan(acc)
        acc = 0;
    else
        acc = acc_y;
    end
end