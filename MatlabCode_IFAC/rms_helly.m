function rms = rms_helly(dv,dx,vel,acc,para)
    %% a(t)=C1*dv(t-T)+C2*(dx(t-T)-D(t)),D(t)=alpha+beta*v(t-T)+gama*a(t-T)
    C1 = para(1);
    C2 = para(2);
    alpha = para(3);
    beta  = para(4);
    gama  = para(5);
    D  = alpha+beta*double(vel)+gama*double(acc);
    acc_y = C1*double(dv)+C2*(double(dx)-D);
    if isnan(acc_y) || isnan(acc)
        rms = 0;
    else
        rms = (acc-acc_y).^2;
    end
end