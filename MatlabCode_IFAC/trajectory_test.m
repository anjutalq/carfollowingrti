close all
clear all
clc
%% simulation testing of trajectory
global datasets
%% Helly
minb_helly = [-0.5,-0.5,-10000,-10000,-10000,0.0];
maxb_helly = [ 0.5, 0.5,10000,10000,10000,0.5];
id_helly   = 6;
%% IDM
minb_IDM = [0.01,0.01,1,0.01,4,0.01];%-10*ones(1,I_D);
maxb_IDM = [6,6,70,6,4,6];
id_IDM   = 6;
%% GHR
minb_GHR = [-100,-10,-10,0];%-10*ones(1,I_D);
maxb_GHR = [100,10,10,2];
id_GHR   = 4;
%%
tic
para = [];
load('para_helly_all.mat');
load('para_idm_all.mat');
load('train_data_ngsim.mat');
load('pattern_seq_over5.mat');
len = length(seq_index);
%% frame
for k = 1:300
    index = seq_index(k);
    pat_temp = seq_pattern{k}(2:end);
    dv_temp = dv{index+1}(seq_start(k):seq_end(k))./3.6;
    dx_temp = dx{index+1}(seq_start(k):seq_end(k));
    vel_temp = vel{index+1}(seq_start(k):seq_end(k));
    acc_temp = acc{index+1}(seq_start(k):seq_end(k));
    len_temp = length(dv_temp);
    xf = 0; xl = dx_temp(1); vl = vel_temp(1)+dv_temp(1);
    xf_sim = 0; dx_sim = dx_temp(1); vf_sim = vel_temp(1);dv_sim = dv_temp(1);acc_sim = acc_temp(1);
    xf_sim1 = 0; dx_sim1 = dx_temp(1); vf_sim1 = vel_temp(1);dv_sim1 = dv_temp(1);acc_sim1 = acc_temp(1);
    para = para_helly_all;
    for i = 2:len_temp
        xl(i) = xf(i-1) + dx_temp(i) + vel_temp(i) * 0.1;
        xf(i) = xf(i-1) + vel_temp(i-1) * 0.1;
        vl(i) = vel_temp(i-1) + dv_temp(i-1); 
        xf_sim(i) = xf_sim(i-1) + vf_sim(i-1) * 0.1;
        dv_sim(i) = vl(i-1) - vf_sim(i-1);
        dx_sim(i) = xl(i) - xf_sim(i);
        acc_sim(i) =  helly_out(dx_sim(i),dv_sim(i),vf_sim(i-1),acc_sim(i-1),para);
        vf_sim(i) = vf_sim(i-1) + acc_sim(i) * 0.1;  
        
        xf_sim1(i) = xf_sim1(i-1) + vf_sim1(i-1) * 0.1;
        dv_sim1(i) = vl(i-1) - vf_sim1(i-1);
        dx_sim1(i) = xl(i) - xf_sim1(i);
        acc_sim1(i) =  idm_out(dx_sim1(i),dv_sim1(i),vf_sim1(i-1),acc_sim1(i-1),para_idm_all);
        vf_sim1(i) = vf_sim1(i-1) + acc_sim1(i) * 0.1;
    end   
    figure(1)
    subplot(2,1,1);hold off;plot(dx_temp,'g');hold on;plot(dx_sim,'r');plot(dx_sim1,'b');%plot(xf_sim);%plot(vel_temp);%plot(vl);
%     plot(dx_sim);
    subplot(2,1,2);hold off;plot(vel_temp,'g');hold on;plot(vf_sim,'r');plot(vf_sim1,'b');
    figure(2)
    plot(dv_temp,dx_temp,'r.');
%     datasets = struct('dx',{dx_temp},'dv',{dv_temp},'vel',{vel_temp},'acc',{acc_temp});
%     [a1,b1]  = DE_train_ngsim(data,id_IDM,minb_IDM,maxb_IDM,'objfun_IDM_traj');
%     [a2,b2]  = DE_train_ngsim(data,id_helly,minb_helly,maxb_helly,'objfun_helly_traj');
end
% for i = 1:1
%     datasets = struct('dx',{data.dx{i}},'dv',{data.dv{i}},'vel',{data.vel{i}},'acc',{data.acc{i}});
%     [a,b]  = DE_train_ngsim(data,id_helly,minb_helly,maxb_helly,'objfun_helly');
% %     [a,b]  = DE_train_ngsim(data,id_IDM,minb_IDM,maxb_IDM,'objfun_IDM');
%     para{i} = [a,b.FVr_oa];
% %     [a,b]  = DE_train_ngsim(data,id_GHR,minb_GHR,maxb_GHR,'objfun_GHR');
% % 	[a,b]  = DE_train_ngsim(data,id_IDM,minb_IDM,maxb_IDM,'objfun_IDM');
% end

%%
toc




