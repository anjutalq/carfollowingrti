
if 0
    format short
    rms = [];
%     load('para_helly_sym.mat', 'para') 
    load('para_helly_testS.mat', 'para')
%     load('para_idm_testS.mat', 'para')
%     load('para_idm_sym.mat', 'para')
    obs = {};cal = {};rms = {};
    for i = 1:4
        testfile = ['test/CFtest_seq_',num2str(i),'.mat'];
        data = load(testfile);
        obs_data = {};cal_data = {};rms_data = [];
        for j = 1:4
            para1 = para{j}(1:6);
            [obs_data_temp, cal_data_temp, rmse]  = DE_test_ngsim(data , para1 , 'helly');
            obs_data{j} = obs_data_temp;
            cal_data{j} = cal_data_temp;
            rms_data(j) = rmse;
        end
        obs{i} = obs_data;
        cal{i} = cal_data;
        rms{i} = rms_data;
        rms_train(i) = para{i}(7);
    %     para1 = [-0.0038   -0.0102   20.6457    0.0877   97.7328    0.2830].*[0.1 0.1 1 10 10 0.1];
    %     [obs_data, cal_data, rmse]  = DE_test_ngsim(data , para1 , 'helly');
    % %     index = 1:length(obs_data);
    % %     plot(index,obs_data,'r.',index,cal_data,'b.');
    %     rms = [rms, rmse];
    
    end
    rmss = rms{1};
    cell2mat(rms')
    rms_train
end
%%%%%  obs{datasets 1:6 }{parasets 1:6 }{seq~}
if 0
    close all
    clear all
    clc
    datasets = 2; parasets = 6; index = 1;
%     temp = cal{i};
%     cal_acc = temp{index};
    testfile = ['testS/test_seq_',num2str(datasets),'.mat'];
    data = load(testfile);
    len = length(data.acc);
    figure(1)
    hold off
    for i = 1:len
        plot(data.vel{i},'b');%,'MarkerSize',2);
        hold on
        plot(data.acc{i},'r');
        plot(data.dx{i},'g');
    end
    set(gca,'FontSize',10, 'FontName','Arial')
    xlabel('v (m)','FontSize',10,'FontName','Arial')
    ylabel('t (s)','FontSize',10,'FontName','Arial')
    grid on
   
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0
    close all
    clear all
    clc
    datasets = 2; parasets = 6; index = 1;
    for k = 1:6
        testfile = ['testS/test_seq_',num2str(k),'.mat'];
        data{k} = load(testfile);
    end
    
    colorm = jet(6);
    figure(1)
    hold off
    for index = 1:6
        len = length(data{index}.acc);
        for i = 1:len
            plot3(data{index}.dv{i},data{index}.dx{i},data{index}.vel{i},'Color',colorm(index,:));%,'MarkerSize',2);
            hold on
%             plot(data.acc{i},'r');
%             plot(data.dx{i},'g');
        end   
    end   
    
    set(gca,'FontSize',10, 'FontName','Arial')
    xlabel('dv (m)','FontSize',10,'FontName','Arial')
    ylabel('dx (m/s)','FontSize',10,'FontName','Arial')
    zlabel('vel (m/s)','FontSize',10,'FontName','Arial')
    grid on
   
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0
    close all
    clear all
    clc
    datasets = 2; parasets = 6; index = 1;
    for k = 1:6
        testfile = ['testS/test_seq_',num2str(k),'.mat'];
        data{k} = load(testfile);
    end
    
    colorm = jet(6);
    figure(1)
    hold off
    for index = 1:6
        len = length(data{index}.acc);
        for i = 1:len
            plot(data{index}.dx{i},'Color',colorm(index,:));%,'MarkerSize',2);
            hold on
%             plot(data.acc{i},'r');
%             plot(data.dx{i},'g');
        end   
    end   
    
    set(gca,'FontSize',10, 'FontName','Arial')
    xlabel('t (m)','FontSize',10,'FontName','Arial')
    ylabel('dx (m/s)','FontSize',10,'FontName','Arial')
    grid on
   
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0
    close all
    clear all
    clc
    load('pattern_seq_over5.mat');
    load('train_data_ngsim.mat');
    len = length(seq_index);
%     ori_seq = textread('ori_seq.txt','%s','headerlines',1);
    colorm = jet(6);
    for k = 698:len
        index = seq_index(k);
        pat_temp = seq_pattern{k}(2:end);
        dv_temp = dv{index+1}(seq_start(k):seq_end(k));
        dx_temp = dx{index+1}(seq_start(k):seq_end(k));
        vel_temp = vel{index+1}(seq_start(k):seq_end(k));
        acc_temp = acc{index+1}(seq_start(k):seq_end(k));
        len_temp = length(dv_temp);
        figure(1)
        hold off
        unique(pat_temp)
        plot3(dv_temp(1),dx_temp(1),vel_temp(1),'r*');
        hold on
        plot3(dv_temp(len_temp),dx_temp(len_temp),vel_temp(len_temp),'g*');
        plot3(dv_temp(round(len_temp/2)),dx_temp(round(len_temp/2)),vel_temp(round(len_temp/2)),'b*');
        for i = 1:len_temp
            plot3(dv_temp(i),dx_temp(i),vel_temp(i),'.','Color',colorm(str2num(pat_temp(i)),:));
            hold on;
        end
        title(['current index: ',num2str(index),' k: ',num2str(k)])
        set(gca,'FontSize',10, 'FontName','Arial')
        xlabel('dv (m/s)','FontSize',10,'FontName','Arial')
        ylabel('dx (m)','FontSize',10,'FontName','Arial')
        zlabel('vel (m/s)','FontSize',10,'FontName','Arial')
        grid on
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0
    close all
    clear all
    clc
    format short
    load para_helly_all.mat   
    %%
    rms = [];
    for i = 1:6
        testfile = ['testS/test_seq_',num2str(i),'.mat'];
        data = load(testfile);
        para1 = para_helly_all(1:6);
        [obs_data, cal_data, rmse]  = DE_test_ngsim(data , para1 , 'helly');
        %     index = 1:length(obs_data);
        %     plot(index,obs_data,'r.',index,cal_data,'b.');
        rms = [rms, rmse];
    end
    rms
   
end

%%%%%%%%%%%%%%%%%%%%%  test  %%%%%%%%%%%%%%%%%%%%%
if 0
    close all
    clear all
    clc
    load('pattern_seq_over5.mat');
    load('train_data_ngsim.mat');
    load('para_helly_all.mat');
    load('para_helly_testS.mat', 'para')
    len = length(seq_index);
%     ori_seq = textread('ori_seq.txt','%s','headerlines',1);
    colorm = jet(6);
    cost_pat = [];
    cost_all = [];
    for k = 1:len
        index = seq_index(k);
        pat_temp = seq_pattern{k}(2:end);
        dv_temp = dv{index+1}(seq_start(k):seq_end(k));
        dx_temp = dx{index+1}(seq_start(k):seq_end(k));
        vel_temp = vel{index+1}(seq_start(k):seq_end(k));
        acc_temp = acc{index+1}(seq_start(k):seq_end(k));
        len_temp = length(dv_temp);
        cost_pat_temp = 0;
        cost_all_temp = 0;
        for i = 1:len_temp
            cost_pat_temp = cost_pat_temp + rms_helly(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para{str2num(pat_temp(i))});
            cost_all_temp = cost_all_temp + rms_helly(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_helly_all);
        end
        cost_pat = [cost_pat,sqrt(cost_pat_temp/len_temp)];
        cost_all = [cost_all,sqrt(cost_all_temp/len_temp)];
    end
    cost_pat1 = max(cost_pat);
    cost_all1 = max(cost_all);
end
% cost_pat1 =
% 
%     0.0070
% 
% 
% cost_all1 =
% 
%     0.3735

%%%%%%%%%%%%%%%%%%%%%  sym test  %%%%%%%%%%%%%%%%%%%%%
if 0
    close all
    clear all
    clc
    format short
    colorm = jet(6);
    cost_pat = [];
    cost_sym = [];
    cost_all = [];
    load('test_gross_820.mat');
    load('para_helly_all.mat');
    len1 = length(dx);
    for ii = 1:len1
        dv_temp = dv{ii};
        dx_temp = dx{ii};
        vel_temp = vel{ii};
        acc_temp = acc{ii};
        len_temp = length(dv_temp);
        cost_all_temp = 0;
        for i = 1:len_temp
            cost_all_temp = cost_all_temp + rms_helly(double(dv_temp(i)),double(dx_temp(i)),double(vel_temp(i)),double(acc_temp(i)),para_helly_all);
        end
        cost_all = [cost_all,sqrt(cost_all_temp/len_temp)];
    end
    load('para_helly_sym.mat', 'para');
    para_sym = para;
    ori_seq = textread('assignSymbol9.txt','%s','headerlines',0);
    for ii = 1:820
        dv_temp = dv{ii};
        dx_temp = dx{ii};
        vel_temp = vel{ii};
        acc_temp = acc{ii};
        sym_temp = ori_seq{ii+3279};
        len_temp = length(dv_temp);
        cost_sym_temp = 0;
        for i = 1: len_temp
            cost_sym_temp = cost_sym_temp + rms_helly(double(dv_temp(i)),double(dx_temp(i)),double(vel_temp(i)),double(acc_temp(i)),para_sym{1+str2num(sym_temp(i*2-1))});
        end
        cost_sym = [cost_sym,sqrt(cost_sym_temp/len_temp)];
    end
    load('train_data_ngsim.mat');
    load('para_helly_testS.mat', 'para');
    para_testS = para;
    load('pattern_seq_testS.mat');
    len = length(seq_index);%490
    for k = 1:len
        index = seq_index(k);
        pat_temp = seq_pattern{k}(2:end);
        sym_temp = ori_seq{index+1};
        dv_temp = dv{index+1}(seq_start(k):seq_end(k));
        dx_temp = dx{index+1}(seq_start(k):seq_end(k));
        vel_temp = vel{index+1}(seq_start(k):seq_end(k));
        acc_temp = acc{index+1}(seq_start(k):seq_end(k));
        len_temp = length(dv_temp);
        cost_pat_temp = 0;
        for i = 1:len_temp
            cost_pat_temp = cost_pat_temp + rms_helly(double(dv_temp(i)),double(dx_temp(i)),double(vel_temp(i)),double(acc_temp(i)),para_testS{str2num(pat_temp(i))});
        end
        cost_pat = [cost_pat,sqrt(cost_pat_temp/len_temp)];
    end
    resultmax = [max(cost_all),max(cost_sym),max(cost_pat)]
    resultmin = [min(cost_all),min(cost_sym),min(cost_pat)]
    resultavg = [mean(cost_all),mean(cost_sym),mean(cost_pat)]
end
% cost_pat1 =
% 
%     0.0070
% 
% 
% cost_all1 =
% 
%     0.3735

%%%%%%%%%%%%%%%%%%%%%  idm sym test  %%%%%%%%%%%%%%%%%%%%%
if 0
    close all
    clear all
    clc
    format short
    colorm = jet(6);
    cost_pat = [];
    cost_sym = [];
    cost_all = [];
    load('test_gross_820.mat');
    load('para_idm_all.mat');
    len1 = length(dx);
    for ii = 1:len1
        dv_temp = dv{ii};
        dx_temp = dx{ii};
        vel_temp = vel{ii};
        acc_temp = acc{ii};
        len_temp = length(dv_temp);
        cost_all_temp = 0;
        for i = 1:len_temp
            cost_all_temp = cost_all_temp + rms_idm(double(dv_temp(i)),double(dx_temp(i)),double(vel_temp(i)),double(acc_temp(i)),para_idm_all);
        end
        cost_all = [cost_all,sqrt(cost_all_temp/len_temp)];
    end
    load('para_idm_sym.mat', 'para');
    para_sym = para;
    ori_seq = textread('assignSymbol9.txt','%s','headerlines',0);
    for ii = 30:400
        dv_temp = dv{ii};
        dx_temp = dx{ii};
        vel_temp = vel{ii};
        acc_temp = acc{ii};
        sym_temp = ori_seq{ii+3279};
        len_temp = length(dv_temp);
        cost_sym_temp = 0;
        for i = 1: len_temp
            cost_sym_temp = cost_sym_temp + rms_idm(double(dv_temp(i)),double(dx_temp(i)),double(vel_temp(i)),double(acc_temp(i)),para_sym{1+str2num(sym_temp(i*2-1))});
        end
        cost_sym = [cost_sym,sqrt(cost_sym_temp/len_temp)];
    end
    load('train_data_ngsim.mat');
    load('para_idm_testS.mat', 'para');
    para_testS = para;
    load('pattern_seq_testS.mat');
    len = length(seq_index);%490
    for k = 1:len
        index = seq_index(k);
        pat_temp = seq_pattern{k}(2:end);
        sym_temp = ori_seq{index+1};
        dv_temp = dv{index+1}(seq_start(k):seq_end(k));
        dx_temp = dx{index+1}(seq_start(k):seq_end(k));
        vel_temp = vel{index+1}(seq_start(k):seq_end(k));
        acc_temp = acc{index+1}(seq_start(k):seq_end(k));
        len_temp = length(dv_temp);
        cost_pat_temp = 0;
        for i = 1:len_temp
            cost_pat_temp = cost_pat_temp + rms_idm(double(dv_temp(i)),double(dx_temp(i)),double(vel_temp(i)),double(acc_temp(i)),para_testS{str2num(pat_temp(i))});
        end
        cost_pat = [cost_pat,sqrt(cost_pat_temp/len_temp)];
    end
    resultmax = [max(cost_all),max(cost_sym),max(cost_pat)]
    resultmin = [min(cost_all),min(cost_sym),min(cost_pat)]
    resultavg = [mean(cost_all),mean(cost_sym),mean(cost_pat)]
end

%%%%%%%%%%%%%%%%%%%%%  time-space comparison  %%%%%%%%%%%%%%%%%%%%%
if 0
    close all
    clear all
    clc
%     load('pattern_seq_over5.mat');
%     load('train_data_ngsim.mat');
    load('para_helly_all.mat');
    load('para_helly_testS.mat', 'para')
%     ori_seq = textread('ori_seq.txt','%s','headerlines',1);
    colorm = jet(6);
    figure(1)
    hold off
    leadx = [];
    t = 0:0.1:30;
    leadv = 10;
    fox   = [0 0 0 0 0 0];
    fov   = [10 10 10 10 10 10];
    foacc = [0 0 0 0 0 0];
    for k = 0:0.1:30
        leadx = [leadx,10 + k * leadv];     
        for i = 1:6
            C1 = para{i}(1);
            C2 = para{i}(2);
            alpha = para{i}(3);
            beta  = para{i}(4);
            gama  = para{i}(5);
            D  = alpha+beta*fov(i)+gama*foacc(i);
            acc_y = C1*(leadv-fov(i))+C2*((leadx-fox(i))-D);
            foacc(i) = foacc(i)+acc_y;
            fov(i)   = fov(i) + acc_y * 0.1;
            fox(i)   = fox(i) + fov(i) * 0.1;
            temp = fox_all(i);
            fox_all
        end

%         fo1 = [fo1,fo1_temp];
%         index = seq_index(k);
%         pat_temp = seq_pattern{k}(2:end);
%         dv_temp = dv{index+1}(seq_start(k):seq_end(k));
%         dx_temp = dx{index+1}(seq_start(k):seq_end(k));
%         vel_temp = vel{index+1}(seq_start(k):seq_end(k));
%         acc_temp = acc{index+1}(seq_start(k):seq_end(k));
%         len_temp = length(dv_temp);
%         cost_pat_temp = 0;
%         cost_all_temp = 0;
%         for i = 1:len_temp
%             cost_pat_temp = cost_pat_temp + rms_helly(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para{str2num(pat_temp(i))});
%             cost_all_temp = cost_all_temp + rms_helly(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_helly_all);
%         end
%         cost_pat = [cost_pat,sqrt(cost_pat_temp/len_temp)];
%         cost_all = [cost_all,sqrt(cost_all_temp/len_temp)];
    end
    plot(t,leadx,'k-');
    hold on
    legend('leader');
    grid on
    title('time-space comparison');
    set(gca,'FontSize',10, 'FontName','Arial')
    xlabel('t (s)','FontSize',10,'FontName','Arial')
    ylabel('position (m)','FontSize',10,'FontName','Arial')
end
% cost_pat1 =
% 
%     0.0070
% 
% 
% cost_all1 =
% 
%     0.3735

%%%%%%%%%%%%%%%%%%%%%  sym test  %%%%%%%%%%%%%%%%%%%%%
if 0
    close all
    clear all
    clc
    load('train_data_ngsim.mat');
    load('para_helly_all.mat');
    load('para_helly_sym.mat', 'para');
    para_sym = para;
    load('para_helly_testS.mat', 'para');
    para_testS = para;
%     load('para_helly_sym.mat', 'para')
    load('pattern_seq_over5.mat');
    ori_seq = textread('symbolCluster.txt','%s','headerlines',0);
    len = length(seq_index);%len = length(ori_seq);
    colorm = jet(6);
    for k = 29
        index = seq_index(k);
        pat_temp = seq_pattern{k}(2:end);
        sym_temp = ori_seq{index+1};
        dv_temp = dv{index+1}(seq_start(k):seq_end(k));
        dx_temp = dx{index+1}(seq_start(k):seq_end(k));
        vel_temp = vel{index+1}(seq_start(k):seq_end(k));
        acc_temp = acc{index+1}(seq_start(k):seq_end(k));
        len_temp = length(dv_temp);
        acc_ori = [];
        acc_pat_temp = [];
        acc_sym_temp = [];
        acc_all_temp = [];
        for i = 1:len_temp
            acc_ori = [acc_ori, acc_temp(i)];
            acc_pat_temp = [acc_pat_temp,acc_helly(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_testS{str2num(pat_temp(i))})];
            acc_sym_temp = [acc_sym_temp,acc_helly(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_sym{1+str2num(sym_temp(i*2-1))})];
            acc_all_temp = [acc_all_temp,acc_helly(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_helly_all)];
        end           
    end
    t = 0:0.1:len_temp/10-0.1;
    figure(1)
    hold off
    plot(t,acc_ori,'b','linewidth',2);%,'linewidth',2
    hold on
    plot(t,acc_all_temp,'Color',colorm(6,:));%,'Color',colorm(6,:)
    plot(t,acc_sym_temp,'k--');
    plot(t,acc_pat_temp,'r--');
    legend('Benchmark','Gross-fitting','Symbol-based','Proposed');
%     title('Fitting error of a car-following sequence')
    set(gca,'FontSize',12, 'FontName','Arial')
    xlabel('time (s)','FontSize',12,'FontName','Arial')
    ylabel('acceleration (m/s^2)','FontSize',12,'FontName','Arial')
    grid on
    axes('Position',[0.63,0.17,0.25,0.22]);% x ori,y ori, right_width, up_length
    plot(t,acc_ori,'b','linewidth',2);
    hold on
    plot(t,acc_all_temp,'Color',colorm(6,:));%,'Color',colorm(6,:)
    plot(t,acc_sym_temp,'k--');
    plot(t,acc_pat_temp,'r--');
    grid on
end


%%%%%%%%%%%%%%%%%%%%% idm sym test  %%%%%%%%%%%%%%%%%%%%%
if 0
    close all
    clear all
    clc
    load('train_data_ngsim.mat');
    load('para_idm_all.mat');
    load('para_idm_sym.mat', 'para');
    para_sym = para;
    load('para_idm_testS.mat', 'para');
    para_testS = para;
%     load('para_helly_sym.mat', 'para')
    load('pattern_seq_over5.mat');
    ori_seq = textread('symbolCluster.txt','%s','headerlines',0);
    len = length(seq_index);%len = length(ori_seq);
    colorm = jet(6);
    for k = 29%1722
        index = seq_index(k);
        pat_temp = seq_pattern{k}(2:end);
        sym_temp = ori_seq{index+1};
        dv_temp = dv{index+1}(seq_start(k):seq_end(k));
        dx_temp = dx{index+1}(seq_start(k):seq_end(k));
        vel_temp = vel{index+1}(seq_start(k):seq_end(k));
        acc_temp = acc{index+1}(seq_start(k):seq_end(k));
        len_temp = length(dv_temp);
        acc_ori = [];
        acc_pat_temp = [];
        acc_sym_temp = [];
        acc_all_temp = [];
        for i = 1:len_temp
            acc_ori = [acc_ori, acc_temp(i)];
            acc_pat_temp = [acc_pat_temp,acc_idm(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_testS{str2num(pat_temp(i))})];
            acc_sym_temp = [acc_sym_temp,acc_idm(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_sym{1+str2num(sym_temp(i*2-1))})];
            acc_all_temp = [acc_all_temp,acc_idm(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_idm_all)];
        end           
    end
    t = 0:0.1:len_temp/10-0.1;
    figure(1)
    hold off
    plot(t,acc_ori,'b','linewidth',2);%,'linewidth',2
    hold on
    plot(t,acc_all_temp,'Color',colorm(6,:));%,'Color',colorm(6,:)
    plot(t,acc_sym_temp,'k--');
    plot(t,acc_pat_temp,'r--');
    legend('Benchmark','Baseline','Symbol-based','Proposed');
%     title('Fitting error of a car-following sequence')
    set(gca,'FontSize',12, 'FontName','Arial')
    xlabel('time (s)','FontSize',12,'FontName','Arial')
    ylabel('acceleration (m/s^2)','FontSize',12,'FontName','Arial')
    grid on
    axes('Position',[0.63,0.17,0.25,0.22]);% x ori,y ori, right_width, up_length
    plot(t,acc_ori,'b','linewidth',2);
    hold on
    plot(t,acc_all_temp,'Color',colorm(6,:));%,'Color',colorm(6,:)
    plot(t,acc_sym_temp,'k--');
    plot(t,acc_pat_temp,'r--');
    grid on
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0
    close all
    clear all
    clc
    load('pattern_seq_over5.mat');
    load('train_data_ngsim.mat');
    len = length(seq_index);
%     ori_seq = textread('ori_seq.txt','%s','headerlines',1);
    colorm = jet(6);
    for k = 27
        index = seq_index(k);
        pat_temp = seq_pattern{k}(2:end);
        dv_temp = dv{index+1}(seq_start(k):seq_end(k));
        dx_temp = dx{index+1}(seq_start(k):seq_end(k));
        vel_temp = vel{index+1}(seq_start(k):seq_end(k));
        acc_temp = acc{index+1}(seq_start(k):seq_end(k));
        len_temp = length(dv_temp);
        figure(1)
        hold off
        tt = unique(pat_temp);
        pa1 = [];pa2 = [];pa3 = [];pa4 = [];pa5 = [];pa6 = [];
        for i = 1:len_temp
            switch pat_temp(i)
                case '1'
                    pa1 = [pa1;[dv_temp(i),dx_temp(i),vel_temp(i)]];
                case '2'
                    pa2 = [pa2;[dv_temp(i),dx_temp(i),vel_temp(i)]];
                case '3'
                    pa3 = [pa3;[dv_temp(i),dx_temp(i),vel_temp(i)]];
                case '4'
                    pa4 = [pa4;[dv_temp(i),dx_temp(i),vel_temp(i)]];
                case '5'
                    pa5 = [pa5;[dv_temp(i),dx_temp(i),vel_temp(i)]];
                case '6'
                    pa6 = [pa6;[dv_temp(i),dx_temp(i),vel_temp(i)]];
                otherwise
                    break;
             end   
%             plot3(dv_temp(i),dx_temp(i),vel_temp(i),'.','Color',colorm(str2num(pat_temp(i)),:));
        end
        plot3(pa3(:,1)',pa3(:,2)',pa3(:,3)','Color',colorm(3,:),'linewidth',2);
        hold on
        plot3(pa4(:,1)',pa4(:,2)',pa4(:,3)','Color',colorm(4,:),'linewidth',2);
        plot3(pa5(:,1)',pa5(:,2)',pa5(:,3)','Color',colorm(5,:),'linewidth',2);
        plot3(pa6(:,1)',pa6(:,2)',pa6(:,3)','Color',colorm(6,:),'linewidth',2);
%         plot3(dv_temp(1),dx_temp(1),vel_temp(1),'r*');
%         plot3(dv_temp(len_temp),dx_temp(len_temp),vel_temp(len_temp),'k*');
        legend('cluster 3','cluster 4','cluster 5','cluster 6');%,'start point','end point');
%         title('pattern based car-following sequence')
        set(gca,'FontSize',12, 'FontName','Arial')
        xlabel('range rate (m/s)','FontSize',12,'FontName','Arial')
        ylabel('range (m)','FontSize',12,'FontName','Arial')
        zlabel('v (m/s)','FontSize',12,'FontName','Arial')
        grid on
    end
end



%%%%%%%%%%%%%%%%%%%%%  idm percentage test  %%%%%%%%%%%%%%%%%%%%%
if 0
    close all
    clear all
    clc
    load('train_data_ngsim.mat');
    load('para_idm_all.mat');
    load('para_idm_testS.mat', 'para');
    para_testS = para;
    load('para_idm_sym.mat', 'para');
    para_sym = para;
    load('pattern_seq_over5.mat');
    ori_seq = textread('symbolCluster.txt','%s','headerlines',0);
    len = length(seq_index);%len = length(ori_seq);
    colorm = jet(6);
    cost_pat = [];
    cost_sym = [];
    cost_all = [];
    for k = 1:len
        index = seq_index(k);
        pat_temp = seq_pattern{k}(2:end);
        sym_temp = ori_seq{index+1};
        dv_temp = dv{index+1}(seq_start(k):seq_end(k));
        dx_temp = dx{index+1}(seq_start(k):seq_end(k));
        vel_temp = vel{index+1}(seq_start(k):seq_end(k));
        acc_temp = acc{index+1}(seq_start(k):seq_end(k));
        len_temp = length(dv_temp);
        cost_pat_temp = 0;
        cost_sym_temp = 0;
        cost_all_temp = 0;
        for i = 1:len_temp
            cost_pat_temp = cost_pat_temp + per_idm(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_testS{str2num(pat_temp(i))});
            cost_sym_temp = cost_sym_temp + per_idm(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_sym{1+str2num(sym_temp(i*2-1))});
            cost_all_temp = cost_all_temp + per_idm(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_idm_all);
        end
        sum_acc = sum(abs(acc_temp));
%         sqrt((cost_pat_temp/len_temp)/(sum_acc/len_temp));
        if sum_acc == 0
            aaa = 1
        end
        cost_pat = [cost_pat,sqrt((cost_pat_temp/len_temp)/(sum_acc/len_temp))];
        cost_sym = [cost_sym,sqrt((cost_sym_temp/len_temp)/(sum_acc/len_temp))];
        cost_all = [cost_all,sqrt((cost_all_temp/len_temp)/(sum_acc/len_temp))];

             
    end
    format short
    resultmax = [max(cost_pat),max(cost_sym),max(cost_all)]
    resultmin = [min(cost_pat),min(cost_sym),min(cost_all)]
    resultavg = [mean(cost_pat),mean(cost_sym),mean(cost_all)]
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%% Higgs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if 1
    close all
    clear all
    clc
    load('test_gross_820.mat');
    load('para_helly_all.mat');
    load('para_idm_all.mat');
    load('para_helly_Higgs.mat','para');
    para_helly_Higgs = para;
    load('para_idm_Higgs.mat','para');
    para_idm_Higgs = para;
    test_seq = textread('symbolHiggs4.txt','%s','headerlines',3279);
    colorm = jet(6);
    cost_helly_all = [];
    cost_helly_Higgs = [];
    cost_idm_all = [];
    cost_idm_Higgs = [];
    len = length(dv);
    for k = 1:len
        pat_temp = abs(test_seq{k}-96);
        dv_temp = dv{k};
        dx_temp = dx{k};
        vel_temp = vel{k};
        acc_temp = acc{k};
        len_temp = length(dv_temp);
        cost_helly_all_temp = 0;
        cost_helly_Higgs_temp = 0;
        cost_idm_all_temp = 0;
        cost_idm_Higgs_temp = 0;
        for i = 1:len_temp
            cost_helly_all_temp = cost_helly_all_temp + rms_helly(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_helly_all);
            cost_helly_Higgs_temp = cost_helly_Higgs_temp + rms_helly(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_helly_Higgs{pat_temp(i)});
            cost_idm_all_temp = cost_idm_all_temp + rms_idm(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_idm_all);
            cost_idm_Higgs_temp = cost_idm_Higgs_temp + rms_idm(dv_temp(i),dx_temp(i),vel_temp(i),acc_temp(i),para_idm_Higgs{pat_temp(i)});
        end
        cost_helly_all = [cost_helly_all,sqrt(cost_helly_all_temp/len_temp)];
        cost_helly_Higgs = [cost_helly_Higgs,sqrt(cost_helly_Higgs_temp/len_temp)];
        cost_idm_all = [cost_idm_all,sqrt(cost_idm_all_temp/len_temp)];
        cost_idm_Higgs = [cost_idm_Higgs,sqrt(cost_idm_Higgs_temp/len_temp)];
    end
    format short
    mean_err = [mean(cost_helly_all),mean(cost_helly_Higgs),mean(cost_idm_all),mean(cost_idm_Higgs)]
    max_err = [max(cost_helly_all),max(cost_helly_Higgs),max(cost_idm_all),max(cost_idm_Higgs)]
    min_err = [min(cost_helly_all),min(cost_helly_Higgs),min(cost_idm_all),min(cost_idm_Higgs)]
end

















