function acc = helly_out(dx,dv,vel,acc,para);
    %% a(t)=C1*dv(t-T)+C2*(dx(t-T)-D(t)),D(t)=alpha+beta*v(t-T)+gama*a(t-T)
    C1 = para(1);
    C2 = para(2);
    alpha = para(3);
    beta  = para(4);
    gama  = para(5);
    D  = alpha+beta*vel+gama*acc;
    acc_y = C1*dv+C2*(dx-D);
    if isnan(acc_y) || isnan(acc)
        acc = 0;
    else
        acc = acc_y;
    end
end