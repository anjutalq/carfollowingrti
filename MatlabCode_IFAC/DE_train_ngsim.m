function [bestpara,rmse] = DE_train_ngsim(dataset,id,minb,maxb,objfun)
% global datasets;
% % boundary
% %%
% datasets = dataset;
% dx = datasets.dx;
% dv = datasets.dv;
% vel = datasets.vel;
% acc = datasets.acc;

global count;
global storedata;
count  = 0;
storedata  = [];
F_VTR = 0.01;
% I_D		number of parameters of the objective function
I_D = id;
FVr_minbound = minb;%[-100,-100,-100,-100,-100,0];%-10*ones(1,I_D);
FVr_maxbound = maxb;%[100,100,100,100,100,2];     %+10*ones(1,I_D);
I_bnd_constr = 1;  %1: use bounds as bound constraints, 0: no bound constraints
I_NP = 15; %	I_NP            number of population members
I_itermax = 100;    % I_itermax       maximum number of iterations (generations)
F_weight = 0.5; % F_weight        DE-stepsize F_weight ex [0, 2]
F_CR = 0.9;% F_CR            crossover(����) probabililty�����ʣ� constant ex [0, 1]
I_strategy = 1
I_refresh = 1;
I_plotting = 0;
%-----bound at x-value +/- 1.2--------------------------------------------
FVr_bound =[0,1.2,1.88,3.119,6.06879,...
11.25312,20.93868,38.99973,...
72.66066687999998,135.385869312,...
252.2654194687999, 470.0511374131198,...
875.857310322687,  1632.00640736133,...
3040.958067344504, 5666.29295426548,...
10558.14502289265, 19673.25510067688,...
36657.66721873185, 68305.14622427958,...
127274.6837195391];
S_struct.FVr_bound  = FVr_bound;
I_lentol   = 101;
FVr_x      = 1; %ordinate running from -1 to +1 liang is sb
FVr_lim = 0;   %upper limit is 1
%-----tie all important values to a structure that can be passed along----
S_struct.I_lentol   = I_lentol;
S_struct.FVr_x      = FVr_x;
S_struct.I_NP         = I_NP;
S_struct.F_weight     = F_weight;
S_struct.F_CR         = F_CR;
S_struct.I_D          = I_D;
S_struct.FVr_minbound = FVr_minbound;
S_struct.FVr_maxbound = FVr_maxbound;
S_struct.I_bnd_constr = I_bnd_constr;
S_struct.I_itermax    = I_itermax;
S_struct.F_VTR        = F_VTR;
S_struct.I_strategy   = I_strategy;
S_struct.I_refresh    = I_refresh;
S_struct.I_plotting   = I_plotting;
%-----values just needed for plotting-------------------------------------
if (I_plotting == 1)
   FVr_xplot                = 0; %just needed for plotting
   FVr_lim_lo_plot          = FVr_bound(I_D)-(FVr_bound(I_D)+1)*step(FVr_xplot+1.2)+(FVr_bound(I_D)+1)*step(FVr_xplot-1.2);
   S_struct.FVr_xplot       = FVr_xplot;
   S_struct.FVr_lim_lo_plot = FVr_lim_lo_plot;
end

[FVr_x,S_y,I_nf] = deopt(num2str(objfun),S_struct,objfun,dataset)
%%

%%
bestpara  = FVr_x;
rmse      = S_y;




































