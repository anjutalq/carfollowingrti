close all
clear all
clc
global datasets
%% Helly
minb_helly = [-0.5,-0.5,-10000,-10000,-10000,0.0];
maxb_helly = [ 0.5, 0.5,10000,10000,10000,0.5];
id_helly   = 6;
%% IDM
minb_IDM = [0.01,0.01,1,0.01,4,0.01];%-10*ones(1,I_D);
maxb_IDM = [6,6,70,6,4,6];
id_IDM   = 6;
%% GHR
minb_GHR = [-100,-10,-10,0];%-10*ones(1,I_D);
maxb_GHR = [100,10,10,2];
id_GHR   = 4;
%% st
minb_st = [-100,-100,-100,0];%-10*ones(1,I_D);
maxb_st = [100,100,100,2];
id_st   = 4;
%%
tic
para = [];
%% frame
% trainfile = 'train_data_ngsim.mat';
% data = load(trainfile);
% for i = 1:1
%     datasets = struct('dx',{data.dx{i}},'dv',{data.dv{i}},'vel',{data.vel{i}},'acc',{data.acc{i}});
%     [a,b]  = DE_train_ngsim(data,id_helly,minb_helly,maxb_helly,'objfun_helly');
% %     [a,b]  = DE_train_ngsim(data,id_IDM,minb_IDM,maxb_IDM,'objfun_IDM');
%     para{i} = [a,b.FVr_oa];
% %     [a,b]  = DE_train_ngsim(data,id_GHR,minb_GHR,maxb_GHR,'objfun_GHR');
% % 	[a,b]  = DE_train_ngsim(data,id_IDM,minb_IDM,maxb_IDM,'objfun_IDM');
% end
%% gross
% err_percent = [];
% trainfile = 'train_data_ngsim.mat';
% data = load(trainfile);
for i = 1:4
%     trainfile = ['symtestS/train_seq_',num2str(i),'.mat'];
%     trainfile = 'train_gross_3279.mat';
%     trainfile = ['train/CFtest_seq_',num2str(i),'.mat'];
    trainfile = ['train_Higgs/CFtest_seq_',num2str(i),'.mat'];
    data = load(trainfile);
    datasets = data;
%     datasets = struct('dx',{data.dx{i}},'dv',{data.dv{i}},'vel',{data.vel{i}},'acc',{data.acc{i}});
%     [a,b]  = DE_train_ngsim(data,id_helly,minb_helly,maxb_helly,'objfun_helly');
    [a,b]  = DE_train_ngsim(data,id_IDM,minb_IDM,maxb_IDM,'objfun_IDM');
    para{i} = [a,b.FVr_oa];
%     err_percent(i) = percentage_calculate(para{i});
%     [a,b]  = DE_train_ngsim(data,id_GHR,minb_GHR,maxb_GHR,'objfun_GHR');
% 	[a,b]  = DE_train_ngsim(data,id_IDM,minb_IDM,maxb_IDM,'objfun_IDM');
end
%%
toc

% err_percent

% format short
% rms = [];
% a
% for i = 1:6
%     testfile = ['symtestS/test_seq_',num2str(i),'.mat'];
%     data = load(testfile);
%     para1 = a;
%     [obs_data, cal_data, rmse]  = DE_test_ngsim(data , para1 , 'helly');
% %     index = 1:length(obs_data);
% %     plot(index,obs_data,'r.',index,cal_data,'b.');
%     rms = [rms, rmse];
% end
% rms

%% 1 0.1 0.1 1 10 10 0.1
% a =    0.0030    0.0103   18.8324    0.1092  -96.3834    0.2117
% rms =    0.0110    0.0080    0.0087    0.0094    0.0094    0.0128

%%% a =    -0.0033   -0.0100   19.4123    0.0922   99.9678    0.4520
% rms =    0.0105    0.0073    0.0086    0.0085    0.0083    0.0118

% a =   -0.0038   -0.0102   20.6457    0.0877   97.7328    0.2830
% rms =    0.0108    0.0073    0.0091    0.0094    0.0091    0.0131
%% 2
% a = 0.0014   -0.0104   17.1839    0.1230   96.1469    0.1228
% rms = 0.0115    0.0082    0.0080    0.0084    0.0084    0.0121

%%% a = -0.0119    0.0162   41.4010   -0.0205  -61.6575    0.2789
% rms = 0.0266    0.0085    0.0209    0.0292    0.0331    0.0446

% a = 0.0010   -0.0137   24.2988    0.0729   73.2035    0.1754
% rms = 0.0152    0.0082    0.0120    0.0143    0.0140    0.0212

%% 3
%%% a = -0.0039    0.0155   18.4503    0.1184  -64.3394    0.1045
% rms = 0.0179    0.0123    0.0119    0.0130    0.0142    0.0198

% a = 0.0013   -0.0103   13.4538    0.1751   97.0715    0.3308
% rms = 0.0114    0.0106    0.0084    0.0097    0.0090    0.0117

% a = 0.0035   -0.0118   17.2170    0.1263   84.6991    0.2709
% rms = 0.0137    0.0096    0.0090    0.0096    0.0099    0.0141

%% 4
%%% a = -0.0036   -0.0185   18.0853    0.0381   54.0406    0.0664
% rms = 0.0241    0.0231    0.0206    0.0096    0.0114    0.0139

% a = 0.0030   -0.0107   24.2883   -0.0281   93.9174    0.1540
% rms = 0.0184    0.0194    0.0160    0.0079    0.0081    0.0120

% a = 0.0032   -0.0145    2.5116    0.2216   69.0552    0.0024
% rms = 0.0200    0.0190    0.0145    0.0104    0.0100    0.0082

%% 5
%%% a = -0.0130    0.0102   29.5684   -0.1410  -97.6670    0.2999
% rms = 0.0283    0.0320    0.0250    0.0125    0.0100    0.0118

% a = 0.0128   -0.0109   28.0353   -0.0995   91.9408    0.3787
% rms = 0.0279    0.0297    0.0239    0.0117    0.0104    0.0133

% a = -0.0084    0.0128   29.2634   -0.1005  -78.0688    0.1207
% rms = 0.0269    0.0307    0.0240    0.0113    0.0101    0.0155

%% 6
%%% a = -0.0096   -0.0010  106.7102   -1.3777  999.9424    0.1986
% rms = 0.0132    0.0161    0.0156    0.0118    0.0122    0.0099

% a = 
% rms = 0.2629    0.2256    0.2421    0.1603    0.1910    0.1264

% a = 
% rms = 

% [a1,b1]  = DE_train_ngsim(data,id_IDM,minb_IDM,maxb_IDM,'objfun_IDM');
% [a2,b2]  = DE_train_ngsim(data,id_GHR,minb_GHR,maxb_GHR,'objfun_GHR');
% [a3,b3]  = DE_train_ngsim(data,id_helly,minb_helly,maxb_helly,'objfun_helly');
% [a4,b4]  = DE_train_ngsim(data,id_st,minb_st,maxb_st,'objfun_st');
% a1 = DE_train('data/train_data2396.mat',id,minb,maxb,'objfun_helly');
% a2 = DE_train('data/train_data2790.mat',id,minb,maxb,'objfun_helly');
% a3 = DE_train('data/train_data2797.mat',id,minb,maxb,'objfun_helly');
% a4 = DE_train('data/train_data2809.mat',id,minb,maxb,'objfun_helly');
% a5 = DE_train('data/train_data2849.mat',id,minb,maxb,'objfun_helly');
% a6 = DE_train('data/train_data2935.mat',id,minb,maxb,'objfun_helly');
% a7 = DE_train('data/train_data2973.mat',id,minb,maxb,'objfun_helly');
% a8 = DE_train('data/train_data3078.mat',id,minb,maxb,'objfun_helly');
% a9 = DE_train('data/train_data3108.mat',id,minb,maxb,'objfun_helly');
% a10 = DE_train('data/train_data3196.mat',id,minb,maxb,'objfun_helly');
%% GHR
% minb = [-100,-10,-10,0];
% maxb = [100,10,10,2];
% id   = 4;
% b1 = DE_train('data/train_data2396.mat',id,minb,maxb,'objfun_GHR');
% b2 = DE_train('data/train_data2790.mat',id,minb,maxb,'objfun_GHR');
% b3 = DE_train('data/train_data2797.mat',id,minb,maxb,'objfun_GHR');
% b4 = DE_train('data/train_data2809.mat',id,minb,maxb,'objfun_GHR');
% b5 = DE_train('data/train_data2849.mat',id,minb,maxb,'objfun_GHR');
% b6 = DE_train('data/train_data2935.mat',id,minb,maxb,'objfun_GHR');
% b7 = DE_train('data/train_data2973.mat',id,minb,maxb,'objfun_GHR');
% b8 = DE_train('data/train_data3078.mat',id,minb,maxb,'objfun_GHR');
% b9 = DE_train('data/train_data3108.mat',id,minb,maxb,'objfun_GHR');
% b10 = DE_train('data/train_data3196.mat',id,minb,maxb,'objfun_GHR');
%%