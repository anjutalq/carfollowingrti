=======
test_mat2txt.py:
	input: .mat file
	output: .txt file for .rti processing!
=======
patternmining.py:
	input: rti+ solution, rti+ input data
	output: clustered sub state sequence, assigned cluster IDs state sequence
=======
convert.py:
	if you want to plot the automaton learned by rti+, you need to convert the rti+ solution to a dot file
	input: rti+ solution
	output: dot file
=======
video file:
	the video presentation of index 53 frame of NGSIM I80 (fig. 7 in our IFAC paper)
=======
data file:
	the NGSIM time series data. We would like to thank Marcello Montanino and Vincenzo Punzo for sharing the data. If you want to use this data as well, please cite their work as we did:

@misc{pun13,
  title={Reconstructed {NGSIM} {I80-1}. {COST ACTION TU0903-MULTITUDE}},
  author={Marcello Montanino and Vincenzo Punzo},
  year = {2013},
  howpublished= {\url{http://www.multitude-project.eu/exchange/\newline 101.html}}
}
@article{pun15,
  title={Trajectory data reconstruction and simulation-based validation against macroscopic traffic patterns},
  author={Marcello Montanino and Vincenzo Punzo},
  journal={Transportation Research Part B: Methodological},
  volume={80},
  pages={82--106},
  year={2015},
  publisher={Elsevier}
}
 
patternParsing.py:
	input: pre-defined pattern sequence (e.g. S0-S1-S6-S10-S12)
	output: original vehicle states sequence matching this specific pattern
=======

RTI+
An algorithm for learning real-time automata from unlabeled data based on the likelihood ratio test.

1. software download: http://www.cs.ru.nl/~sicco/software.htm

You can refer to paper “A Likelihood-Ratio Test for Identifying Probabilistic Deterministic Real-Time Automata from Positive Data” for details of the algorithm.

2. compile RTI+

cd the path of RTI
make clean
make

If you encounter some errors, maybe you need to install the GNU scientific library.

3. execute
cd the build file (if it compiles successfully, you will see an executable file)
./rti -—help (you will see how to use rti to get the solution)

e.g. 

./rti 1 0.05 INPUTFILE


4. store the solution into solution.txt

5. run the convert.py to convert solution.txt into rti.dot

6. dot -Tpdf rti.dot > try.pdf (pdf file of visible automata)